(function() {
    'use strict';

    angular
        .module('probioticApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
