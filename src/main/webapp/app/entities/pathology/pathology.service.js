(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Pathology', Pathology);

    Pathology.$inject = ['$resource', 'DateUtils'];

    function Pathology ($resource, DateUtils) {
        var resourceUrl =  'api/pathologies/:id';

        return $resource(resourceUrl, {}, {
            'getArray': { method: 'GET', isArray: true, params: {'id.in': []}},
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
