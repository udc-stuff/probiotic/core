(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologyDeleteController',PathologyDeleteController);

    PathologyDeleteController.$inject = ['$uibModalInstance', 'entity', 'Pathology'];

    function PathologyDeleteController($uibModalInstance, entity, Pathology) {
        var vm = this;

        vm.pathology = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Pathology.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
