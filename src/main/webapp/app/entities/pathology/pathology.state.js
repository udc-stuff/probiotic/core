(function () {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('pathology', {
                parent: 'entity',
                url: '/pathology?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.pathology.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/pathology/pathologies.html',
                        controller: 'PathologyController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pathology');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('pathology-detail', {
                parent: 'pathology',
                url: '/pathology/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.pathology.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/pathology/pathology-detail.html',
                        controller: 'PathologyDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pathology');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Pathology', function ($stateParams, Pathology) {
                        return Pathology.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'pathology',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('pathology-detail.edit', {
                parent: 'pathology-detail',
                url: '/detail/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', 'PaginationUtil', function ($stateParams, $state, $uibModal, PaginationUtil) {
                    $uibModal.open({
                        templateUrl: 'app/entities/pathology/pathology-dialog.html',
                        controller: 'PathologyDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Pathology', function (Pathology) {
                                return Pathology.get({id: $stateParams.id}).$promise;
                            }],
                            pagingParams: function () {
                                return {
                                    page: PaginationUtil.parsePage($stateParams.page),
                                    sort: $stateParams.sort,
                                    predicate: PaginationUtil.parsePredicate($stateParams.sort),
                                    ascending: PaginationUtil.parseAscending($stateParams.sort)
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('^', {}, {reload: false});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('pathology.new', {
                parent: 'pathology',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },

                onEnter: ['$stateParams', '$state', '$uibModal', 'PaginationUtil', function ($stateParams, $state, $uibModal, PaginationUtil) {
                    $uibModal.open({
                        templateUrl: 'app/entities/pathology/pathology-dialog.html',
                        controller: 'PathologyDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        params: {
                            page: {
                                value: '1',
                                squash: true
                            },
                            sort: {
                                value: 'id,asc',
                                squash: true
                            }
                        },
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    timestamp: null,
                                    acronym: null,
                                    id: null
                                };
                            },
                            pagingParams: function () {
                                return {
                                    page: PaginationUtil.parsePage($stateParams.page),
                                    sort: $stateParams.sort,
                                    predicate: PaginationUtil.parsePredicate($stateParams.sort),
                                    ascending: PaginationUtil.parseAscending($stateParams.sort)
                                };

                            }
                        }
                    }).result.then(function () {
                        $state.go('pathology', null, {reload: 'pathology'});
                    }, function () {
                        $state.go('pathology');
                    });
                }]
            })
            .state('pathology.edit', {
                parent: 'pathology',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', 'PaginationUtil',  function ($stateParams, $state, $uibModal, PaginationUtil) {
                    $uibModal.open({
                        templateUrl: 'app/entities/pathology/pathology-dialog.html',
                        controller: 'PathologyDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Pathology', function (Pathology) {
                                return Pathology.get({id: $stateParams.id}).$promise;
                            }],
                            pagingParams: function () {
                                return {
                                    page: PaginationUtil.parsePage($stateParams.page),
                                    sort: $stateParams.sort,
                                    predicate: PaginationUtil.parsePredicate($stateParams.sort),
                                    ascending: PaginationUtil.parseAscending($stateParams.sort)
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('pathology', null, {reload: 'pathology'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('pathology.delete', {
                parent: 'pathology',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/pathology/pathology-delete-dialog.html',
                        controller: 'PathologyDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Pathology', function (Pathology) {
                                return Pathology.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('pathology', null, {reload: 'pathology'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('pathology-info', {
                parent: 'pathology',
                url: '/pathologies/{id}',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/pathology/pathology-info.html',
                        controller: 'PathologyInfoController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    }
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('home');
                        $translatePartialLoader.addPart('pathology');
                        $translatePartialLoader.addPart('pathologytype');
                        $translatePartialLoader.addPart('age');

                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Pathology', function ($stateParams, Pathology) {
                        return Pathology.getArray({'id.in': [$stateParams.id]}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'home',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            });
    }

})();

