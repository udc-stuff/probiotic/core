(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologyInfoController', PathologyInfoController);

    PathologyInfoController.$inject = ['$scope', '$rootScope', 'previousState', 'entity', 'Evidence', 'Probiotic', 'AlertService', 'ParseLinks', 'paginationConstants', 'pagingParams'];

    function PathologyInfoController($scope, $rootScope, previousState, entity, Evidence, Probiotic, AlertService, ParseLinks, paginationConstants, pagingParams) {
        var vm = this;

        vm.loadPage = loadPage;
        vm.pathology = entity;
        vm.previousState = previousState.name;
        vm.whatAge = whatAge;

        loadAllEvidences();
        loadAllProbiotics();

        var unsubscribe = $rootScope.$on('probioticApp:pathologyUpdate', function (event, result) {
            vm.pathology = result;
        });

        $scope.$on('$destroy', unsubscribe);

        function loadAllEvidences() {

            Evidence.query({
                page: pagingParams.page - 1,
                size: 100000
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.evidences = data;
                vm.page = pagingParams.page;

                matchEvidences();
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }

        }

        function loadAllProbiotics() {

            Probiotic.query({
                page: pagingParams.page - 1,
                size: 100000
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.probiotics = data;
                vm.page = pagingParams.page;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }

        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function matchEvidences() {
            vm.pathology.map(
                function callback(currentValue) {

                    currentValue.evidences =
                        vm.evidences.filter(
                            function callback(currentValue) {
                                return currentValue.pathologyId == this.id;
                            }, currentValue
                        );

                    return currentValue;
                }
            );
            vm.pathology.map(
                function callback(currentValue) {

                    currentValue.probioticInfo =
                        vm.probiotics.filter(
                            function callback(currentValue) {
                                return currentValue.id == this.evidences[0].probioticId;
                            }, currentValue
                        );

                    return currentValue;
                }
            );
            whatAge();

        }


        function whatAge() {
            for (var i = 0; i < vm.pathology.length; i++) {
                vm.referenceshow =false;

                for (var j = 0; j < vm.pathology[i].evidences.length; j++) {
                    if (vm.pathology[i].evidences[j].dosageAge == "Adulto") {
                        vm.ageAdult = true;
                    } else {
                        vm.agePedia = true;
                    }
                }
                if (vm.ageAdult == true && vm.agePedia == true) {
                    vm.pathology[i]['Age'] = 'Ambos';
                }
                else if (vm.ageAdult == true && vm.agePedia == false) {
                    vm.pathology[i]['Age'] = 'Adulto';
                }
                else if (vm.ageAdult == false && vm.agePedia == true) {
                    vm.pathology[i]['Age'] = 'Pediatrico';
                }
            }
            return vm.pathology;
            showreferences();

        }



        function showreferences() {
            for (var i = 0; i < vm.pathology.length; i++) {
                for (var j = 0; j < vm.pathology[i].evidences.length; j++) {
                    vm.pathology[i].evidences[j]['show'] = false;

                }
            }
            return vm.pathology;

        }

    }
})();
