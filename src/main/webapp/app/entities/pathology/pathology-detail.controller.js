(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologyDetailController', PathologyDetailController);

    PathologyDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Pathology', 'User', 'Pathologytype'];

    function PathologyDetailController($scope, $rootScope, $stateParams, previousState, entity, Pathology, User, Pathologytype) {
        var vm = this;

        vm.pathology = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:pathologyUpdate', function(event, result) {
            vm.pathology = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
