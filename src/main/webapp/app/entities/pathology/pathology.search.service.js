(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('PathologySearch', PathologySearch);

    PathologySearch.$inject = ['$resource'];

    function PathologySearch($resource) {
        var resourceUrl =  'api/_search/pathologies/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
