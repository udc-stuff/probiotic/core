(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologyDialogController', PathologyDialogController);

    PathologyDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pathology', 'User', 'Pathologytype', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function PathologyDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Pathology, User, Pathologytype,  ParseLinks, AlertService, paginationConstants, pagingParams) {
        var vm = this;

        vm.pathology = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transitionPathologytype = transitionPathologytype;
        vm.itemsPerPage = paginationConstants.itemsPerPage;


       loadPathologytype();


        function loadPathologytype() {

            Pathologytype.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.pathologytypes = data;
                vm.page = pagingParams.page;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function transitionPathologytype() {
            pagingParams.page = vm.page;
            loadPathologytype();
        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.pathology.id !== null) {
                Pathology.update(vm.pathology, onSaveSuccess, onSaveError);
            } else {
                Pathology.save(vm.pathology, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('probioticApp:pathologyUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
