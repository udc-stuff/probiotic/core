(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('ProbioticDeleteController',ProbioticDeleteController);

    ProbioticDeleteController.$inject = ['$uibModalInstance', 'entity', 'Probiotic'];

    function ProbioticDeleteController($uibModalInstance, entity, Probiotic) {
        var vm = this;

        vm.probiotic = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Probiotic.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
