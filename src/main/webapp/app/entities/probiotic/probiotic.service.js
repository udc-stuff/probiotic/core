(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Probiotic', Probiotic);

    Probiotic.$inject = ['$resource', 'DateUtils'];

    function Probiotic ($resource, DateUtils) {
        var resourceUrl =  'api/probiotics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
