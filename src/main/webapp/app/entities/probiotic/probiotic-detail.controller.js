(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('ProbioticDetailController', ProbioticDetailController);

    ProbioticDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Dosage', 'ParseLinks', 'AlertService', 'User'];

    function ProbioticDetailController($scope, $rootScope, $stateParams, previousState, entity, Dosage, ParseLinks, AlertService, User) {
        var vm = this;

        vm.probiotic = [entity];
        vm.previousState = previousState.name;

        loadAllDosages();

        var unsubscribe = $rootScope.$on('probioticApp:probioticUpdate', function(event, result) {
            vm.probiotic = result;
        });
        $scope.$on('$destroy', unsubscribe);

        function loadAllDosages() {
            Dosage.query({
                //page: pagingParams.page - 1,
                size: 100000
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.dosages = data;
                //vm.page = pagingParams.page;

                matchProbiotics()
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }


        function matchProbiotics() {

            vm.probiotic.map(
                function callback(currentValue) {
                    currentValue.dosages =
                        vm.dosages.filter(
                            function callback(currentValue) {
                                return currentValue.probioticId == this.id;
                            }, currentValue
                        );

                    return currentValue;
                }
            );
            vm.probiotic = vm.probiotic[0];
        }
    }
})();
