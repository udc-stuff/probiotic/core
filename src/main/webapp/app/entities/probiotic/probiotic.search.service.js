(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('ProbioticSearch', ProbioticSearch);

    ProbioticSearch.$inject = ['$resource'];

    function ProbioticSearch($resource) {
        var resourceUrl =  'api/_search/probiotics/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
