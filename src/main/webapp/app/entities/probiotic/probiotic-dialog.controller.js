(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('ProbioticDialogController', ProbioticDialogController);

    ProbioticDialogController.$inject = ['$timeout', '$scope', '$state', '$stateParams', 'entity', 'Probiotic', 'Allergens', 'Strain', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function ProbioticDialogController($timeout, $scope, $state, $stateParams, entity, Probiotic, Allergens, Strain, ParseLinks, AlertService, paginationConstants, pagingParams) {
        var vm = this;

        vm.currentSelectionAllergens = currentSelectionAllergens;
        vm.currentSelectionStrains = currentSelectionStrains;
        vm.isCheckedAllergens = isCheckedAllergens;
        vm.isCheckedStrains = isCheckedStrains;
        vm.save = save;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.probiotic = entity;

        vm.transitionStrain = transitionStrain;
        vm.transitionAllergen = transitionAllergen;

        loadStrain();
        loadAllergen();


        function loadAllergen() {

            Allergens.query({
                page: pagingParams.pageAllergen - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsAllergen = headers('X-Total-Count');
                vm.queryCountAllergen = vm.totalItemsAllergen;
                vm.allergens = data;
                vm.pageAllergen = pagingParams.pageAllergen;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function transitionAllergen() {
            pagingParams.pageAllergen = vm.pageAllergen;
            loadAllergen();
        }

        function loadStrain() {
            Strain.query({
                page: pagingParams.pageStrain - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsStrain = headers('X-Total-Count');
                vm.queryCountStrain = vm.totalItemsStrain;
                vm.strains = data;
                vm.pageStrain = pagingParams.pageStrain;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function transitionStrain() {
            pagingParams.pageStrain = vm.pageStrain;
            loadStrain();
        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function save() {
            vm.isSaving = true;
            if (vm.probiotic.id !== null) {
                Probiotic.update(vm.probiotic, onSaveSuccess, onSaveError);
            } else {
                Probiotic.save(vm.probiotic, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('probioticApp:probioticUpdate', result);
            window.history.go(-1);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function isCheckedAllergens(id) {
            if (vm.probiotic.allergens != null) {
                var match = false;
                for (var i = 0; i < vm.probiotic.allergens.length; i++) {
                    if (vm.probiotic.allergens[i].id == id) {
                        match = true;
                    }
                }
                return match;
            }
            else {
                vm.probiotic.allergens = [];
            }
        }

        function currentSelectionAllergens(ids) {
            var idx = null;
            var exist = false;
            for (var i = 0; i < vm.probiotic.allergens.length; i++) {
                if (ids.id == vm.probiotic.allergens[i].id) {
                    idx = vm.probiotic.allergens.indexOf(vm.probiotic.allergens[i]);
                    exist = true;
                }
            }
            if (!exist)
                idx = vm.probiotic.allergens.indexOf(ids);

            if (idx > -1) {
                vm.probiotic.allergens.splice(idx, 1);
            }
            else {
                vm.probiotic.allergens.push(ids);
            }
        }

        function isCheckedStrains(id) {
            if (vm.probiotic.strains != null) {
                var match = false;
                for (var i = 0; i < vm.probiotic.strains.length; i++) {
                    if (vm.probiotic.strains[i].id == id) {
                        match = true;
                    }
                }
                return match;
            }
            else {
                vm.probiotic.strains = [];
            }
        }

        function currentSelectionStrains(ids) {
            var idx = null;
            var exist = false;
            for (var i = 0; i < vm.probiotic.strains.length; i++) {
                if (ids.id == vm.probiotic.strains[i].id) {
                    idx = vm.probiotic.strains.indexOf(vm.probiotic.strains[i]);
                    exist = true;
                }
            }
            if (!exist)
                idx = vm.probiotic.strains.indexOf(ids);

            if (idx > -1) {
                vm.probiotic.strains.splice(idx, 1);
            }
            else {
                vm.probiotic.strains.push(ids);
            }
        }

    }
})();
