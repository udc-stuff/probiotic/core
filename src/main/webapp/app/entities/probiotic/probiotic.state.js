(function () {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('probiotic', {
                parent: 'entity',
                url: '/probiotic?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.probiotic.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/probiotic/probiotics.html',
                        controller: 'ProbioticController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('probiotic');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('probiotic-detail', {
                parent: 'probiotic',
                url: '/probiotic/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.probiotic.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/probiotic/probiotic-detail.html',
                        controller: 'ProbioticDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('probiotic');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Probiotic', function ($stateParams, Probiotic) {
                        return Probiotic.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'probiotic',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('probiotic.new', {
                parent: 'probiotic',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.probiotic.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/probiotic/probiotic-dialog.html',
                        controller: 'ProbioticDialogController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    pageAllergen:{
                        value: '1',
                    squash: true,
                    },
                    pageStrain: {
                        value: '1',
                        squash: true,
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    }
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            pageAllergen: PaginationUtil.parsePage($stateParams.pageAllergen),
                            pageStrain: PaginationUtil.parsePage($stateParams.pageStrain),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort)
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('home');
                        $translatePartialLoader.addPart('probiotic');
                        return $translate.refresh();
                    }],
                    entity: [function () {
                        return {
                            name: null,
                            timestamp: null,
                            have_allergens: false,
                            id: null
                        }
                    }]
                }
            })
            .state('probiotic.edit', {
                parent: 'probiotic',
                url: '/edit/{id}',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/probiotic/probiotic-dialog.html',
                        controller: 'ProbioticDialogController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    pageAllergen:{
                        value: '1',
                        squash: true,
                    },
                    pageStrain: {
                        value: '1',
                        squash: true,
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    }
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            pageAllergen: PaginationUtil.parsePage($stateParams.pageAllergen),
                            pageStrain: PaginationUtil.parsePage($stateParams.pageStrain),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort)
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('home');
                        $translatePartialLoader.addPart('probiotic');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Probiotic', function ($stateParams, Probiotic) {
                        return Probiotic.get({id: $stateParams.id}).$promise;
                    }]
                }
            })
            .state('probiotic.delete', {
                parent: 'probiotic',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/probiotic/probiotic-delete-dialog.html',
                        controller: 'ProbioticDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Probiotic', function (Probiotic) {
                                return Probiotic.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('probiotic', null, {reload: 'probiotic'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
