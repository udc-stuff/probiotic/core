(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Strain', Strain);

    Strain.$inject = ['$resource', 'DateUtils'];

    function Strain ($resource, DateUtils) {
        var resourceUrl =  'api/strains/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
