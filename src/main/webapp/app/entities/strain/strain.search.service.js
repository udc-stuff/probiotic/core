(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('StrainSearch', StrainSearch);

    StrainSearch.$inject = ['$resource'];

    function StrainSearch($resource) {
        var resourceUrl =  'api/_search/strains/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
