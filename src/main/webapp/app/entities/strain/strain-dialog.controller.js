(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('StrainDialogController', StrainDialogController);

    StrainDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Strain', 'User'];

    function StrainDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Strain, User) {
        var vm = this;

        vm.strain = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.strain.id !== null) {
                Strain.update(vm.strain, onSaveSuccess, onSaveError);
            } else {
                Strain.save(vm.strain, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:strainUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
