(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('StrainDeleteController',StrainDeleteController);

    StrainDeleteController.$inject = ['$uibModalInstance', 'entity', 'Strain'];

    function StrainDeleteController($uibModalInstance, entity, Strain) {
        var vm = this;

        vm.strain = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Strain.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
