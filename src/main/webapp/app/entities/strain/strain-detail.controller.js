(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('StrainDetailController', StrainDetailController);

    StrainDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Strain', 'User'];

    function StrainDetailController($scope, $rootScope, $stateParams, previousState, entity, Strain, User) {
        var vm = this;

        vm.strain = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:strainUpdate', function(event, result) {
            vm.strain = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
