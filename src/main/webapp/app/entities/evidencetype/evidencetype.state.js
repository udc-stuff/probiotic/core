(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evidencetype', {
            parent: 'entity',
            url: '/evidencetype?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.evidencetype.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evidencetype/evidencetypes.html',
                    controller: 'EvidencetypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evidencetype');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
       /* .state('evidencetype-detail', {
            parent: 'evidencetype',
            url: '/evidencetype/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.evidencetype.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evidencetype/evidencetype-detail.html',
                    controller: 'EvidencetypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evidencetype');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Evidencetype', function($stateParams, Evidencetype) {
                    return Evidencetype.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evidencetype',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evidencetype-detail.edit', {
            parent: 'evidencetype-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencetype/evidencetype-dialog.html',
                    controller: 'EvidencetypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evidencetype', function(Evidencetype) {
                            return Evidencetype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('evidencetype.new', {
            parent: 'evidencetype',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencetype/evidencetype-dialog.html',
                    controller: 'EvidencetypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                timestamp: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evidencetype', null, { reload: 'evidencetype' });
                }, function() {
                    $state.go('evidencetype');
                });
            }]
        })
        .state('evidencetype.edit', {
            parent: 'evidencetype',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencetype/evidencetype-dialog.html',
                    controller: 'EvidencetypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evidencetype', function(Evidencetype) {
                            return Evidencetype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evidencetype', null, { reload: 'evidencetype' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evidencetype.delete', {
            parent: 'evidencetype',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencetype/evidencetype-delete-dialog.html',
                    controller: 'EvidencetypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Evidencetype', function(Evidencetype) {
                            return Evidencetype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evidencetype', null, { reload: 'evidencetype' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
