(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencetypeDeleteController',EvidencetypeDeleteController);

    EvidencetypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'Evidencetype'];

    function EvidencetypeDeleteController($uibModalInstance, entity, Evidencetype) {
        var vm = this;

        vm.evidencetype = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Evidencetype.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
