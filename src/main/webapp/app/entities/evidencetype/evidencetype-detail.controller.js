(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencetypeDetailController', EvidencetypeDetailController);

    EvidencetypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Evidencetype'];

    function EvidencetypeDetailController($scope, $rootScope, $stateParams, previousState, entity, Evidencetype) {
        var vm = this;

        vm.evidencetype = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:evidencetypeUpdate', function(event, result) {
            vm.evidencetype = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
