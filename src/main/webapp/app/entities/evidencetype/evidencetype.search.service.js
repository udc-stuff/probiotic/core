(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('EvidencetypeSearch', EvidencetypeSearch);

    EvidencetypeSearch.$inject = ['$resource'];

    function EvidencetypeSearch($resource) {
        var resourceUrl =  'api/_search/evidencetypes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
