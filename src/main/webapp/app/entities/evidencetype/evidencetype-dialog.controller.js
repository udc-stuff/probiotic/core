(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencetypeDialogController', EvidencetypeDialogController);

    EvidencetypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Evidencetype'];

    function EvidencetypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Evidencetype) {
        var vm = this;

        vm.evidencetype = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.evidencetype.id !== null) {
                Evidencetype.update(vm.evidencetype, onSaveSuccess, onSaveError);
            } else {
                Evidencetype.save(vm.evidencetype, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:evidencetypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
