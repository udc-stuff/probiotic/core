(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('DosageSearch', DosageSearch);

    DosageSearch.$inject = ['$resource'];

    function DosageSearch($resource) {
        var resourceUrl =  'api/_search/dosages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
