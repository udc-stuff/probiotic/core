(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DosageDetailController', DosageDetailController);

    DosageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Dosage', 'Probiotic', 'Doseform', 'User'];

    function DosageDetailController($scope, $rootScope, $stateParams, previousState, entity, Dosage, Probiotic, Doseform, User) {
        var vm = this;

        vm.dosage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:dosageUpdate', function(event, result) {
            vm.dosage = result;
        });
        $scope.$on('$destroy', unsubscribe);


        console.log(vm.previousState);
    }
})();
