(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DosageDeleteController',DosageDeleteController);

    DosageDeleteController.$inject = ['$uibModalInstance', 'entity', 'Dosage'];

    function DosageDeleteController($uibModalInstance, entity, Dosage) {
        var vm = this;

        vm.dosage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Dosage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
