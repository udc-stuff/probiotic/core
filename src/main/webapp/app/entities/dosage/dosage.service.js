(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Dosage', Dosage);

    Dosage.$inject = ['$resource', 'DateUtils'];

    function Dosage ($resource, DateUtils) {
        var resourceUrl =  'api/dosages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
