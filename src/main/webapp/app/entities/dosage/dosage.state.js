(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('dosage', {
            parent: 'entity',
            url: '/dosage?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'probioticApp.dosage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dosage/dosages.html',
                    controller: 'DosageController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dosage');
                    $translatePartialLoader.addPart('age');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('dosage-detail', {
            parent: 'dosage',
            url: '/dosage/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'probioticApp.dosage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/dosage/dosage-detail.html',
                    controller: 'DosageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('dosage');
                    $translatePartialLoader.addPart('age');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Dosage', function($stateParams, Dosage) {
                    return Dosage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'dosage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('dosage-detail.edit', {
            parent: 'dosage-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dosage/dosage-dialog.html',
                    controller: 'DosageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dosage', function(Dosage) {
                            return Dosage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dosage.new', {
            parent: 'dosage',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dosage/dosage-dialog.html',
                    controller: 'DosageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                timestamp: null,
                                age: null,
                                cfu: null,
                                intake: null,
                                frequency: null,
                                indication: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('dosage', null, { reload: 'dosage' });
                }, function() {
                    $state.go('dosage');
                });
            }]
        })
        .state('dosage.edit', {
            parent: 'dosage',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dosage/dosage-dialog.html',
                    controller: 'DosageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Dosage', function(Dosage) {
                            return Dosage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dosage', null, { reload: 'dosage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('dosage.delete', {
            parent: 'dosage',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/dosage/dosage-delete-dialog.html',
                    controller: 'DosageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Dosage', function(Dosage) {
                            return Dosage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('dosage', null, { reload: 'dosage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
