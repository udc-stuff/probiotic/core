(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DosageDialogController', DosageDialogController);

    DosageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Dosage', 'Probiotic', 'Doseform', 'User'];

    function DosageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Dosage, Probiotic, Doseform, User) {
        var vm = this;

        vm.dosage = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.probiotics = Probiotic.query();
        vm.doseforms = Doseform.query();
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dosage.id !== null) {
                Dosage.update(vm.dosage, onSaveSuccess, onSaveError);
            } else {
                Dosage.save(vm.dosage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:dosageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
