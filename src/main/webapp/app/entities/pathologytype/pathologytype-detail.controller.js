(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologytypeDetailController', PathologytypeDetailController);

    PathologytypeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Pathologytype'];

    function PathologytypeDetailController($scope, $rootScope, $stateParams, previousState, entity, Pathologytype) {
        var vm = this;

        vm.pathologytype = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:pathologytypeUpdate', function(event, result) {
            vm.pathologytype = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
