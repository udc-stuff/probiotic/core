(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Pathologytype', Pathologytype);

    Pathologytype.$inject = ['$resource', 'DateUtils'];

    function Pathologytype ($resource, DateUtils) {
        var resourceUrl =  'api/pathologytypes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
