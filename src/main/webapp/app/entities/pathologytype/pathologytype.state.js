(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pathologytype', {
            parent: 'entity',
            url: '/pathologytype?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.pathologytype.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pathologytype/pathologytypes.html',
                    controller: 'PathologytypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('pathologytype');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        /*.state('pathologytype-detail', {
            parent: 'pathologytype',
            url: '/pathologytype/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.pathologytype.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pathologytype/pathologytype-detail.html',
                    controller: 'PathologytypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('pathologytype');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Pathologytype', function($stateParams, Pathologytype) {
                    return Pathologytype.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'pathologytype',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('pathologytype-detail.edit', {
            parent: 'pathologytype-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pathologytype/pathologytype-dialog.html',
                    controller: 'PathologytypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Pathologytype', function(Pathologytype) {
                            return Pathologytype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('pathologytype.new', {
            parent: 'pathologytype',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pathologytype/pathologytype-dialog.html',
                    controller: 'PathologytypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                timestamp: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('pathologytype', null, { reload: 'pathologytype' });
                }, function() {
                    $state.go('pathologytype');
                });
            }]
        })
        .state('pathologytype.edit', {
            parent: 'pathologytype',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pathologytype/pathologytype-dialog.html',
                    controller: 'PathologytypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Pathologytype', function(Pathologytype) {
                            return Pathologytype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pathologytype', null, { reload: 'pathologytype' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pathologytype.delete', {
            parent: 'pathologytype',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pathologytype/pathologytype-delete-dialog.html',
                    controller: 'PathologytypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Pathologytype', function(Pathologytype) {
                            return Pathologytype.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pathologytype', null, { reload: 'pathologytype' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
