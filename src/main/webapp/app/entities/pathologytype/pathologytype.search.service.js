(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('PathologytypeSearch', PathologytypeSearch);

    PathologytypeSearch.$inject = ['$resource'];

    function PathologytypeSearch($resource) {
        var resourceUrl =  'api/_search/pathologytypes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
