(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologytypeDialogController', PathologytypeDialogController);

    PathologytypeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pathologytype'];

    function PathologytypeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Pathologytype) {
        var vm = this;

        vm.pathologytype = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.pathologytype.id !== null) {
                Pathologytype.update(vm.pathologytype, onSaveSuccess, onSaveError);
            } else {
                Pathologytype.save(vm.pathologytype, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:pathologytypeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
