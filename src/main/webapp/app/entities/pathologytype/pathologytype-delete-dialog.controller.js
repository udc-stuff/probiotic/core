(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('PathologytypeDeleteController',PathologytypeDeleteController);

    PathologytypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'Pathologytype'];

    function PathologytypeDeleteController($uibModalInstance, entity, Pathologytype) {
        var vm = this;

        vm.pathologytype = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Pathologytype.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
