(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Allergens', Allergens);

    Allergens.$inject = ['$resource', 'DateUtils'];

    function Allergens ($resource, DateUtils) {
        var resourceUrl =  'api/allergens/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
