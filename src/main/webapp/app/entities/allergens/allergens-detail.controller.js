(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('AllergensDetailController', AllergensDetailController);

    AllergensDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Allergens'];

    function AllergensDetailController($scope, $rootScope, $stateParams, previousState, entity, Allergens) {
        var vm = this;

        vm.allergens = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:allergensUpdate', function(event, result) {
            vm.allergens = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
