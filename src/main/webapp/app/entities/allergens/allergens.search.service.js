(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('AllergensSearch', AllergensSearch);

    AllergensSearch.$inject = ['$resource'];

    function AllergensSearch($resource) {
        var resourceUrl =  'api/_search/allergens/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
