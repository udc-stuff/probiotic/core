(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('AllergensDeleteController',AllergensDeleteController);

    AllergensDeleteController.$inject = ['$uibModalInstance', 'entity', 'Allergens'];

    function AllergensDeleteController($uibModalInstance, entity, Allergens) {
        var vm = this;

        vm.allergens = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Allergens.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
