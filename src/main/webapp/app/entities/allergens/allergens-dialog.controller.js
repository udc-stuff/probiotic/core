(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('AllergensDialogController', AllergensDialogController);

    AllergensDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Allergens'];

    function AllergensDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Allergens) {
        var vm = this;

        vm.allergens = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.allergens.id !== null) {
                Allergens.update(vm.allergens, onSaveSuccess, onSaveError);
            } else {
                Allergens.save(vm.allergens, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:allergensUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
