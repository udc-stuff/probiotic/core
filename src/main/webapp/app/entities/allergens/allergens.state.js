(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('allergens', {
            parent: 'entity',
            url: '/allergens?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.allergens.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/allergens/allergens.html',
                    controller: 'AllergensController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('allergens');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        /*.state('allergens-detail', {
            parent: 'allergens',
            url: '/allergens/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.allergens.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/allergens/allergens-detail.html',
                    controller: 'AllergensDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('allergens');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Allergens', function($stateParams, Allergens) {
                    return Allergens.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'allergens',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('allergens-detail.edit', {
            parent: 'allergens-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allergens/allergens-dialog.html',
                    controller: 'AllergensDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Allergens', function(Allergens) {
                            return Allergens.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('allergens.new', {
            parent: 'allergens',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allergens/allergens-dialog.html',
                    controller: 'AllergensDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                timestamp: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('allergens', null, { reload: 'allergens' });
                }, function() {
                    $state.go('allergens');
                });
            }]
        })
        .state('allergens.edit', {
            parent: 'allergens',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allergens/allergens-dialog.html',
                    controller: 'AllergensDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Allergens', function(Allergens) {
                            return Allergens.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('allergens', null, { reload: 'allergens' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('allergens.delete', {
            parent: 'allergens',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/allergens/allergens-delete-dialog.html',
                    controller: 'AllergensDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Allergens', function(Allergens) {
                            return Allergens.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('allergens', null, { reload: 'allergens' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
