(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DoseformDialogController', DoseformDialogController);

    DoseformDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Doseform'];

    function DoseformDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Doseform) {
        var vm = this;

        vm.doseform = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.doseform.id !== null) {
                Doseform.update(vm.doseform, onSaveSuccess, onSaveError);
            } else {
                Doseform.save(vm.doseform, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:doseformUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
