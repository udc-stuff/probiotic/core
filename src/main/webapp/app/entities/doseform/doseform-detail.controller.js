(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DoseformDetailController', DoseformDetailController);

    DoseformDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Doseform'];

    function DoseformDetailController($scope, $rootScope, $stateParams, previousState, entity, Doseform) {
        var vm = this;

        vm.doseform = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:doseformUpdate', function(event, result) {
            vm.doseform = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
