(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('DoseformDeleteController',DoseformDeleteController);

    DoseformDeleteController.$inject = ['$uibModalInstance', 'entity', 'Doseform'];

    function DoseformDeleteController($uibModalInstance, entity, Doseform) {
        var vm = this;

        vm.doseform = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Doseform.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
