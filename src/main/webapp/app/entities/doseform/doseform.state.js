(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('doseform', {
            parent: 'entity',
            url: '/doseform?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'probioticApp.doseform.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/doseform/doseforms.html',
                    controller: 'DoseformController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('doseform');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        /*.state('doseform-detail', {
            parent: 'doseform',
            url: '/doseform/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'probioticApp.doseform.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/doseform/doseform-detail.html',
                    controller: 'DoseformDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('doseform');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Doseform', function($stateParams, Doseform) {
                    return Doseform.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'doseform',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('doseform-detail.edit', {
            parent: 'doseform-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doseform/doseform-dialog.html',
                    controller: 'DoseformDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Doseform', function(Doseform) {
                            return Doseform.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('doseform.new', {
            parent: 'doseform',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doseform/doseform-dialog.html',
                    controller: 'DoseformDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                type: null,
                                timestamp: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('doseform', null, { reload: 'doseform' });
                }, function() {
                    $state.go('doseform');
                });
            }]
        })
        .state('doseform.edit', {
            parent: 'doseform',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doseform/doseform-dialog.html',
                    controller: 'DoseformDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Doseform', function(Doseform) {
                            return Doseform.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('doseform', null, { reload: 'doseform' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('doseform.delete', {
            parent: 'doseform',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doseform/doseform-delete-dialog.html',
                    controller: 'DoseformDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Doseform', function(Doseform) {
                            return Doseform.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('doseform', null, { reload: 'doseform' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
