(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Doseform', Doseform);

    Doseform.$inject = ['$resource', 'DateUtils'];

    function Doseform ($resource, DateUtils) {
        var resourceUrl =  'api/doseforms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
