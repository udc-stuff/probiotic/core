(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('DoseformSearch', DoseformSearch);

    DoseformSearch.$inject = ['$resource'];

    function DoseformSearch($resource) {
        var resourceUrl =  'api/_search/doseforms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
