(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidenceDialogController', EvidenceDialogController);

    EvidenceDialogController.$inject = ['$timeout', '$scope', '$stateParams', 'entity', 'Evidence', 'Pathology', 'Dosage', 'Evidencereferences', 'Evidencetype', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function EvidenceDialogController($timeout, $scope, $stateParams, entity, Evidence, Pathology, Dosage, Evidencereferences, Evidencetype, ParseLinks, AlertService, paginationConstants, pagingParams) {
        var vm = this;

        vm.evidence = entity;
        vm.save = save;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.transitionPathology = transitionPathology;
        vm.transitionEvidencetype = transitionEvidencetype;
        vm.transitionReferences = transitionReferences;
        vm.transitionDosage = transitionDosage;
        vm.isCheckedReferences = isCheckedReferences;
        vm.currentSelectionReferences = currentSelectionReferences;

        loadEvidencetype();
        loadPathology();
        loadReferences();
        loadDosage();

        function loadEvidencetype() {
            Evidencetype.query({
                page: pagingParams.pageEvidencetype - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsEvidencetype = headers('X-Total-Count');
                vm.queryCountEvidencetype = vm.totalItemsEvidencetype;
                vm.evidencetypes = data;
                vm.pageEvidencetype = pagingParams.pageEvidencetype;
            }
        }

        function loadReferences() {
            Evidencereferences.query({
                page: pagingParams.pageReferences - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsReferences = headers('X-Total-Count');
                vm.queryCountReferences = vm.totalItemsReferences;
                vm.evidencereferences = data;
                vm.pageReferences = pagingParams.pageReferences;
            }
        }

        function loadPathology() {
            Pathology.query({
                page: pagingParams.pagePathology - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsPathology = headers('X-Total-Count');
                vm.queryCountPathology = vm.totalItemsPathology;
                vm.pathologies = data;
                vm.pagePathology = pagingParams.pagePathology;
            }
        }

        function loadDosage() {
            Dosage.query({
                page: pagingParams.pageDosage - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItemsDosage = headers('X-Total-Count');
                vm.queryCountDosage = vm.totalItemsDosage;
                vm.dosages = data;
                vm.pageDosage = pagingParams.pageDosage;
            }
        }

        function transitionPathology() {
            pagingParams.pagePathology = vm.pagePathology;
            loadPathology();
        }

        function transitionEvidencetype() {
            pagingParams.pageEvidencetype = vm.pageEvidencetype;
            loadEvidencetype();
        }

        function transitionReferences() {
            pagingParams.pageReferences = vm.pageReferences;
            loadReferences();
        }

        function transitionDosage() {
            pagingParams.pageDosage = vm.pageDosage;
            loadDosage();
        }

        function sort() {
            var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
            if (vm.predicate !== 'id') {
                result.push('id');
            }
            return result;
        }

        function onError(error) {
            AlertService.error(error.data.message);
        }

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function save() {
            vm.isSaving = true;
            if (vm.evidence.id !== null) {
                Evidence.update(vm.evidence, onSaveSuccess, onSaveError);
            } else {
                Evidence.save(vm.evidence, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('probioticApp:evidenceUpdate', result);
            window.history.go(-1);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function isCheckedReferences(id) {
            if (vm.evidence.evidencereferences != null) {
                var match = false;
                for (var i = 0; i < vm.evidence.evidencereferences.length; i++) {
                    if (vm.evidence.evidencereferences[i].id == id) {
                        match = true;
                    }
                }
                return match;
            }
            else {
                vm.evidence.evidencereferences = [];
            }
        }

        function currentSelectionReferences(ids) {
            var idx = null;
            var exist = false;
            for (var i = 0; i < vm.evidence.evidencereferences.length; i++) {
                if (ids.id == vm.evidence.evidencereferences[i].id) {
                    idx = vm.evidence.evidencereferences.indexOf(vm.evidence.evidencereferences[i]);
                    exist = true;
                }
            }
            if (!exist)
                idx = vm.evidence.evidencereferences.indexOf(ids);

            if (idx > -1) {
                vm.evidence.evidencereferences.splice(idx, 1);
            }
            else {
                vm.evidence.evidencereferences.push(ids);
            }
        }


    }
})();
