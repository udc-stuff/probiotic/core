(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidenceDetailController', EvidenceDetailController);

    EvidenceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Evidence', 'Pathology', 'Dosage', 'Evidencereferences', 'Evidencetype', 'User'];

    function EvidenceDetailController($scope, $rootScope, $stateParams, previousState, entity, Evidence, Pathology, Dosage, Evidencereferences, Evidencetype, User) {
        var vm = this;

        vm.evidence = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:evidenceUpdate', function(event, result) {
            vm.evidence = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
