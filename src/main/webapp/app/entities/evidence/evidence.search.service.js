(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('EvidenceSearch', EvidenceSearch);

    EvidenceSearch.$inject = ['$resource'];

    function EvidenceSearch($resource) {
        var resourceUrl =  'api/_search/evidences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
