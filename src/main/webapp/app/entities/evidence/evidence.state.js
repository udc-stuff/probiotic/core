(function () {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('evidence', {
                parent: 'entity',
                url: '/evidence?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.evidence.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/evidence/evidences.html',
                        controller: 'EvidenceController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    page: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('evidence');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('evidence-detail', {
                parent: 'evidence',
                url: '/evidence/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'probioticApp.evidence.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/evidence/evidence-detail.html',
                        controller: 'EvidenceDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('evidence');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Evidence', function ($stateParams, Evidence) {
                        return Evidence.get({id: $stateParams.id}).$promise;
                    }],
                    previousState: ["$state", function ($state) {
                        var currentStateData = {
                            name: $state.current.name || 'evidence',
                            params: $state.params,
                            url: $state.href($state.current.name, $state.params)
                        };
                        return currentStateData;
                    }]
                }
            })
            .state('evidence.new', {
                parent: 'evidence',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/evidence/evidence-dialog.html',
                        controller: 'EvidenceDialogController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    pageDosage: {
                        value: '1',
                        squash: true
                    },
                    pageReferences: {
                        value: '1',
                        squash: true
                    },
                    pagePathology: {
                        value: '1',
                        squash: true
                    },
                    pageEvidencetype: {
                        value: '1',
                        squash: true
                    },
                    sort: {
                        value: 'id,asc',
                        squash: true
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('evidence');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('dosage');
                        $translatePartialLoader.addPart('age');
                        return $translate.refresh();
                    }],
                    entity: [function () {
                        return {
                            timestamp: null,
                            id: null
                        };
                    }],
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            pagePathology: PaginationUtil.parsePage($stateParams.pagePathology),
                            pageEvidencetype: PaginationUtil.parsePage($stateParams.pageEvidencetype),
                            pageDosage: PaginationUtil.parsePage($stateParams.pageDosage),
                            pageReferences: PaginationUtil.parsePage($stateParams.pageReferences),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort)
                        };
                    }]
                }
            })
            .state('evidence.edit', {
                parent: 'evidence',
                url: '/edit/{id}',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/evidence/evidence-dialog.html',
                        controller: 'EvidenceDialogController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('evidence');
                        $translatePartialLoader.addPart('global');
                        $translatePartialLoader.addPart('dosage');
                        $translatePartialLoader.addPart('age');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Evidence', function ($stateParams, Evidence) {
                        return Evidence.get({id: $stateParams.id}).$promise;
                    }]


                }

            })
            .state('evidence.delete', {
                parent: 'evidence',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/evidence/evidence-delete-dialog.html',
                        controller: 'EvidenceDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Evidence', function (Evidence) {
                                return Evidence.get({id: $stateParams.id}).$promise;
                            }]
                        }
                    }).result.then(function () {
                        $state.go('evidence', null, {reload: 'evidence'});
                    }, function () {
                        $state.go('^');
                    });
                }]
            });
    }

})();
