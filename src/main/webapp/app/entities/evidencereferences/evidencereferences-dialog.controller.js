(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencereferencesDialogController', EvidencereferencesDialogController);

    EvidencereferencesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Evidencereferences', 'User'];

    function EvidencereferencesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Evidencereferences, User) {
        var vm = this;

        vm.evidencereferences = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.evidencereferences.id !== null) {
                Evidencereferences.update(vm.evidencereferences, onSaveSuccess, onSaveError);
            } else {
                Evidencereferences.save(vm.evidencereferences, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('probioticApp:evidencereferencesUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.timestamp = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
