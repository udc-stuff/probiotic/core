(function() {
    'use strict';

    angular
        .module('probioticApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('evidencereferences', {
            parent: 'entity',
            url: '/evidencereferences?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'probioticApp.evidencereferences.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evidencereferences/evidencereferences.html',
                    controller: 'EvidencereferencesController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evidencereferences');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        /*.state('evidencereferences-detail', {
            parent: 'evidencereferences',
            url: '/evidencereferences/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'probioticApp.evidencereferences.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/evidencereferences/evidencereferences-detail.html',
                    controller: 'EvidencereferencesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('evidencereferences');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Evidencereferences', function($stateParams, Evidencereferences) {
                    return Evidencereferences.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'evidencereferences',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('evidencereferences-detail.edit', {
            parent: 'evidencereferences-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencereferences/evidencereferences-dialog.html',
                    controller: 'EvidencereferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evidencereferences', function(Evidencereferences) {
                            return Evidencereferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })*/
        .state('evidencereferences.new', {
            parent: 'evidencereferences',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencereferences/evidencereferences-dialog.html',
                    controller: 'EvidencereferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                text: null,
                                timestamp: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('evidencereferences', null, { reload: 'evidencereferences' });
                }, function() {
                    $state.go('evidencereferences');
                });
            }]
        })
        .state('evidencereferences.edit', {
            parent: 'evidencereferences',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencereferences/evidencereferences-dialog.html',
                    controller: 'EvidencereferencesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Evidencereferences', function(Evidencereferences) {
                            return Evidencereferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evidencereferences', null, { reload: 'evidencereferences' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('evidencereferences.delete', {
            parent: 'evidencereferences',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/evidencereferences/evidencereferences-delete-dialog.html',
                    controller: 'EvidencereferencesDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Evidencereferences', function(Evidencereferences) {
                            return Evidencereferences.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('evidencereferences', null, { reload: 'evidencereferences' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
