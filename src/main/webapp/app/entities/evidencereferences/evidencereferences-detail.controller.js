(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencereferencesDetailController', EvidencereferencesDetailController);

    EvidencereferencesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Evidencereferences', 'User'];

    function EvidencereferencesDetailController($scope, $rootScope, $stateParams, previousState, entity, Evidencereferences, User) {
        var vm = this;

        vm.evidencereferences = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('probioticApp:evidencereferencesUpdate', function(event, result) {
            vm.evidencereferences = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
