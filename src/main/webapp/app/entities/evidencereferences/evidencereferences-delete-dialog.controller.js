(function() {
    'use strict';

    angular
        .module('probioticApp')
        .controller('EvidencereferencesDeleteController',EvidencereferencesDeleteController);

    EvidencereferencesDeleteController.$inject = ['$uibModalInstance', 'entity', 'Evidencereferences'];

    function EvidencereferencesDeleteController($uibModalInstance, entity, Evidencereferences) {
        var vm = this;

        vm.evidencereferences = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Evidencereferences.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
