(function() {
    'use strict';

    angular
        .module('probioticApp')
        .factory('EvidencereferencesSearch', EvidencereferencesSearch);

    EvidencereferencesSearch.$inject = ['$resource'];

    function EvidencereferencesSearch($resource) {
        var resourceUrl =  'api/_search/evidencereferences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
