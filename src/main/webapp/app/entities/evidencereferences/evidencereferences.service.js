(function() {
    'use strict';
    angular
        .module('probioticApp')
        .factory('Evidencereferences', Evidencereferences);

    Evidencereferences.$inject = ['$resource', 'DateUtils'];

    function Evidencereferences ($resource, DateUtils) {
        var resourceUrl =  'api/evidencereferences/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.timestamp = DateUtils.convertDateTimeFromServer(data.timestamp);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
