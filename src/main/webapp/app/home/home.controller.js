(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['Principal', 'Pathologytype', 'Pathology', '$scope', '$state', 'LoginService', 'AlertService', 'ParseLinks', 'paginationConstants', 'pagingParams'];

    function HomeController(Principal, Pathologytype, Pathology, $scope, $state, LoginService, AlertService, ParseLinks, paginationConstants, pagingParams) {
        var vm = this;

        vm.selection = [];
        vm.currentSelection = currentSelection;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;

        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        loadAllPathologyTypes();

        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function register() {
            $state.go('register');
        }

        function loadAllPathologyTypes() {
            Pathologytype.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.pathologytypes = data;
                vm.page = pagingParams.page;

                loadAllPathology();
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }


        function loadAllPathology() {

            Pathology.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.pathologies = data;
                vm.page = pagingParams.page;

                matchPathologies();
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function matchPathologies() {

            vm.pathologytypes.map(
                function callback(currentValue) {

                    currentValue.pathologies =
                        vm.pathologies.filter(
                            function callback(currentValue) {
                                return currentValue.pathologytypeId == this.id;
                            }, currentValue
                        );

                    return currentValue;
                }
            );

                // loadAllProbiotics();
        }


        function currentSelection(pathologyId) {
            var idx = vm.selection.indexOf(pathologyId);

            if (idx > -1) {
                vm.selection.splice(idx, 1);
            }
            else {
                vm.selection.push(pathologyId);
            }
        }

       /* function loadAllProbiotics(){

            Probiotic.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.probiotics = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }

        }
*/
    }
})();
