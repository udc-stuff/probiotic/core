(function () {
    'use strict';

    angular
        .module('probioticApp')
        .controller('ElasticsearchReindexController', ElasticsearchReindexController);

    ElasticsearchReindexController.$inject = [];

    function ElasticsearchReindexController() {
    }
})();
