package es.udc.jose.service;

import es.udc.jose.service.dto.EvidenceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Evidence.
 */
public interface EvidenceService {

    /**
     * Save a evidence.
     *
     * @param evidenceDTO the entity to save
     * @return the persisted entity
     */
    EvidenceDTO save(EvidenceDTO evidenceDTO);

    /**
     * Get all the evidences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidenceDTO> findAll(Pageable pageable);

    /**
     * Get the "id" evidence.
     *
     * @param id the id of the entity
     * @return the entity
     */
    EvidenceDTO findOne(Long id);

    /**
     * Delete the "id" evidence.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the evidence corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidenceDTO> search(String query, Pageable pageable);
}
