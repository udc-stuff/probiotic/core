package es.udc.jose.service;

import es.udc.jose.service.dto.DosageDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Dosage.
 */
public interface DosageService {

    /**
     * Save a dosage.
     *
     * @param dosageDTO the entity to save
     * @return the persisted entity
     */
    DosageDTO save(DosageDTO dosageDTO);

    /**
     * Get all the dosages.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DosageDTO> findAll(Pageable pageable);

    /**
     * Get the "id" dosage.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DosageDTO findOne(Long id);

    /**
     * Delete the "id" dosage.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the dosage corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DosageDTO> search(String query, Pageable pageable);
}
