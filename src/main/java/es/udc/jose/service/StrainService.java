package es.udc.jose.service;

import es.udc.jose.service.dto.StrainDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Strain.
 */
public interface StrainService {

    /**
     * Save a strain.
     *
     * @param strainDTO the entity to save
     * @return the persisted entity
     */
    StrainDTO save(StrainDTO strainDTO);

    /**
     * Get all the strains.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StrainDTO> findAll(Pageable pageable);

    /**
     * Get the "id" strain.
     *
     * @param id the id of the entity
     * @return the entity
     */
    StrainDTO findOne(Long id);

    /**
     * Delete the "id" strain.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the strain corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StrainDTO> search(String query, Pageable pageable);
}
