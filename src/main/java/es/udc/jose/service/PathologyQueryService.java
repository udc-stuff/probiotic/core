package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Pathology;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.PathologyRepository;
import es.udc.jose.repository.search.PathologySearchRepository;
import es.udc.jose.service.dto.PathologyCriteria;

import es.udc.jose.service.dto.PathologyDTO;
import es.udc.jose.service.mapper.PathologyMapper;

/**
 * Service for executing complex queries for Pathology entities in the database.
 * The main input is a {@link PathologyCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PathologyDTO} or a {@link Page} of {@link PathologyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PathologyQueryService extends QueryService<Pathology> {

    private final Logger log = LoggerFactory.getLogger(PathologyQueryService.class);


    private final PathologyRepository pathologyRepository;

    private final PathologyMapper pathologyMapper;

    private final PathologySearchRepository pathologySearchRepository;

    public PathologyQueryService(PathologyRepository pathologyRepository, PathologyMapper pathologyMapper, PathologySearchRepository pathologySearchRepository) {
        this.pathologyRepository = pathologyRepository;
        this.pathologyMapper = pathologyMapper;
        this.pathologySearchRepository = pathologySearchRepository;
    }

    /**
     * Return a {@link List} of {@link PathologyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PathologyDTO> findByCriteria(PathologyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Pathology> specification = createSpecification(criteria);
        return pathologyMapper.toDto(pathologyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PathologyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PathologyDTO> findByCriteria(PathologyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Pathology> specification = createSpecification(criteria);
        final Page<Pathology> result = pathologyRepository.findAll(specification, page);
        return result.map(pathologyMapper::toDto);
    }

    /**
     * Function to convert PathologyCriteria to a {@link Specifications}
     */
    private Specifications<Pathology> createSpecification(PathologyCriteria criteria) {
        Specifications<Pathology> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Pathology_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Pathology_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Pathology_.timestamp));
            }
            if (criteria.getAcronym() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAcronym(), Pathology_.acronym));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Pathology_.modifiedby, User_.id));
            }
            if (criteria.getPathologytypeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getPathologytypeId(), Pathology_.pathologytype, Pathologytype_.id));
            }
        }
        return specification;
    }

}
