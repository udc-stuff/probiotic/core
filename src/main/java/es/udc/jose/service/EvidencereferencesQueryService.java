package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Evidencereferences;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.EvidencereferencesRepository;
import es.udc.jose.repository.search.EvidencereferencesSearchRepository;
import es.udc.jose.service.dto.EvidencereferencesCriteria;

import es.udc.jose.service.dto.EvidencereferencesDTO;
import es.udc.jose.service.mapper.EvidencereferencesMapper;

/**
 * Service for executing complex queries for Evidencereferences entities in the database.
 * The main input is a {@link EvidencereferencesCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EvidencereferencesDTO} or a {@link Page} of {@link EvidencereferencesDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EvidencereferencesQueryService extends QueryService<Evidencereferences> {

    private final Logger log = LoggerFactory.getLogger(EvidencereferencesQueryService.class);


    private final EvidencereferencesRepository evidencereferencesRepository;

    private final EvidencereferencesMapper evidencereferencesMapper;

    private final EvidencereferencesSearchRepository evidencereferencesSearchRepository;

    public EvidencereferencesQueryService(EvidencereferencesRepository evidencereferencesRepository, EvidencereferencesMapper evidencereferencesMapper, EvidencereferencesSearchRepository evidencereferencesSearchRepository) {
        this.evidencereferencesRepository = evidencereferencesRepository;
        this.evidencereferencesMapper = evidencereferencesMapper;
        this.evidencereferencesSearchRepository = evidencereferencesSearchRepository;
    }

    /**
     * Return a {@link List} of {@link EvidencereferencesDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EvidencereferencesDTO> findByCriteria(EvidencereferencesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Evidencereferences> specification = createSpecification(criteria);
        return evidencereferencesMapper.toDto(evidencereferencesRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EvidencereferencesDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EvidencereferencesDTO> findByCriteria(EvidencereferencesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Evidencereferences> specification = createSpecification(criteria);
        final Page<Evidencereferences> result = evidencereferencesRepository.findAll(specification, page);
        return result.map(evidencereferencesMapper::toDto);
    }

    /**
     * Function to convert EvidencereferencesCriteria to a {@link Specifications}
     */
    private Specifications<Evidencereferences> createSpecification(EvidencereferencesCriteria criteria) {
        Specifications<Evidencereferences> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Evidencereferences_.id));
            }
            if (criteria.getText() != null) {
                specification = specification.and(buildStringSpecification(criteria.getText(), Evidencereferences_.text));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Evidencereferences_.timestamp));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Evidencereferences_.modifiedby, User_.id));
            }
        }
        return specification;
    }

}
