package es.udc.jose.service;

import es.udc.jose.service.dto.EvidencereferencesDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Evidencereferences.
 */
public interface EvidencereferencesService {

    /**
     * Save a evidencereferences.
     *
     * @param evidencereferencesDTO the entity to save
     * @return the persisted entity
     */
    EvidencereferencesDTO save(EvidencereferencesDTO evidencereferencesDTO);

    /**
     * Get all the evidencereferences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidencereferencesDTO> findAll(Pageable pageable);

    /**
     * Get the "id" evidencereferences.
     *
     * @param id the id of the entity
     * @return the entity
     */
    EvidencereferencesDTO findOne(Long id);

    /**
     * Delete the "id" evidencereferences.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the evidencereferences corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidencereferencesDTO> search(String query, Pageable pageable);
}
