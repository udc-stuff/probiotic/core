package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Strain;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.StrainRepository;
import es.udc.jose.repository.search.StrainSearchRepository;
import es.udc.jose.service.dto.StrainCriteria;

import es.udc.jose.service.dto.StrainDTO;
import es.udc.jose.service.mapper.StrainMapper;

/**
 * Service for executing complex queries for Strain entities in the database.
 * The main input is a {@link StrainCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link StrainDTO} or a {@link Page} of {@link StrainDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StrainQueryService extends QueryService<Strain> {

    private final Logger log = LoggerFactory.getLogger(StrainQueryService.class);


    private final StrainRepository strainRepository;

    private final StrainMapper strainMapper;

    private final StrainSearchRepository strainSearchRepository;

    public StrainQueryService(StrainRepository strainRepository, StrainMapper strainMapper, StrainSearchRepository strainSearchRepository) {
        this.strainRepository = strainRepository;
        this.strainMapper = strainMapper;
        this.strainSearchRepository = strainSearchRepository;
    }

    /**
     * Return a {@link List} of {@link StrainDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<StrainDTO> findByCriteria(StrainCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Strain> specification = createSpecification(criteria);
        return strainMapper.toDto(strainRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link StrainDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<StrainDTO> findByCriteria(StrainCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Strain> specification = createSpecification(criteria);
        final Page<Strain> result = strainRepository.findAll(specification, page);
        return result.map(strainMapper::toDto);
    }

    /**
     * Function to convert StrainCriteria to a {@link Specifications}
     */
    private Specifications<Strain> createSpecification(StrainCriteria criteria) {
        Specifications<Strain> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Strain_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Strain_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Strain_.timestamp));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Strain_.modifiedby, User_.id));
            }
        }
        return specification;
    }

}
