package es.udc.jose.service;

import es.udc.jose.service.dto.ProbioticDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Probiotic.
 */
public interface ProbioticService {

    /**
     * Save a probiotic.
     *
     * @param probioticDTO the entity to save
     * @return the persisted entity
     */
    ProbioticDTO save(ProbioticDTO probioticDTO);

    /**
     * Get all the probiotics.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ProbioticDTO> findAll(Pageable pageable);

    /**
     * Get the "id" probiotic.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ProbioticDTO findOne(Long id);

    /**
     * Delete the "id" probiotic.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the probiotic corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<ProbioticDTO> search(String query, Pageable pageable);
}
