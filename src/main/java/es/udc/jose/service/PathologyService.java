package es.udc.jose.service;

import es.udc.jose.service.dto.PathologyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Pathology.
 */
public interface PathologyService {

    /**
     * Save a pathology.
     *
     * @param pathologyDTO the entity to save
     * @return the persisted entity
     */
    PathologyDTO save(PathologyDTO pathologyDTO);

    /**
     * Get all the pathologies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PathologyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" pathology.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PathologyDTO findOne(Long id);

    /**
     * Delete the "id" pathology.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pathology corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PathologyDTO> search(String query, Pageable pageable);
}
