package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Evidence;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.EvidenceRepository;
import es.udc.jose.repository.search.EvidenceSearchRepository;
import es.udc.jose.service.dto.EvidenceCriteria;

import es.udc.jose.service.dto.EvidenceDTO;
import es.udc.jose.service.mapper.EvidenceMapper;

/**
 * Service for executing complex queries for Evidence entities in the database.
 * The main input is a {@link EvidenceCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EvidenceDTO} or a {@link Page} of {@link EvidenceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EvidenceQueryService extends QueryService<Evidence> {

    private final Logger log = LoggerFactory.getLogger(EvidenceQueryService.class);


    private final EvidenceRepository evidenceRepository;

    private final EvidenceMapper evidenceMapper;

    private final EvidenceSearchRepository evidenceSearchRepository;

    public EvidenceQueryService(EvidenceRepository evidenceRepository, EvidenceMapper evidenceMapper, EvidenceSearchRepository evidenceSearchRepository) {
        this.evidenceRepository = evidenceRepository;
        this.evidenceMapper = evidenceMapper;
        this.evidenceSearchRepository = evidenceSearchRepository;
    }

    /**
     * Return a {@link List} of {@link EvidenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EvidenceDTO> findByCriteria(EvidenceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Evidence> specification = createSpecification(criteria);
        return evidenceMapper.toDto(evidenceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EvidenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EvidenceDTO> findByCriteria(EvidenceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Evidence> specification = createSpecification(criteria);
        final Page<Evidence> result = evidenceRepository.findAll(specification, page);
        return result.map(evidenceMapper::toDto);
    }

    /**
     * Function to convert EvidenceCriteria to a {@link Specifications}
     */
    private Specifications<Evidence> createSpecification(EvidenceCriteria criteria) {
        Specifications<Evidence> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Evidence_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Evidence_.timestamp));
            }
            if (criteria.getPathologyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getPathologyId(), Evidence_.pathology, Pathology_.id));
            }
            if (criteria.getDosageId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getDosageId(), Evidence_.dosage, Dosage_.id));
            }
            if (criteria.getEvidencereferencesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getEvidencereferencesId(), Evidence_.evidencereferences, Evidencereferences_.id));
            }
            if (criteria.getEvidencetypeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getEvidencetypeId(), Evidence_.evidencetype, Evidencetype_.id));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Evidence_.modifiedby, User_.id));
            }
        }
        return specification;
    }

}
