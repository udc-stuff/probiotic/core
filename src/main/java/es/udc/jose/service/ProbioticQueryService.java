package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Probiotic;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.ProbioticRepository;
import es.udc.jose.repository.search.ProbioticSearchRepository;
import es.udc.jose.service.dto.ProbioticCriteria;

import es.udc.jose.service.dto.ProbioticDTO;
import es.udc.jose.service.mapper.ProbioticMapper;

/**
 * Service for executing complex queries for Probiotic entities in the database.
 * The main input is a {@link ProbioticCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProbioticDTO} or a {@link Page} of {@link ProbioticDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProbioticQueryService extends QueryService<Probiotic> {

    private final Logger log = LoggerFactory.getLogger(ProbioticQueryService.class);


    private final ProbioticRepository probioticRepository;

    private final ProbioticMapper probioticMapper;

    private final ProbioticSearchRepository probioticSearchRepository;

    public ProbioticQueryService(ProbioticRepository probioticRepository, ProbioticMapper probioticMapper, ProbioticSearchRepository probioticSearchRepository) {
        this.probioticRepository = probioticRepository;
        this.probioticMapper = probioticMapper;
        this.probioticSearchRepository = probioticSearchRepository;
    }

    /**
     * Return a {@link List} of {@link ProbioticDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProbioticDTO> findByCriteria(ProbioticCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Probiotic> specification = createSpecification(criteria);
        return probioticMapper.toDto(probioticRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProbioticDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProbioticDTO> findByCriteria(ProbioticCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Probiotic> specification = createSpecification(criteria);
        final Page<Probiotic> result = probioticRepository.findAll(specification, page);
        return result.map(probioticMapper::toDto);
    }

    /**
     * Function to convert ProbioticCriteria to a {@link Specifications}
     */
    private Specifications<Probiotic> createSpecification(ProbioticCriteria criteria) {
        Specifications<Probiotic> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Probiotic_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Probiotic_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Probiotic_.timestamp));
            }
            if (criteria.getHave_allergens() != null) {
                specification = specification.and(buildSpecification(criteria.getHave_allergens(), Probiotic_.have_allergens));
            }
            if (criteria.getAllergensId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getAllergensId(), Probiotic_.allergens, Allergens_.id));
            }
            if (criteria.getStrainId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getStrainId(), Probiotic_.strains, Strain_.id));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Probiotic_.modifiedby, User_.id));
            }
        }
        return specification;
    }

}
