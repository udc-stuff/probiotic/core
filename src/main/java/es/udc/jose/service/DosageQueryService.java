package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Dosage;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.DosageRepository;
import es.udc.jose.repository.search.DosageSearchRepository;
import es.udc.jose.service.dto.DosageCriteria;

import es.udc.jose.service.dto.DosageDTO;
import es.udc.jose.service.mapper.DosageMapper;
import es.udc.jose.domain.enumeration.Age;

/**
 * Service for executing complex queries for Dosage entities in the database.
 * The main input is a {@link DosageCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DosageDTO} or a {@link Page} of {@link DosageDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DosageQueryService extends QueryService<Dosage> {

    private final Logger log = LoggerFactory.getLogger(DosageQueryService.class);


    private final DosageRepository dosageRepository;

    private final DosageMapper dosageMapper;

    private final DosageSearchRepository dosageSearchRepository;

    public DosageQueryService(DosageRepository dosageRepository, DosageMapper dosageMapper, DosageSearchRepository dosageSearchRepository) {
        this.dosageRepository = dosageRepository;
        this.dosageMapper = dosageMapper;
        this.dosageSearchRepository = dosageSearchRepository;
    }

    /**
     * Return a {@link List} of {@link DosageDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DosageDTO> findByCriteria(DosageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Dosage> specification = createSpecification(criteria);
        return dosageMapper.toDto(dosageRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DosageDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DosageDTO> findByCriteria(DosageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Dosage> specification = createSpecification(criteria);
        final Page<Dosage> result = dosageRepository.findAll(specification, page);
        return result.map(dosageMapper::toDto);
    }

    /**
     * Function to convert DosageCriteria to a {@link Specifications}
     */
    private Specifications<Dosage> createSpecification(DosageCriteria criteria) {
        Specifications<Dosage> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Dosage_.id));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Dosage_.timestamp));
            }
            if (criteria.getAge() != null) {
                specification = specification.and(buildSpecification(criteria.getAge(), Dosage_.age));
            }
            if (criteria.getIntake() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIntake(), Dosage_.intake));
            }
            if (criteria.getFrequency() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFrequency(), Dosage_.frequency));
            }
            if (criteria.getIndication() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndication(), Dosage_.indication));
            }
            if (criteria.getCfu() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCfu(), Dosage_.cfu));
            }
            if (criteria.getProbioticId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProbioticId(), Dosage_.probiotic, Probiotic_.id));
            }
            if (criteria.getDoseformId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getDoseformId(), Dosage_.doseform, Doseform_.id));
            }
            if (criteria.getModifiedbyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getModifiedbyId(), Dosage_.modifiedby, User_.id));
            }
        }
        return specification;
    }

}
