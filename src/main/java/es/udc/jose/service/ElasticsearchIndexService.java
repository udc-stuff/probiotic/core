package es.udc.jose.service;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonIgnore;
import es.udc.jose.domain.*;
import es.udc.jose.repository.*;
import es.udc.jose.repository.search.*;
import org.elasticsearch.indices.IndexAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.ManyToMany;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ElasticsearchIndexService {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);

    private final AllergensRepository allergensRepository;

    private final AllergensSearchRepository allergensSearchRepository;

    private final DosageRepository dosageRepository;

    private final DosageSearchRepository dosageSearchRepository;

    private final DoseformRepository doseformRepository;

    private final DoseformSearchRepository doseformSearchRepository;

    private final EvidenceRepository evidenceRepository;

    private final EvidenceSearchRepository evidenceSearchRepository;

    private final EvidencereferencesRepository evidencereferencesRepository;

    private final EvidencereferencesSearchRepository evidencereferencesSearchRepository;

    private final EvidencetypeRepository evidencetypeRepository;

    private final EvidencetypeSearchRepository evidencetypeSearchRepository;

    private final PathologyRepository pathologyRepository;

    private final PathologySearchRepository pathologySearchRepository;

    private final PathologytypeRepository pathologytypeRepository;

    private final PathologytypeSearchRepository pathologytypeSearchRepository;

    private final ProbioticRepository probioticRepository;

    private final ProbioticSearchRepository probioticSearchRepository;

    private final StrainRepository strainRepository;

    private final StrainSearchRepository strainSearchRepository;

    private final UserRepository userRepository;

    private final UserSearchRepository userSearchRepository;

    private final ElasticsearchTemplate elasticsearchTemplate;

    private static final Lock reindexLock = new ReentrantLock();

    public ElasticsearchIndexService(
        UserRepository userRepository,
        UserSearchRepository userSearchRepository,
        AllergensRepository allergensRepository,
        AllergensSearchRepository allergensSearchRepository,
        DosageRepository dosageRepository,
        DosageSearchRepository dosageSearchRepository,
        DoseformRepository doseformRepository,
        DoseformSearchRepository doseformSearchRepository,
        EvidenceRepository evidenceRepository,
        EvidenceSearchRepository evidenceSearchRepository,
        EvidencereferencesRepository evidencereferencesRepository,
        EvidencereferencesSearchRepository evidencereferencesSearchRepository,
        EvidencetypeRepository evidencetypeRepository,
        EvidencetypeSearchRepository evidencetypeSearchRepository,
        PathologyRepository pathologyRepository,
        PathologySearchRepository pathologySearchRepository,
        PathologytypeRepository pathologytypeRepository,
        PathologytypeSearchRepository pathologytypeSearchRepository,
        ProbioticRepository probioticRepository,
        ProbioticSearchRepository probioticSearchRepository,
        StrainRepository strainRepository,
        StrainSearchRepository strainSearchRepository,
        ElasticsearchTemplate elasticsearchTemplate) {
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.allergensRepository = allergensRepository;
        this.allergensSearchRepository = allergensSearchRepository;
        this.dosageRepository = dosageRepository;
        this.dosageSearchRepository = dosageSearchRepository;
        this.doseformRepository = doseformRepository;
        this.doseformSearchRepository = doseformSearchRepository;
        this.evidenceRepository = evidenceRepository;
        this.evidenceSearchRepository = evidenceSearchRepository;
        this.evidencereferencesRepository = evidencereferencesRepository;
        this.evidencereferencesSearchRepository = evidencereferencesSearchRepository;
        this.evidencetypeRepository = evidencetypeRepository;
        this.evidencetypeSearchRepository = evidencetypeSearchRepository;
        this.pathologyRepository = pathologyRepository;
        this.pathologySearchRepository = pathologySearchRepository;
        this.pathologytypeRepository = pathologytypeRepository;
        this.pathologytypeSearchRepository = pathologytypeSearchRepository;
        this.probioticRepository = probioticRepository;
        this.probioticSearchRepository = probioticSearchRepository;
        this.strainRepository = strainRepository;
        this.strainSearchRepository = strainSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Async
    @Timed
    public void reindexAll() {
        if(reindexLock.tryLock()) {
            try {
                reindexForClass(Allergens.class, allergensRepository, allergensSearchRepository);
                reindexForClass(Dosage.class, dosageRepository, dosageSearchRepository);
                reindexForClass(Doseform.class, doseformRepository, doseformSearchRepository);
                reindexForClass(Evidence.class, evidenceRepository, evidenceSearchRepository);
                reindexForClass(Evidencereferences.class, evidencereferencesRepository, evidencereferencesSearchRepository);
                reindexForClass(Evidencetype.class, evidencetypeRepository, evidencetypeSearchRepository);
                reindexForClass(Pathology.class, pathologyRepository, pathologySearchRepository);
                reindexForClass(Pathologytype.class, pathologytypeRepository, pathologytypeSearchRepository);
                reindexForClass(Probiotic.class, probioticRepository, probioticSearchRepository);
                reindexForClass(Strain.class, strainRepository, strainSearchRepository);
                reindexForClass(User.class, userRepository, userSearchRepository);

                log.info("Elasticsearch: Successfully performed reindexing");
            } finally {
                reindexLock.unlock();
            }
        } else {
            log.info("Elasticsearch: concurrent reindexing attempt");
        }
    }

    @SuppressWarnings("unchecked")
    private <T, ID extends Serializable> void reindexForClass(Class<T> entityClass, JpaRepository<T, ID> jpaRepository,
                                                              ElasticsearchRepository<T, ID> elasticsearchRepository) {
        elasticsearchTemplate.deleteIndex(entityClass);
        try {
            elasticsearchTemplate.createIndex(entityClass);
        } catch (IndexAlreadyExistsException e) {
            // Do nothing. Index was already concurrently recreated by some other service.
        }
        elasticsearchTemplate.putMapping(entityClass);
        if (jpaRepository.count() > 0) {
            // if a JHipster entity field is the owner side of a many-to-many relationship, it should be loaded manually
            List<Method> relationshipGetters = Arrays.stream(entityClass.getDeclaredFields())
                .filter(field -> field.getType().equals(Set.class))
                .filter(field -> field.getAnnotation(ManyToMany.class) != null)
                .filter(field -> field.getAnnotation(ManyToMany.class).mappedBy().isEmpty())
                .filter(field -> field.getAnnotation(JsonIgnore.class) == null)
                .map(field -> {
                    try {
                        return new PropertyDescriptor(field.getName(), entityClass).getReadMethod();
                    } catch (IntrospectionException e) {
                        log.error("Error retrieving getter for class {}, field {}. Field will NOT be indexed",
                            entityClass.getSimpleName(), field.getName(), e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

            int size = 100;
            for (int i = 0; i <= jpaRepository.count() / size; i++) {
                Pageable page = new PageRequest(i, size);
                log.info("Indexing page {} of {}, size {}", i, jpaRepository.count() / size, size);
                Page<T> results = jpaRepository.findAll(page);
                results.map(result -> {
                    // if there are any relationships to load, do it now
                    relationshipGetters.forEach(method -> {
                        try {
                            // eagerly load the relationship set
                            ((Set) method.invoke(result)).size();
                        } catch (Exception ex) {
                            log.error(ex.getMessage());
                        }
                    });
                    return result;
                });
                elasticsearchRepository.save(results.getContent());
            }
        }
        log.info("Elasticsearch: Indexed all rows for {}", entityClass.getSimpleName());
    }
}
