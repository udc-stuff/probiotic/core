package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Pathologytype;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.PathologytypeRepository;
import es.udc.jose.repository.search.PathologytypeSearchRepository;
import es.udc.jose.service.dto.PathologytypeCriteria;

import es.udc.jose.service.dto.PathologytypeDTO;
import es.udc.jose.service.mapper.PathologytypeMapper;

/**
 * Service for executing complex queries for Pathologytype entities in the database.
 * The main input is a {@link PathologytypeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PathologytypeDTO} or a {@link Page} of {@link PathologytypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PathologytypeQueryService extends QueryService<Pathologytype> {

    private final Logger log = LoggerFactory.getLogger(PathologytypeQueryService.class);


    private final PathologytypeRepository pathologytypeRepository;

    private final PathologytypeMapper pathologytypeMapper;

    private final PathologytypeSearchRepository pathologytypeSearchRepository;

    public PathologytypeQueryService(PathologytypeRepository pathologytypeRepository, PathologytypeMapper pathologytypeMapper, PathologytypeSearchRepository pathologytypeSearchRepository) {
        this.pathologytypeRepository = pathologytypeRepository;
        this.pathologytypeMapper = pathologytypeMapper;
        this.pathologytypeSearchRepository = pathologytypeSearchRepository;
    }

    /**
     * Return a {@link List} of {@link PathologytypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PathologytypeDTO> findByCriteria(PathologytypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Pathologytype> specification = createSpecification(criteria);
        return pathologytypeMapper.toDto(pathologytypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PathologytypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PathologytypeDTO> findByCriteria(PathologytypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Pathologytype> specification = createSpecification(criteria);
        final Page<Pathologytype> result = pathologytypeRepository.findAll(specification, page);
        return result.map(pathologytypeMapper::toDto);
    }

    /**
     * Function to convert PathologytypeCriteria to a {@link Specifications}
     */
    private Specifications<Pathologytype> createSpecification(PathologytypeCriteria criteria) {
        Specifications<Pathologytype> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Pathologytype_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Pathologytype_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Pathologytype_.timestamp));
            }
        }
        return specification;
    }

}
