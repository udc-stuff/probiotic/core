package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.PathologyService;
import es.udc.jose.domain.Pathology;
import es.udc.jose.repository.PathologyRepository;
import es.udc.jose.repository.search.PathologySearchRepository;
import es.udc.jose.service.dto.PathologyDTO;
import es.udc.jose.service.mapper.PathologyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Pathology.
 */
@Service
@Transactional
public class PathologyServiceImpl implements PathologyService {

    private final Logger log = LoggerFactory.getLogger(PathologyServiceImpl.class);

    private final PathologyRepository pathologyRepository;

    private final PathologyMapper pathologyMapper;

    private final PathologySearchRepository pathologySearchRepository;

    public PathologyServiceImpl(PathologyRepository pathologyRepository, PathologyMapper pathologyMapper, PathologySearchRepository pathologySearchRepository) {
        this.pathologyRepository = pathologyRepository;
        this.pathologyMapper = pathologyMapper;
        this.pathologySearchRepository = pathologySearchRepository;
    }

    /**
     * Save a pathology.
     *
     * @param pathologyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PathologyDTO save(PathologyDTO pathologyDTO) {
        pathologyDTO.setTimestamp(ZonedDateTime.now());
        pathologyDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Pathology : {}", pathologyDTO);
        Pathology pathology = pathologyMapper.toEntity(pathologyDTO);
        pathology = pathologyRepository.save(pathology);
        PathologyDTO result = pathologyMapper.toDto(pathology);
        pathologySearchRepository.save(pathology);
        return result;
    }

    /**
     * Get all the pathologies.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PathologyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pathologies");
        return pathologyRepository.findAll(pageable)
            .map(pathologyMapper::toDto);
    }

    /**
     * Get one pathology by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PathologyDTO findOne(Long id) {
        log.debug("Request to get Pathology : {}", id);
        Pathology pathology = pathologyRepository.findOne(id);
        return pathologyMapper.toDto(pathology);
    }

    /**
     * Delete the pathology by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pathology : {}", id);
        pathologyRepository.delete(id);
        pathologySearchRepository.delete(id);
    }

    /**
     * Search for the pathology corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PathologyDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pathologies for query {}", query);
        Page<Pathology> result = pathologySearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pathologyMapper::toDto);
    }
}
