package es.udc.jose.service.impl;

import es.udc.jose.service.EvidencetypeService;
import es.udc.jose.domain.Evidencetype;
import es.udc.jose.repository.EvidencetypeRepository;
import es.udc.jose.repository.search.EvidencetypeSearchRepository;
import es.udc.jose.service.dto.EvidencetypeDTO;
import es.udc.jose.service.mapper.EvidencetypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Evidencetype.
 */
@Service
@Transactional
public class EvidencetypeServiceImpl implements EvidencetypeService {

    private final Logger log = LoggerFactory.getLogger(EvidencetypeServiceImpl.class);

    private final EvidencetypeRepository evidencetypeRepository;

    private final EvidencetypeMapper evidencetypeMapper;

    private final EvidencetypeSearchRepository evidencetypeSearchRepository;

    public EvidencetypeServiceImpl(EvidencetypeRepository evidencetypeRepository, EvidencetypeMapper evidencetypeMapper, EvidencetypeSearchRepository evidencetypeSearchRepository) {
        this.evidencetypeRepository = evidencetypeRepository;
        this.evidencetypeMapper = evidencetypeMapper;
        this.evidencetypeSearchRepository = evidencetypeSearchRepository;
    }

    /**
     * Save a evidencetype.
     *
     * @param evidencetypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EvidencetypeDTO save(EvidencetypeDTO evidencetypeDTO) {
        evidencetypeDTO.setTimestamp(ZonedDateTime.now());
        log.debug("Request to save Evidencetype : {}", evidencetypeDTO);
        Evidencetype evidencetype = evidencetypeMapper.toEntity(evidencetypeDTO);
        evidencetype = evidencetypeRepository.save(evidencetype);
        EvidencetypeDTO result = evidencetypeMapper.toDto(evidencetype);
        evidencetypeSearchRepository.save(evidencetype);
        return result;
    }

    /**
     * Get all the evidencetypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidencetypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Evidencetypes");
        return evidencetypeRepository.findAll(pageable)
            .map(evidencetypeMapper::toDto);
    }

    /**
     * Get one evidencetype by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EvidencetypeDTO findOne(Long id) {
        log.debug("Request to get Evidencetype : {}", id);
        Evidencetype evidencetype = evidencetypeRepository.findOne(id);
        return evidencetypeMapper.toDto(evidencetype);
    }

    /**
     * Delete the evidencetype by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Evidencetype : {}", id);
        evidencetypeRepository.delete(id);
        evidencetypeSearchRepository.delete(id);
    }

    /**
     * Search for the evidencetype corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidencetypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Evidencetypes for query {}", query);
        Page<Evidencetype> result = evidencetypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(evidencetypeMapper::toDto);
    }
}
