package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.DosageService;
import es.udc.jose.domain.Dosage;
import es.udc.jose.repository.DosageRepository;
import es.udc.jose.repository.search.DosageSearchRepository;
import es.udc.jose.service.dto.DosageDTO;
import es.udc.jose.service.mapper.DosageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Dosage.
 */
@Service
@Transactional
public class DosageServiceImpl implements DosageService {

    private final Logger log = LoggerFactory.getLogger(DosageServiceImpl.class);

    private final DosageRepository dosageRepository;

    private final DosageMapper dosageMapper;

    private final DosageSearchRepository dosageSearchRepository;

    public DosageServiceImpl(DosageRepository dosageRepository, DosageMapper dosageMapper, DosageSearchRepository dosageSearchRepository) {
        this.dosageRepository = dosageRepository;
        this.dosageMapper = dosageMapper;
        this.dosageSearchRepository = dosageSearchRepository;
    }

    /**
     * Save a dosage.
     *
     * @param dosageDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DosageDTO save(DosageDTO dosageDTO) {
        dosageDTO.setTimestamp(ZonedDateTime.now());
        dosageDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Dosage : {}", dosageDTO);
        Dosage dosage = dosageMapper.toEntity(dosageDTO);
        dosage = dosageRepository.save(dosage);
        DosageDTO result = dosageMapper.toDto(dosage);
        dosageSearchRepository.save(dosage);
        return result;
    }

    /**
     * Get all the dosages.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DosageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Dosages");
        return dosageRepository.findAll(pageable)
            .map(dosageMapper::toDto);
    }

    /**
     * Get one dosage by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DosageDTO findOne(Long id) {
        log.debug("Request to get Dosage : {}", id);
        Dosage dosage = dosageRepository.findOne(id);
        return dosageMapper.toDto(dosage);
    }

    /**
     * Delete the dosage by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Dosage : {}", id);
        dosageRepository.delete(id);
        dosageSearchRepository.delete(id);
    }

    /**
     * Search for the dosage corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DosageDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Dosages for query {}", query);
        Page<Dosage> result = dosageSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(dosageMapper::toDto);
    }
}
