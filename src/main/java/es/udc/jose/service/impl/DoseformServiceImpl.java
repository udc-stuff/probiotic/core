package es.udc.jose.service.impl;

import es.udc.jose.service.DoseformService;
import es.udc.jose.domain.Doseform;
import es.udc.jose.repository.DoseformRepository;
import es.udc.jose.repository.search.DoseformSearchRepository;
import es.udc.jose.service.dto.DoseformDTO;
import es.udc.jose.service.mapper.DoseformMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Doseform.
 */
@Service
@Transactional
public class DoseformServiceImpl implements DoseformService {

    private final Logger log = LoggerFactory.getLogger(DoseformServiceImpl.class);

    private final DoseformRepository doseformRepository;

    private final DoseformMapper doseformMapper;

    private final DoseformSearchRepository doseformSearchRepository;

    public DoseformServiceImpl(DoseformRepository doseformRepository, DoseformMapper doseformMapper, DoseformSearchRepository doseformSearchRepository) {
        this.doseformRepository = doseformRepository;
        this.doseformMapper = doseformMapper;
        this.doseformSearchRepository = doseformSearchRepository;
    }

    /**
     * Save a doseform.
     *
     * @param doseformDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DoseformDTO save(DoseformDTO doseformDTO) {
        doseformDTO.setTimestamp(ZonedDateTime.now());
        log.debug("Request to save Doseform : {}", doseformDTO);
        Doseform doseform = doseformMapper.toEntity(doseformDTO);
        doseform = doseformRepository.save(doseform);
        DoseformDTO result = doseformMapper.toDto(doseform);
        doseformSearchRepository.save(doseform);
        return result;
    }

    /**
     * Get all the doseforms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoseformDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Doseforms");
        return doseformRepository.findAll(pageable)
            .map(doseformMapper::toDto);
    }

    /**
     * Get one doseform by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DoseformDTO findOne(Long id) {
        log.debug("Request to get Doseform : {}", id);
        Doseform doseform = doseformRepository.findOne(id);
        return doseformMapper.toDto(doseform);
    }

    /**
     * Delete the doseform by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Doseform : {}", id);
        doseformRepository.delete(id);
        doseformSearchRepository.delete(id);
    }

    /**
     * Search for the doseform corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DoseformDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Doseforms for query {}", query);
        Page<Doseform> result = doseformSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(doseformMapper::toDto);
    }
}
