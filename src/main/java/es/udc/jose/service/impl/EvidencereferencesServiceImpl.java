package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.EvidencereferencesService;
import es.udc.jose.domain.Evidencereferences;
import es.udc.jose.repository.EvidencereferencesRepository;
import es.udc.jose.repository.search.EvidencereferencesSearchRepository;
import es.udc.jose.service.dto.EvidencereferencesDTO;
import es.udc.jose.service.mapper.EvidencereferencesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Evidencereferences.
 */
@Service
@Transactional
public class EvidencereferencesServiceImpl implements EvidencereferencesService {

    private final Logger log = LoggerFactory.getLogger(EvidencereferencesServiceImpl.class);

    private final EvidencereferencesRepository evidencereferencesRepository;

    private final EvidencereferencesMapper evidencereferencesMapper;

    private final EvidencereferencesSearchRepository evidencereferencesSearchRepository;

    public EvidencereferencesServiceImpl(EvidencereferencesRepository evidencereferencesRepository, EvidencereferencesMapper evidencereferencesMapper, EvidencereferencesSearchRepository evidencereferencesSearchRepository) {
        this.evidencereferencesRepository = evidencereferencesRepository;
        this.evidencereferencesMapper = evidencereferencesMapper;
        this.evidencereferencesSearchRepository = evidencereferencesSearchRepository;
    }

    /**
     * Save a evidencereferences.
     *
     * @param evidencereferencesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EvidencereferencesDTO save(EvidencereferencesDTO evidencereferencesDTO) {
        evidencereferencesDTO.setTimestamp(ZonedDateTime.now());
        evidencereferencesDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Evidencereferences : {}", evidencereferencesDTO);
        Evidencereferences evidencereferences = evidencereferencesMapper.toEntity(evidencereferencesDTO);
        evidencereferences = evidencereferencesRepository.save(evidencereferences);
        EvidencereferencesDTO result = evidencereferencesMapper.toDto(evidencereferences);
        evidencereferencesSearchRepository.save(evidencereferences);
        return result;
    }

    /**
     * Get all the evidencereferences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidencereferencesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Evidencereferences");
        return evidencereferencesRepository.findAll(pageable)
            .map(evidencereferencesMapper::toDto);
    }

    /**
     * Get one evidencereferences by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EvidencereferencesDTO findOne(Long id) {
        log.debug("Request to get Evidencereferences : {}", id);
        Evidencereferences evidencereferences = evidencereferencesRepository.findOne(id);
        return evidencereferencesMapper.toDto(evidencereferences);
    }

    /**
     * Delete the evidencereferences by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Evidencereferences : {}", id);
        evidencereferencesRepository.delete(id);
        evidencereferencesSearchRepository.delete(id);
    }

    /**
     * Search for the evidencereferences corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidencereferencesDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Evidencereferences for query {}", query);
        Page<Evidencereferences> result = evidencereferencesSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(evidencereferencesMapper::toDto);
    }
}
