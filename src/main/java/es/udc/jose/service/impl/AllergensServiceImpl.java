package es.udc.jose.service.impl;

import es.udc.jose.service.AllergensService;
import es.udc.jose.domain.Allergens;
import es.udc.jose.repository.AllergensRepository;
import es.udc.jose.repository.search.AllergensSearchRepository;
import es.udc.jose.service.dto.AllergensDTO;
import es.udc.jose.service.mapper.AllergensMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Allergens.
 */
@Service
@Transactional
public class AllergensServiceImpl implements AllergensService {

    private final Logger log = LoggerFactory.getLogger(AllergensServiceImpl.class);

    private final AllergensRepository allergensRepository;

    private final AllergensMapper allergensMapper;

    private final AllergensSearchRepository allergensSearchRepository;

    public AllergensServiceImpl(AllergensRepository allergensRepository, AllergensMapper allergensMapper, AllergensSearchRepository allergensSearchRepository) {
        this.allergensRepository = allergensRepository;
        this.allergensMapper = allergensMapper;
        this.allergensSearchRepository = allergensSearchRepository;
    }

    /**
     * Save a allergens.
     *
     * @param allergensDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AllergensDTO save(AllergensDTO allergensDTO) {
        allergensDTO.setTimestamp(ZonedDateTime.now());
        log.debug("Request to save Allergens : {}", allergensDTO);
        Allergens allergens = allergensMapper.toEntity(allergensDTO);
        allergens = allergensRepository.save(allergens);
        AllergensDTO result = allergensMapper.toDto(allergens);
        allergensSearchRepository.save(allergens);
        return result;
    }

    /**
     * Get all the allergens.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AllergensDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Allergens");
        return allergensRepository.findAll(pageable)
            .map(allergensMapper::toDto);
    }

    /**
     * Get one allergens by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AllergensDTO findOne(Long id) {
        log.debug("Request to get Allergens : {}", id);
        Allergens allergens = allergensRepository.findOne(id);
        return allergensMapper.toDto(allergens);
    }

    /**
     * Delete the allergens by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Allergens : {}", id);
        allergensRepository.delete(id);
        allergensSearchRepository.delete(id);
    }

    /**
     * Search for the allergens corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AllergensDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Allergens for query {}", query);
        Page<Allergens> result = allergensSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(allergensMapper::toDto);
    }
}
