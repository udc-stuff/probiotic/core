package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.ProbioticService;
import es.udc.jose.domain.Probiotic;
import es.udc.jose.repository.ProbioticRepository;
import es.udc.jose.repository.search.ProbioticSearchRepository;
import es.udc.jose.service.dto.ProbioticDTO;
import es.udc.jose.service.mapper.ProbioticMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Probiotic.
 */
@Service
@Transactional
public class ProbioticServiceImpl implements ProbioticService {

    private final Logger log = LoggerFactory.getLogger(ProbioticServiceImpl.class);

    private final ProbioticRepository probioticRepository;

    private final ProbioticMapper probioticMapper;

    private final ProbioticSearchRepository probioticSearchRepository;

    public ProbioticServiceImpl(ProbioticRepository probioticRepository, ProbioticMapper probioticMapper, ProbioticSearchRepository probioticSearchRepository) {
        this.probioticRepository = probioticRepository;
        this.probioticMapper = probioticMapper;
        this.probioticSearchRepository = probioticSearchRepository;
    }

    /**
     * Save a probiotic.
     *
     * @param probioticDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProbioticDTO save(ProbioticDTO probioticDTO) {
        probioticDTO.setTimestamp(ZonedDateTime.now());
        probioticDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Probiotic : {}", probioticDTO);
        Probiotic probiotic = probioticMapper.toEntity(probioticDTO);
        probiotic = probioticRepository.save(probiotic);
        ProbioticDTO result = probioticMapper.toDto(probiotic);
        probioticSearchRepository.save(probiotic);
        return result;
    }

    /**
     * Get all the probiotics.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProbioticDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Probiotics");
        return probioticRepository.findAll(pageable)
            .map(probioticMapper::toDto);
    }

    /**
     * Get one probiotic by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ProbioticDTO findOne(Long id) {
        log.debug("Request to get Probiotic : {}", id);
        Probiotic probiotic = probioticRepository.findOneWithEagerRelationships(id);
        return probioticMapper.toDto(probiotic);
    }

    /**
     * Delete the probiotic by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Probiotic : {}", id);
        probioticRepository.delete(id);
        probioticSearchRepository.delete(id);
    }

    /**
     * Search for the probiotic corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProbioticDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Probiotics for query {}", query);
        Page<Probiotic> result = probioticSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(probioticMapper::toDto);
    }
}
