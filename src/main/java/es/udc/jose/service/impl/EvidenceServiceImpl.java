package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.EvidenceService;
import es.udc.jose.domain.Evidence;
import es.udc.jose.repository.EvidenceRepository;
import es.udc.jose.repository.search.EvidenceSearchRepository;
import es.udc.jose.service.dto.EvidenceDTO;
import es.udc.jose.service.mapper.EvidenceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Evidence.
 */
@Service
@Transactional
public class EvidenceServiceImpl implements EvidenceService {

    private final Logger log = LoggerFactory.getLogger(EvidenceServiceImpl.class);

    private final EvidenceRepository evidenceRepository;

    private final EvidenceMapper evidenceMapper;

    private final EvidenceSearchRepository evidenceSearchRepository;

    public EvidenceServiceImpl(EvidenceRepository evidenceRepository, EvidenceMapper evidenceMapper, EvidenceSearchRepository evidenceSearchRepository) {
        this.evidenceRepository = evidenceRepository;
        this.evidenceMapper = evidenceMapper;
        this.evidenceSearchRepository = evidenceSearchRepository;
    }

    /**
     * Save a evidence.
     *
     * @param evidenceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public EvidenceDTO save(EvidenceDTO evidenceDTO) {
        evidenceDTO.setTimestamp(ZonedDateTime.now());
        evidenceDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Evidence : {}", evidenceDTO);
        Evidence evidence = evidenceMapper.toEntity(evidenceDTO);
        evidence = evidenceRepository.save(evidence);
        EvidenceDTO result = evidenceMapper.toDto(evidence);
        evidenceSearchRepository.save(evidence);
        return result;
    }

    /**
     * Get all the evidences.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidenceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Evidences");
        return evidenceRepository.findAll(pageable)
            .map(evidenceMapper::toDto);
    }

    /**
     * Get one evidence by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public EvidenceDTO findOne(Long id) {
        log.debug("Request to get Evidence : {}", id);
        Evidence evidence = evidenceRepository.findOneWithEagerRelationships(id);
        return evidenceMapper.toDto(evidence);
    }

    /**
     * Delete the evidence by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Evidence : {}", id);
        evidenceRepository.delete(id);
        evidenceSearchRepository.delete(id);
    }

    /**
     * Search for the evidence corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EvidenceDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Evidences for query {}", query);
        Page<Evidence> result = evidenceSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(evidenceMapper::toDto);
    }
}
