package es.udc.jose.service.impl;

import es.udc.jose.security.SecurityUtils;
import es.udc.jose.service.StrainService;
import es.udc.jose.domain.Strain;
import es.udc.jose.repository.StrainRepository;
import es.udc.jose.repository.search.StrainSearchRepository;
import es.udc.jose.service.dto.StrainDTO;
import es.udc.jose.service.mapper.StrainMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Strain.
 */
@Service
@Transactional
public class StrainServiceImpl implements StrainService {

    private final Logger log = LoggerFactory.getLogger(StrainServiceImpl.class);

    private final StrainRepository strainRepository;

    private final StrainMapper strainMapper;

    private final StrainSearchRepository strainSearchRepository;

    public StrainServiceImpl(StrainRepository strainRepository, StrainMapper strainMapper, StrainSearchRepository strainSearchRepository) {
        this.strainRepository = strainRepository;
        this.strainMapper = strainMapper;
        this.strainSearchRepository = strainSearchRepository;
    }

    /**
     * Save a strain.
     *
     * @param strainDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public StrainDTO save(StrainDTO strainDTO) {
        strainDTO.setTimestamp(ZonedDateTime.now());
        strainDTO.setModifiedbyId(SecurityUtils.getCurrentUserId());
        log.debug("Request to save Strain : {}", strainDTO);
        Strain strain = strainMapper.toEntity(strainDTO);
        strain = strainRepository.save(strain);
        StrainDTO result = strainMapper.toDto(strain);
        strainSearchRepository.save(strain);
        return result;
    }

    /**
     * Get all the strains.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StrainDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Strains");
        return strainRepository.findAll(pageable)
            .map(strainMapper::toDto);
    }

    /**
     * Get one strain by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public StrainDTO findOne(Long id) {
        log.debug("Request to get Strain : {}", id);
        Strain strain = strainRepository.findOne(id);
        return strainMapper.toDto(strain);
    }

    /**
     * Delete the strain by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Strain : {}", id);
        strainRepository.delete(id);
        strainSearchRepository.delete(id);
    }

    /**
     * Search for the strain corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StrainDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Strains for query {}", query);
        Page<Strain> result = strainSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(strainMapper::toDto);
    }
}
