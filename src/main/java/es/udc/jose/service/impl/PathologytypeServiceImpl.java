package es.udc.jose.service.impl;

import es.udc.jose.service.PathologytypeService;
import es.udc.jose.domain.Pathologytype;
import es.udc.jose.repository.PathologytypeRepository;
import es.udc.jose.repository.search.PathologytypeSearchRepository;
import es.udc.jose.service.dto.PathologytypeDTO;
import es.udc.jose.service.mapper.PathologytypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZonedDateTime;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Pathologytype.
 */
@Service
@Transactional
public class PathologytypeServiceImpl implements PathologytypeService {

    private final Logger log = LoggerFactory.getLogger(PathologytypeServiceImpl.class);

    private final PathologytypeRepository pathologytypeRepository;

    private final PathologytypeMapper pathologytypeMapper;

    private final PathologytypeSearchRepository pathologytypeSearchRepository;

    public PathologytypeServiceImpl(PathologytypeRepository pathologytypeRepository, PathologytypeMapper pathologytypeMapper, PathologytypeSearchRepository pathologytypeSearchRepository) {
        this.pathologytypeRepository = pathologytypeRepository;
        this.pathologytypeMapper = pathologytypeMapper;
        this.pathologytypeSearchRepository = pathologytypeSearchRepository;
    }

    /**
     * Save a pathologytype.
     *
     * @param pathologytypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PathologytypeDTO save(PathologytypeDTO pathologytypeDTO) {
        pathologytypeDTO.setTimestamp(ZonedDateTime.now());
        log.debug("Request to save Pathologytype : {}", pathologytypeDTO);
        Pathologytype pathologytype = pathologytypeMapper.toEntity(pathologytypeDTO);
        pathologytype = pathologytypeRepository.save(pathologytype);
        PathologytypeDTO result = pathologytypeMapper.toDto(pathologytype);
        pathologytypeSearchRepository.save(pathologytype);
        return result;
    }

    /**
     * Get all the pathologytypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PathologytypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pathologytypes");
        return pathologytypeRepository.findAll(pageable)
            .map(pathologytypeMapper::toDto);
    }

    /**
     * Get one pathologytype by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PathologytypeDTO findOne(Long id) {
        log.debug("Request to get Pathologytype : {}", id);
        Pathologytype pathologytype = pathologytypeRepository.findOne(id);
        return pathologytypeMapper.toDto(pathologytype);
    }

    /**
     * Delete the pathologytype by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pathologytype : {}", id);
        pathologytypeRepository.delete(id);
        pathologytypeSearchRepository.delete(id);
    }

    /**
     * Search for the pathologytype corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PathologytypeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Pathologytypes for query {}", query);
        Page<Pathologytype> result = pathologytypeSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(pathologytypeMapper::toDto);
    }
}
