package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Allergens;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.AllergensRepository;
import es.udc.jose.repository.search.AllergensSearchRepository;
import es.udc.jose.service.dto.AllergensCriteria;

import es.udc.jose.service.dto.AllergensDTO;
import es.udc.jose.service.mapper.AllergensMapper;

/**
 * Service for executing complex queries for Allergens entities in the database.
 * The main input is a {@link AllergensCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AllergensDTO} or a {@link Page} of {@link AllergensDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AllergensQueryService extends QueryService<Allergens> {

    private final Logger log = LoggerFactory.getLogger(AllergensQueryService.class);


    private final AllergensRepository allergensRepository;

    private final AllergensMapper allergensMapper;

    private final AllergensSearchRepository allergensSearchRepository;

    public AllergensQueryService(AllergensRepository allergensRepository, AllergensMapper allergensMapper, AllergensSearchRepository allergensSearchRepository) {
        this.allergensRepository = allergensRepository;
        this.allergensMapper = allergensMapper;
        this.allergensSearchRepository = allergensSearchRepository;
    }

    /**
     * Return a {@link List} of {@link AllergensDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AllergensDTO> findByCriteria(AllergensCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Allergens> specification = createSpecification(criteria);
        return allergensMapper.toDto(allergensRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AllergensDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AllergensDTO> findByCriteria(AllergensCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Allergens> specification = createSpecification(criteria);
        final Page<Allergens> result = allergensRepository.findAll(specification, page);
        return result.map(allergensMapper::toDto);
    }

    /**
     * Function to convert AllergensCriteria to a {@link Specifications}
     */
    private Specifications<Allergens> createSpecification(AllergensCriteria criteria) {
        Specifications<Allergens> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Allergens_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Allergens_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Allergens_.timestamp));
            }
        }
        return specification;
    }

}
