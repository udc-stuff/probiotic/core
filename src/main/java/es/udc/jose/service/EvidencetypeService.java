package es.udc.jose.service;

import es.udc.jose.service.dto.EvidencetypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Evidencetype.
 */
public interface EvidencetypeService {

    /**
     * Save a evidencetype.
     *
     * @param evidencetypeDTO the entity to save
     * @return the persisted entity
     */
    EvidencetypeDTO save(EvidencetypeDTO evidencetypeDTO);

    /**
     * Get all the evidencetypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidencetypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" evidencetype.
     *
     * @param id the id of the entity
     * @return the entity
     */
    EvidencetypeDTO findOne(Long id);

    /**
     * Delete the "id" evidencetype.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the evidencetype corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<EvidencetypeDTO> search(String query, Pageable pageable);
}
