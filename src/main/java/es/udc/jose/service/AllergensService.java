package es.udc.jose.service;

import es.udc.jose.service.dto.AllergensDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Allergens.
 */
public interface AllergensService {

    /**
     * Save a allergens.
     *
     * @param allergensDTO the entity to save
     * @return the persisted entity
     */
    AllergensDTO save(AllergensDTO allergensDTO);

    /**
     * Get all the allergens.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AllergensDTO> findAll(Pageable pageable);

    /**
     * Get the "id" allergens.
     *
     * @param id the id of the entity
     * @return the entity
     */
    AllergensDTO findOne(Long id);

    /**
     * Delete the "id" allergens.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the allergens corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AllergensDTO> search(String query, Pageable pageable);
}
