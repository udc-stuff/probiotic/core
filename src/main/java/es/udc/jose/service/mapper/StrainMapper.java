package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.StrainDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Strain and its DTO StrainDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface StrainMapper extends EntityMapper<StrainDTO, Strain> {

    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    StrainDTO toDto(Strain strain);

    @Mapping(source = "modifiedbyId", target = "modifiedby")
    Strain toEntity(StrainDTO strainDTO);

    default Strain fromId(Long id) {
        if (id == null) {
            return null;
        }
        Strain strain = new Strain();
        strain.setId(id);
        return strain;
    }
}
