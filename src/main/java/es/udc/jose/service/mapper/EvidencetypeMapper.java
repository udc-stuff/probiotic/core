package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.EvidencetypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Evidencetype and its DTO EvidencetypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EvidencetypeMapper extends EntityMapper<EvidencetypeDTO, Evidencetype> {



    default Evidencetype fromId(Long id) {
        if (id == null) {
            return null;
        }
        Evidencetype evidencetype = new Evidencetype();
        evidencetype.setId(id);
        return evidencetype;
    }
}
