package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.DoseformDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Doseform and its DTO DoseformDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DoseformMapper extends EntityMapper<DoseformDTO, Doseform> {



    default Doseform fromId(Long id) {
        if (id == null) {
            return null;
        }
        Doseform doseform = new Doseform();
        doseform.setId(id);
        return doseform;
    }
}
