package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.PathologyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Pathology and its DTO PathologyDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, PathologytypeMapper.class})
public interface PathologyMapper extends EntityMapper<PathologyDTO, Pathology> {

    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    @Mapping(source = "pathologytype.id", target = "pathologytypeId")
    @Mapping(source = "pathologytype.name", target = "pathologytypeName")
    PathologyDTO toDto(Pathology pathology);

    @Mapping(source = "modifiedbyId", target = "modifiedby")
    @Mapping(source = "pathologytypeId", target = "pathologytype")
    Pathology toEntity(PathologyDTO pathologyDTO);

    default Pathology fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pathology pathology = new Pathology();
        pathology.setId(id);
        return pathology;
    }
}
