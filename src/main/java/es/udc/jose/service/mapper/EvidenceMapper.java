package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.EvidenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Evidence and its DTO EvidenceDTO.
 */
@Mapper(componentModel = "spring", uses = {PathologyMapper.class, DosageMapper.class, EvidencereferencesMapper.class, EvidencetypeMapper.class, UserMapper.class})
public interface EvidenceMapper extends EntityMapper<EvidenceDTO, Evidence> {

    @Mapping(source = "pathology.id", target = "pathologyId")
    @Mapping(source = "pathology.acronym", target = "pathologyAcronym")
    @Mapping(source = "dosage.id", target = "dosageId")
    @Mapping(source = "evidencetype.id", target = "evidencetypeId")
    @Mapping(source = "evidencetype.name", target = "evidencetypeName")
    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    @Mapping(source = "dosage.doseform.name", target = "doseFormName")
    @Mapping(source = "dosage.probiotic.name", target = "probioticName")
    @Mapping(source = "dosage.probiotic.id", target = "probioticId")
    @Mapping(source = "dosage.age", target = "dosageAge")
    EvidenceDTO toDto(Evidence evidence);

    @Mapping(source = "pathologyId", target = "pathology")
    @Mapping(source = "dosageId", target = "dosage")
    @Mapping(source = "evidencetypeId", target = "evidencetype")
    @Mapping(source = "modifiedbyId", target = "modifiedby")
    Evidence toEntity(EvidenceDTO evidenceDTO);

    default Evidence fromId(Long id) {
        if (id == null) {
            return null;
        }
        Evidence evidence = new Evidence();
        evidence.setId(id);
        return evidence;
    }
}
