package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.EvidencereferencesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Evidencereferences and its DTO EvidencereferencesDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface EvidencereferencesMapper extends EntityMapper<EvidencereferencesDTO, Evidencereferences> {

    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    EvidencereferencesDTO toDto(Evidencereferences evidencereferences);

    @Mapping(source = "modifiedbyId", target = "modifiedby")
    Evidencereferences toEntity(EvidencereferencesDTO evidencereferencesDTO);

    default Evidencereferences fromId(Long id) {
        if (id == null) {
            return null;
        }
        Evidencereferences evidencereferences = new Evidencereferences();
        evidencereferences.setId(id);
        return evidencereferences;
    }
}
