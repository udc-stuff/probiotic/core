package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.PathologytypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Pathologytype and its DTO PathologytypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PathologytypeMapper extends EntityMapper<PathologytypeDTO, Pathologytype> {



    default Pathologytype fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pathologytype pathologytype = new Pathologytype();
        pathologytype.setId(id);
        return pathologytype;
    }
}
