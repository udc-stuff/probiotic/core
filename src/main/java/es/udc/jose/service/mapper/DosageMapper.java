package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.DosageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Dosage and its DTO DosageDTO.
 */
@Mapper(componentModel = "spring", uses = {ProbioticMapper.class, DoseformMapper.class, UserMapper.class})
public interface DosageMapper extends EntityMapper<DosageDTO, Dosage> {

    @Mapping(source = "probiotic.id", target = "probioticId")
    @Mapping(source = "doseform.id", target = "doseformId")
    @Mapping(source = "probiotic.name", target = "probioticName")
    @Mapping(source = "doseform.name", target = "doseformName")
    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    DosageDTO toDto(Dosage dosage);

    @Mapping(source = "probioticId", target = "probiotic")
    @Mapping(source = "doseformId", target = "doseform")
    @Mapping(source = "modifiedbyId", target = "modifiedby")
    Dosage toEntity(DosageDTO dosageDTO);

    default Dosage fromId(Long id) {
        if (id == null) {
            return null;
        }
        Dosage dosage = new Dosage();
        dosage.setId(id);
        return dosage;
    }
}
