package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.AllergensDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Allergens and its DTO AllergensDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AllergensMapper extends EntityMapper<AllergensDTO, Allergens> {



    default Allergens fromId(Long id) {
        if (id == null) {
            return null;
        }
        Allergens allergens = new Allergens();
        allergens.setId(id);
        return allergens;
    }
}
