package es.udc.jose.service.mapper;

import es.udc.jose.domain.*;
import es.udc.jose.service.dto.ProbioticDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Probiotic and its DTO ProbioticDTO.
 */
@Mapper(componentModel = "spring", uses = {AllergensMapper.class, StrainMapper.class, UserMapper.class})
public interface ProbioticMapper extends EntityMapper<ProbioticDTO, Probiotic> {

    @Mapping(source = "modifiedby.id", target = "modifiedbyId")
    @Mapping(source = "modifiedby.login", target = "modifiedbyLogin")
    ProbioticDTO toDto(Probiotic probiotic);

    @Mapping(source = "modifiedbyId", target = "modifiedby")
    Probiotic toEntity(ProbioticDTO probioticDTO);

    default Probiotic fromId(Long id) {
        if (id == null) {
            return null;
        }
        Probiotic probiotic = new Probiotic();
        probiotic.setId(id);
        return probiotic;
    }
}
