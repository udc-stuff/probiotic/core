package es.udc.jose.service;

import es.udc.jose.service.dto.PathologytypeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Pathologytype.
 */
public interface PathologytypeService {

    /**
     * Save a pathologytype.
     *
     * @param pathologytypeDTO the entity to save
     * @return the persisted entity
     */
    PathologytypeDTO save(PathologytypeDTO pathologytypeDTO);

    /**
     * Get all the pathologytypes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PathologytypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" pathologytype.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PathologytypeDTO findOne(Long id);

    /**
     * Delete the "id" pathologytype.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the pathologytype corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PathologytypeDTO> search(String query, Pageable pageable);
}
