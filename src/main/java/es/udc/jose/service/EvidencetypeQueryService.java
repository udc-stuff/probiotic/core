package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Evidencetype;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.EvidencetypeRepository;
import es.udc.jose.repository.search.EvidencetypeSearchRepository;
import es.udc.jose.service.dto.EvidencetypeCriteria;

import es.udc.jose.service.dto.EvidencetypeDTO;
import es.udc.jose.service.mapper.EvidencetypeMapper;

/**
 * Service for executing complex queries for Evidencetype entities in the database.
 * The main input is a {@link EvidencetypeCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EvidencetypeDTO} or a {@link Page} of {@link EvidencetypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EvidencetypeQueryService extends QueryService<Evidencetype> {

    private final Logger log = LoggerFactory.getLogger(EvidencetypeQueryService.class);


    private final EvidencetypeRepository evidencetypeRepository;

    private final EvidencetypeMapper evidencetypeMapper;

    private final EvidencetypeSearchRepository evidencetypeSearchRepository;

    public EvidencetypeQueryService(EvidencetypeRepository evidencetypeRepository, EvidencetypeMapper evidencetypeMapper, EvidencetypeSearchRepository evidencetypeSearchRepository) {
        this.evidencetypeRepository = evidencetypeRepository;
        this.evidencetypeMapper = evidencetypeMapper;
        this.evidencetypeSearchRepository = evidencetypeSearchRepository;
    }

    /**
     * Return a {@link List} of {@link EvidencetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EvidencetypeDTO> findByCriteria(EvidencetypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Evidencetype> specification = createSpecification(criteria);
        return evidencetypeMapper.toDto(evidencetypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EvidencetypeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EvidencetypeDTO> findByCriteria(EvidencetypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Evidencetype> specification = createSpecification(criteria);
        final Page<Evidencetype> result = evidencetypeRepository.findAll(specification, page);
        return result.map(evidencetypeMapper::toDto);
    }

    /**
     * Function to convert EvidencetypeCriteria to a {@link Specifications}
     */
    private Specifications<Evidencetype> createSpecification(EvidencetypeCriteria criteria) {
        Specifications<Evidencetype> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Evidencetype_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Evidencetype_.name));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Evidencetype_.timestamp));
            }
        }
        return specification;
    }

}
