package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Evidencetype entity.
 */
public class EvidencetypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime timestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EvidencetypeDTO evidencetypeDTO = (EvidencetypeDTO) o;
        if(evidencetypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evidencetypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvidencetypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
