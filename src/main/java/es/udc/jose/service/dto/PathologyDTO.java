package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Pathology entity.
 */
public class PathologyDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime timestamp;

    @NotNull
    private String acronym;

    private Long modifiedbyId;

    private String modifiedbyLogin;

    private Long pathologytypeId;

    private String pathologytypeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    public Long getPathologytypeId() {
        return pathologytypeId;
    }

    public void setPathologytypeId(Long pathologytypeId) {
        this.pathologytypeId = pathologytypeId;
    }

    public String getPathologytypeName() {
        return pathologytypeName;
    }

    public void setPathologytypeName(String pathologytypeName) {
        this.pathologytypeName = pathologytypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PathologyDTO pathologyDTO = (PathologyDTO) o;
        if(pathologyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pathologyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PathologyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", acronym='" + getAcronym() + "'" +
            "}";
    }
}
