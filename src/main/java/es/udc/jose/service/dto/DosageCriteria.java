package es.udc.jose.service.dto;

import java.io.Serializable;
import es.udc.jose.domain.enumeration.Age;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Dosage entity. This class is used in DosageResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /dosages?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DosageCriteria implements Serializable {
    /**
     * Class for filtering Age
     */
    public static class AgeFilter extends Filter<Age> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ZonedDateTimeFilter timestamp;

    private AgeFilter age;

    private StringFilter intake;

    private StringFilter frequency;

    private StringFilter indication;

    private LongFilter cfu;

    private LongFilter probioticId;

    private LongFilter doseformId;

    private LongFilter modifiedbyId;

    public DosageCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTimeFilter timestamp) {
        this.timestamp = timestamp;
    }

    public AgeFilter getAge() {
        return age;
    }

    public void setAge(AgeFilter age) {
        this.age = age;
    }

    public StringFilter getIntake() {
        return intake;
    }

    public void setIntake(StringFilter intake) {
        this.intake = intake;
    }

    public StringFilter getFrequency() {
        return frequency;
    }

    public void setFrequency(StringFilter frequency) {
        this.frequency = frequency;
    }

    public StringFilter getIndication() {
        return indication;
    }

    public void setIndication(StringFilter indication) {
        this.indication = indication;
    }

    public LongFilter getCfu() {
        return cfu;
    }

    public void setCfu(LongFilter cfu) {
        this.cfu = cfu;
    }

    public LongFilter getProbioticId() {
        return probioticId;
    }

    public void setProbioticId(LongFilter probioticId) {
        this.probioticId = probioticId;
    }

    public LongFilter getDoseformId() {
        return doseformId;
    }

    public void setDoseformId(LongFilter doseformId) {
        this.doseformId = doseformId;
    }

    public LongFilter getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(LongFilter modifiedbyId) {
        this.modifiedbyId = modifiedbyId;
    }

    @Override
    public String toString() {
        return "DosageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (age != null ? "age=" + age + ", " : "") +
                (intake != null ? "intake=" + intake + ", " : "") +
                (frequency != null ? "frequency=" + frequency + ", " : "") +
                (indication != null ? "indication=" + indication + ", " : "") +
                (cfu != null ? "cfu=" + cfu + ", " : "") +
                (probioticId != null ? "probioticId=" + probioticId + ", " : "") +
                (doseformId != null ? "doseformId=" + doseformId + ", " : "") +
                (modifiedbyId != null ? "modifiedbyId=" + modifiedbyId + ", " : "") +
            "}";
    }

}
