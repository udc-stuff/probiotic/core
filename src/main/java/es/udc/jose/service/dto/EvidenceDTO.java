package es.udc.jose.service.dto;


import es.udc.jose.domain.enumeration.Age;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Evidence entity.
 */
public class EvidenceDTO implements Serializable {

    private Long id;

    private ZonedDateTime timestamp;

    private Long pathologyId;

    private String pathologyAcronym;

    private Long dosageId;

    private Set<EvidencereferencesDTO> evidencereferences = new HashSet<>();

    private Long evidencetypeId;

    private String evidencetypeName;

    private Long modifiedbyId;

    private String modifiedbyLogin;

    private String doseFormName;

    private String probioticName;

    private Long probioticId;

    private Age dosageAge;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getPathologyId() {
        return pathologyId;
    }

    public void setPathologyId(Long pathologyId) {
        this.pathologyId = pathologyId;
    }

    public Long getDosageId() {
        return dosageId;
    }

    public void setDosageId(Long dosageId) {
        this.dosageId = dosageId;
    }

    public Set<EvidencereferencesDTO> getEvidencereferences() {
        return evidencereferences;
    }

    public void setEvidencereferences(Set<EvidencereferencesDTO> evidencereferences) {
        this.evidencereferences = evidencereferences;
    }

    public Long getEvidencetypeId() {
        return evidencetypeId;
    }

    public void setEvidencetypeId(Long evidencetypeId) {
        this.evidencetypeId = evidencetypeId;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    public String getPathologyAcronym() {
        return pathologyAcronym;
    }

    public void setPathologyAcronym(String pathologyAcronym) {
        this.pathologyAcronym = pathologyAcronym;
    }

    public String getEvidencetypeName() {
        return evidencetypeName;
    }

    public void setEvidencetypeName(String evidencetypeName) {
        this.evidencetypeName = evidencetypeName;
    }

    public String getDoseFormName() {
        return doseFormName;
    }

    public void setDoseFormName(String doseFormName) {
        this.doseFormName = doseFormName;
    }

    public String getProbioticName() {
        return probioticName;
    }

    public void setProbioticName(String probioticName) {
        this.probioticName = probioticName;
    }

    public Long getProbioticId() {
        return probioticId;
    }

    public void setProbioticId(Long probioticId) {
        this.probioticId = probioticId;
    }

    public Age getDosageAge() {
        return dosageAge;
    }

    public void setDosageAge(Age dosageAge) {
        this.dosageAge = dosageAge;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EvidenceDTO evidenceDTO = (EvidenceDTO) o;
        if(evidenceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evidenceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvidenceDTO{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
