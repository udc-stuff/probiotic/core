package es.udc.jose.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Pathology entity. This class is used in PathologyResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /pathologies?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PathologyCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private ZonedDateTimeFilter timestamp;

    private StringFilter acronym;

    private LongFilter modifiedbyId;

    private LongFilter pathologytypeId;

    public PathologyCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public ZonedDateTimeFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTimeFilter timestamp) {
        this.timestamp = timestamp;
    }

    public StringFilter getAcronym() {
        return acronym;
    }

    public void setAcronym(StringFilter acronym) {
        this.acronym = acronym;
    }

    public LongFilter getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(LongFilter modifiedbyId) {
        this.modifiedbyId = modifiedbyId;
    }

    public LongFilter getPathologytypeId() {
        return pathologytypeId;
    }

    public void setPathologytypeId(LongFilter pathologytypeId) {
        this.pathologytypeId = pathologytypeId;
    }

    @Override
    public String toString() {
        return "PathologyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (acronym != null ? "acronym=" + acronym + ", " : "") +
                (modifiedbyId != null ? "modifiedbyId=" + modifiedbyId + ", " : "") +
                (pathologytypeId != null ? "pathologytypeId=" + pathologytypeId + ", " : "") +
            "}";
    }

}
