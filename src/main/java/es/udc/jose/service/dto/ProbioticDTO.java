package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Probiotic entity.
 */
public class ProbioticDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime timestamp;

    @NotNull
    private Boolean have_allergens;

    private Set<AllergensDTO> allergens = new HashSet<>();

    private Set<StrainDTO> strains = new HashSet<>();

    private Long modifiedbyId;

    private String modifiedbyLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean isHave_allergens() {
        return have_allergens;
    }

    public void setHave_allergens(Boolean have_allergens) {
        this.have_allergens = have_allergens;
    }

    public Set<AllergensDTO> getAllergens() {
        return allergens;
    }

    public void setAllergens(Set<AllergensDTO> allergens) {
        this.allergens = allergens;
    }

    public Set<StrainDTO> getStrains() {
        return strains;
    }

    public void setStrains(Set<StrainDTO> strains) {
        this.strains = strains;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProbioticDTO probioticDTO = (ProbioticDTO) o;
        if(probioticDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), probioticDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProbioticDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", have_allergens='" + isHave_allergens() + "'" +
            "}";
    }
}
