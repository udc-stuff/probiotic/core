package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Evidencereferences entity.
 */
public class EvidencereferencesDTO implements Serializable {

    private Long id;

    @NotNull
    private String text;

    private ZonedDateTime timestamp;

    private Long modifiedbyId;

    private String modifiedbyLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EvidencereferencesDTO evidencereferencesDTO = (EvidencereferencesDTO) o;
        if(evidencereferencesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evidencereferencesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EvidencereferencesDTO{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
