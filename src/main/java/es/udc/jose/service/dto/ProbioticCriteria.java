package es.udc.jose.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Probiotic entity. This class is used in ProbioticResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /probiotics?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProbioticCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private ZonedDateTimeFilter timestamp;

    private BooleanFilter have_allergens;

    private LongFilter allergensId;

    private LongFilter strainId;

    private LongFilter modifiedbyId;

    public ProbioticCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public ZonedDateTimeFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTimeFilter timestamp) {
        this.timestamp = timestamp;
    }

    public BooleanFilter getHave_allergens() {
        return have_allergens;
    }

    public void setHave_allergens(BooleanFilter have_allergens) {
        this.have_allergens = have_allergens;
    }

    public LongFilter getAllergensId() {
        return allergensId;
    }

    public void setAllergensId(LongFilter allergensId) {
        this.allergensId = allergensId;
    }

    public LongFilter getStrainId() {
        return strainId;
    }

    public void setStrainId(LongFilter strainId) {
        this.strainId = strainId;
    }

    public LongFilter getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(LongFilter modifiedbyId) {
        this.modifiedbyId = modifiedbyId;
    }

    @Override
    public String toString() {
        return "ProbioticCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (have_allergens != null ? "have_allergens=" + have_allergens + ", " : "") +
                (allergensId != null ? "allergensId=" + allergensId + ", " : "") +
                (strainId != null ? "strainId=" + strainId + ", " : "") +
                (modifiedbyId != null ? "modifiedbyId=" + modifiedbyId + ", " : "") +
            "}";
    }

}
