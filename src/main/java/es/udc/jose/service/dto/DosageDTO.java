package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import es.udc.jose.domain.enumeration.Age;

/**
 * A DTO for the Dosage entity.
 */
public class DosageDTO implements Serializable {

    private Long id;

    private ZonedDateTime timestamp;

    @NotNull
    private Age age;

    @NotNull
    private Long cfu;

    @NotNull
    private String intake;

    @NotNull
    private String frequency;

    private String indication;

    private Long probioticId;

    private String probioticName;

    private Long doseformId;

    private String doseformName;

    private Long modifiedbyId;

    private String modifiedbyLogin;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public Long getCfu() {
        return cfu;
    }

    public void setCfu(Long cfu) {
        this.cfu = cfu;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public Long getProbioticId() {
        return probioticId;
    }

    public void setProbioticId(Long probioticId) {
        this.probioticId = probioticId;
    }

    public Long getDoseformId() {
        return doseformId;
    }

    public void setDoseformId(Long doseformId) {
        this.doseformId = doseformId;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    public String getProbioticName() {
        return probioticName;
    }

    public void setProbioticName(String probioticName) {
        this.probioticName = probioticName;
    }

    public String getDoseformName() {
        return doseformName;
    }

    public void setDoseformName(String doseformName) {
        this.doseformName = doseformName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DosageDTO dosageDTO = (DosageDTO) o;
        if(dosageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dosageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DosageDTO{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", age='" + getAge() + "'" +
            ", cfu='" + getCfu() + "'" +
            ", intake='" + getIntake() + "'" +
            ", frequency='" + getFrequency() + "'" +
            ", indication='" + getIndication() + "'" +
            "}";
    }
}
