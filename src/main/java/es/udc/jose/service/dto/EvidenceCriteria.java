package es.udc.jose.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;



import io.github.jhipster.service.filter.ZonedDateTimeFilter;


/**
 * Criteria class for the Evidence entity. This class is used in EvidenceResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /evidences?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EvidenceCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ZonedDateTimeFilter timestamp;

    private LongFilter pathologyId;

    private LongFilter dosageId;

    private LongFilter evidencereferencesId;

    private LongFilter evidencetypeId;

    private LongFilter modifiedbyId;

    public EvidenceCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTimeFilter timestamp) {
        this.timestamp = timestamp;
    }

    public LongFilter getPathologyId() {
        return pathologyId;
    }

    public void setPathologyId(LongFilter pathologyId) {
        this.pathologyId = pathologyId;
    }

    public LongFilter getDosageId() {
        return dosageId;
    }

    public void setDosageId(LongFilter dosageId) {
        this.dosageId = dosageId;
    }

    public LongFilter getEvidencereferencesId() {
        return evidencereferencesId;
    }

    public void setEvidencereferencesId(LongFilter evidencereferencesId) {
        this.evidencereferencesId = evidencereferencesId;
    }

    public LongFilter getEvidencetypeId() {
        return evidencetypeId;
    }

    public void setEvidencetypeId(LongFilter evidencetypeId) {
        this.evidencetypeId = evidencetypeId;
    }

    public LongFilter getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(LongFilter modifiedbyId) {
        this.modifiedbyId = modifiedbyId;
    }

    @Override
    public String toString() {
        return "EvidenceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (timestamp != null ? "timestamp=" + timestamp + ", " : "") +
                (pathologyId != null ? "pathologyId=" + pathologyId + ", " : "") +
                (dosageId != null ? "dosageId=" + dosageId + ", " : "") +
                (evidencereferencesId != null ? "evidencereferencesId=" + evidencereferencesId + ", " : "") +
                (evidencetypeId != null ? "evidencetypeId=" + evidencetypeId + ", " : "") +
                (modifiedbyId != null ? "modifiedbyId=" + modifiedbyId + ", " : "") +
            "}";
    }

}
