package es.udc.jose.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Strain entity.
 */
public class StrainDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private ZonedDateTime timestamp;

    private Long modifiedbyId;

    private String modifiedbyLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getModifiedbyId() {
        return modifiedbyId;
    }

    public void setModifiedbyId(Long userId) {
        this.modifiedbyId = userId;
    }

    public String getModifiedbyLogin() {
        return modifiedbyLogin;
    }

    public void setModifiedbyLogin(String userLogin) {
        this.modifiedbyLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StrainDTO strainDTO = (StrainDTO) o;
        if(strainDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), strainDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StrainDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
