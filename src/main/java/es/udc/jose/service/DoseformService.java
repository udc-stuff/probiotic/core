package es.udc.jose.service;

import es.udc.jose.service.dto.DoseformDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Doseform.
 */
public interface DoseformService {

    /**
     * Save a doseform.
     *
     * @param doseformDTO the entity to save
     * @return the persisted entity
     */
    DoseformDTO save(DoseformDTO doseformDTO);

    /**
     * Get all the doseforms.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoseformDTO> findAll(Pageable pageable);

    /**
     * Get the "id" doseform.
     *
     * @param id the id of the entity
     * @return the entity
     */
    DoseformDTO findOne(Long id);

    /**
     * Delete the "id" doseform.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the doseform corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DoseformDTO> search(String query, Pageable pageable);
}
