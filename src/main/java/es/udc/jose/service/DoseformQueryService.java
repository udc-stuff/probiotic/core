package es.udc.jose.service;


import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.udc.jose.domain.Doseform;
import es.udc.jose.domain.*; // for static metamodels
import es.udc.jose.repository.DoseformRepository;
import es.udc.jose.repository.search.DoseformSearchRepository;
import es.udc.jose.service.dto.DoseformCriteria;

import es.udc.jose.service.dto.DoseformDTO;
import es.udc.jose.service.mapper.DoseformMapper;

/**
 * Service for executing complex queries for Doseform entities in the database.
 * The main input is a {@link DoseformCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DoseformDTO} or a {@link Page} of {@link DoseformDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DoseformQueryService extends QueryService<Doseform> {

    private final Logger log = LoggerFactory.getLogger(DoseformQueryService.class);


    private final DoseformRepository doseformRepository;

    private final DoseformMapper doseformMapper;

    private final DoseformSearchRepository doseformSearchRepository;

    public DoseformQueryService(DoseformRepository doseformRepository, DoseformMapper doseformMapper, DoseformSearchRepository doseformSearchRepository) {
        this.doseformRepository = doseformRepository;
        this.doseformMapper = doseformMapper;
        this.doseformSearchRepository = doseformSearchRepository;
    }

    /**
     * Return a {@link List} of {@link DoseformDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DoseformDTO> findByCriteria(DoseformCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Doseform> specification = createSpecification(criteria);
        return doseformMapper.toDto(doseformRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DoseformDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DoseformDTO> findByCriteria(DoseformCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Doseform> specification = createSpecification(criteria);
        final Page<Doseform> result = doseformRepository.findAll(specification, page);
        return result.map(doseformMapper::toDto);
    }

    /**
     * Function to convert DoseformCriteria to a {@link Specifications}
     */
    private Specifications<Doseform> createSpecification(DoseformCriteria criteria) {
        Specifications<Doseform> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Doseform_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Doseform_.name));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getType(), Doseform_.type));
            }
            if (criteria.getTimestamp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTimestamp(), Doseform_.timestamp));
            }
        }
        return specification;
    }

}
