package es.udc.jose.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(es.udc.jose.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(es.udc.jose.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Pathology.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Strain.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Allergens.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Probiotic.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Probiotic.class.getName() + ".allergens", jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Probiotic.class.getName() + ".strains", jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Dosage.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Evidence.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Doseform.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Evidencereferences.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Evidence.class.getName() + ".evidencereferences", jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Pathologytype.class.getName(), jcacheConfiguration);
            cm.createCache(es.udc.jose.domain.Evidencetype.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
