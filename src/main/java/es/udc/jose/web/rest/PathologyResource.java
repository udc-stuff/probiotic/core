package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.PathologyService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.PathologyDTO;
import es.udc.jose.service.dto.PathologyCriteria;
import es.udc.jose.service.PathologyQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pathology.
 */
@RestController
@RequestMapping("/api")
public class PathologyResource {

    private final Logger log = LoggerFactory.getLogger(PathologyResource.class);

    private static final String ENTITY_NAME = "pathology";

    private final PathologyService pathologyService;

    private final PathologyQueryService pathologyQueryService;

    public PathologyResource(PathologyService pathologyService, PathologyQueryService pathologyQueryService) {
        this.pathologyService = pathologyService;
        this.pathologyQueryService = pathologyQueryService;
    }

    /**
     * POST  /pathologies : Create a new pathology.
     *
     * @param pathologyDTO the pathologyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pathologyDTO, or with status 400 (Bad Request) if the pathology has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pathologies")
    @Timed
    public ResponseEntity<PathologyDTO> createPathology(@Valid @RequestBody PathologyDTO pathologyDTO) throws URISyntaxException {
        log.debug("REST request to save Pathology : {}", pathologyDTO);
        if (pathologyDTO.getId() != null) {
            throw new BadRequestAlertException("A new pathology cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PathologyDTO result = pathologyService.save(pathologyDTO);
        return ResponseEntity.created(new URI("/api/pathologies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /pathologies : Updates an existing pathology.
     *
     * @param pathologyDTO the pathologyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pathologyDTO,
     * or with status 400 (Bad Request) if the pathologyDTO is not valid,
     * or with status 500 (Internal Server Error) if the pathologyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pathologies")
    @Timed
    public ResponseEntity<PathologyDTO> updatePathology(@Valid @RequestBody PathologyDTO pathologyDTO) throws URISyntaxException {
        log.debug("REST request to update Pathology : {}", pathologyDTO);
        if (pathologyDTO.getId() == null) {
            return createPathology(pathologyDTO);
        }
        PathologyDTO result = pathologyService.save(pathologyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pathologyDTO.getName()))
            .body(result);
    }

    /**
     * GET  /pathologies : get all the pathologies.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of pathologies in body
     */
    @GetMapping("/pathologies")
    @Timed
    public ResponseEntity<List<PathologyDTO>> getAllPathologies(PathologyCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Pathologies by criteria: {}", criteria);
        Page<PathologyDTO> page = pathologyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pathologies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pathologies/:id : get the "id" pathology.
     *
     * @param id the id of the pathologyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pathologyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pathologies/{id}")
    @Timed
    public ResponseEntity<PathologyDTO> getPathology(@PathVariable Long id) {
        log.debug("REST request to get Pathology : {}", id);
        PathologyDTO pathologyDTO = pathologyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pathologyDTO));
    }

    /**
     * DELETE  /pathologies/:id : delete the "id" pathology.
     *
     * @param id the id of the pathologyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pathologies/{id}")
    @Timed
    public ResponseEntity<Void> deletePathology(@PathVariable Long id) {
        log.debug("REST request to delete Pathology : {}", id);
        PathologyDTO pathologyDTO = pathologyService.findOne(id);
        pathologyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, pathologyDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/pathologies?query=:query : search for the pathology corresponding
     * to the query.
     *
     * @param query the query of the pathology search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/pathologies")
    @Timed
    public ResponseEntity<List<PathologyDTO>> searchPathologies(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Pathologies for query {}", query);
        Page<PathologyDTO> page = pathologyService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/pathologies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
