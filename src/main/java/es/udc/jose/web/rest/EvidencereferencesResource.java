package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.EvidencereferencesService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.EvidencereferencesDTO;
import es.udc.jose.service.dto.EvidencereferencesCriteria;
import es.udc.jose.service.EvidencereferencesQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Evidencereferences.
 */
@RestController
@RequestMapping("/api")
public class EvidencereferencesResource {

    private final Logger log = LoggerFactory.getLogger(EvidencereferencesResource.class);

    private static final String ENTITY_NAME = "evidencereferences";

    private final EvidencereferencesService evidencereferencesService;

    private final EvidencereferencesQueryService evidencereferencesQueryService;

    public EvidencereferencesResource(EvidencereferencesService evidencereferencesService, EvidencereferencesQueryService evidencereferencesQueryService) {
        this.evidencereferencesService = evidencereferencesService;
        this.evidencereferencesQueryService = evidencereferencesQueryService;
    }

    /**
     * POST  /evidencereferences : Create a new evidencereferences.
     *
     * @param evidencereferencesDTO the evidencereferencesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evidencereferencesDTO, or with status 400 (Bad Request) if the evidencereferences has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evidencereferences")
    @Timed
    public ResponseEntity<EvidencereferencesDTO> createEvidencereferences(@Valid @RequestBody EvidencereferencesDTO evidencereferencesDTO) throws URISyntaxException {
        log.debug("REST request to save Evidencereferences : {}", evidencereferencesDTO);
        if (evidencereferencesDTO.getId() != null) {
            throw new BadRequestAlertException("A new evidencereferences cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EvidencereferencesDTO result = evidencereferencesService.save(evidencereferencesDTO);
        return ResponseEntity.created(new URI("/api/evidencereferences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evidencereferences : Updates an existing evidencereferences.
     *
     * @param evidencereferencesDTO the evidencereferencesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evidencereferencesDTO,
     * or with status 400 (Bad Request) if the evidencereferencesDTO is not valid,
     * or with status 500 (Internal Server Error) if the evidencereferencesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evidencereferences")
    @Timed
    public ResponseEntity<EvidencereferencesDTO> updateEvidencereferences(@Valid @RequestBody EvidencereferencesDTO evidencereferencesDTO) throws URISyntaxException {
        log.debug("REST request to update Evidencereferences : {}", evidencereferencesDTO);
        if (evidencereferencesDTO.getId() == null) {
            return createEvidencereferences(evidencereferencesDTO);
        }
        EvidencereferencesDTO result = evidencereferencesService.save(evidencereferencesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evidencereferencesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evidencereferences : get all the evidencereferences.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of evidencereferences in body
     */
    @GetMapping("/evidencereferences")
    @Timed
    public ResponseEntity<List<EvidencereferencesDTO>> getAllEvidencereferences(EvidencereferencesCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Evidencereferences by criteria: {}", criteria);
        Page<EvidencereferencesDTO> page = evidencereferencesQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evidencereferences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evidencereferences/:id : get the "id" evidencereferences.
     *
     * @param id the id of the evidencereferencesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evidencereferencesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/evidencereferences/{id}")
    @Timed
    public ResponseEntity<EvidencereferencesDTO> getEvidencereferences(@PathVariable Long id) {
        log.debug("REST request to get Evidencereferences : {}", id);
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evidencereferencesDTO));
    }

    /**
     * DELETE  /evidencereferences/:id : delete the "id" evidencereferences.
     *
     * @param id the id of the evidencereferencesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evidencereferences/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvidencereferences(@PathVariable Long id) {
        log.debug("REST request to delete Evidencereferences : {}", id);
        evidencereferencesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/evidencereferences?query=:query : search for the evidencereferences corresponding
     * to the query.
     *
     * @param query the query of the evidencereferences search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/evidencereferences")
    @Timed
    public ResponseEntity<List<EvidencereferencesDTO>> searchEvidencereferences(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Evidencereferences for query {}", query);
        Page<EvidencereferencesDTO> page = evidencereferencesService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/evidencereferences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
