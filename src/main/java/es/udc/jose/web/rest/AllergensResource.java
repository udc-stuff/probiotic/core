package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.AllergensService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.AllergensDTO;
import es.udc.jose.service.dto.AllergensCriteria;
import es.udc.jose.service.AllergensQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Allergens.
 */
@RestController
@RequestMapping("/api")
public class AllergensResource {

    private final Logger log = LoggerFactory.getLogger(AllergensResource.class);

    private static final String ENTITY_NAME = "allergens";

    private final AllergensService allergensService;

    private final AllergensQueryService allergensQueryService;

    public AllergensResource(AllergensService allergensService, AllergensQueryService allergensQueryService) {
        this.allergensService = allergensService;
        this.allergensQueryService = allergensQueryService;
    }

    /**
     * POST  /allergens : Create a new allergens.
     *
     * @param allergensDTO the allergensDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new allergensDTO, or with status 400 (Bad Request) if the allergens has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/allergens")
    @Timed
    public ResponseEntity<AllergensDTO> createAllergens(@Valid @RequestBody AllergensDTO allergensDTO) throws URISyntaxException {
        log.debug("REST request to save Allergens : {}", allergensDTO);
        if (allergensDTO.getId() != null) {
            throw new BadRequestAlertException("A new allergens cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AllergensDTO result = allergensService.save(allergensDTO);
        return ResponseEntity.created(new URI("/api/allergens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /allergens : Updates an existing allergens.
     *
     * @param allergensDTO the allergensDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated allergensDTO,
     * or with status 400 (Bad Request) if the allergensDTO is not valid,
     * or with status 500 (Internal Server Error) if the allergensDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/allergens")
    @Timed
    public ResponseEntity<AllergensDTO> updateAllergens(@Valid @RequestBody AllergensDTO allergensDTO) throws URISyntaxException {
        log.debug("REST request to update Allergens : {}", allergensDTO);
        if (allergensDTO.getId() == null) {
            return createAllergens(allergensDTO);
        }
        AllergensDTO result = allergensService.save(allergensDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, allergensDTO.getName()))
            .body(result);
    }

    /**
     * GET  /allergens : get all the allergens.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of allergens in body
     */
    @GetMapping("/allergens")
    @Timed
    public ResponseEntity<List<AllergensDTO>> getAllAllergens(AllergensCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Allergens by criteria: {}", criteria);
        Page<AllergensDTO> page = allergensQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/allergens");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /allergens/:id : get the "id" allergens.
     *
     * @param id the id of the allergensDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the allergensDTO, or with status 404 (Not Found)
     */
    @GetMapping("/allergens/{id}")
    @Timed
    public ResponseEntity<AllergensDTO> getAllergens(@PathVariable Long id) {
        log.debug("REST request to get Allergens : {}", id);
        AllergensDTO allergensDTO = allergensService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(allergensDTO));
    }

    /**
     * DELETE  /allergens/:id : delete the "id" allergens.
     *
     * @param id the id of the allergensDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/allergens/{id}")
    @Timed
    public ResponseEntity<Void> deleteAllergens(@PathVariable Long id) {
        log.debug("REST request to delete Allergens : {}", id);
        AllergensDTO allergensDTO = allergensService.findOne(id);
        allergensService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, allergensDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/allergens?query=:query : search for the allergens corresponding
     * to the query.
     *
     * @param query the query of the allergens search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/allergens")
    @Timed
    public ResponseEntity<List<AllergensDTO>> searchAllergens(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Allergens for query {}", query);
        Page<AllergensDTO> page = allergensService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/allergens");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
