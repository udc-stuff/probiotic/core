package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.EvidencetypeService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.EvidencetypeDTO;
import es.udc.jose.service.dto.EvidencetypeCriteria;
import es.udc.jose.service.EvidencetypeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Evidencetype.
 */
@RestController
@RequestMapping("/api")
public class EvidencetypeResource {

    private final Logger log = LoggerFactory.getLogger(EvidencetypeResource.class);

    private static final String ENTITY_NAME = "evidencetype";

    private final EvidencetypeService evidencetypeService;

    private final EvidencetypeQueryService evidencetypeQueryService;

    public EvidencetypeResource(EvidencetypeService evidencetypeService, EvidencetypeQueryService evidencetypeQueryService) {
        this.evidencetypeService = evidencetypeService;
        this.evidencetypeQueryService = evidencetypeQueryService;
    }

    /**
     * POST  /evidencetypes : Create a new evidencetype.
     *
     * @param evidencetypeDTO the evidencetypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evidencetypeDTO, or with status 400 (Bad Request) if the evidencetype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evidencetypes")
    @Timed
    public ResponseEntity<EvidencetypeDTO> createEvidencetype(@Valid @RequestBody EvidencetypeDTO evidencetypeDTO) throws URISyntaxException {
        log.debug("REST request to save Evidencetype : {}", evidencetypeDTO);
        if (evidencetypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new evidencetype cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EvidencetypeDTO result = evidencetypeService.save(evidencetypeDTO);
        return ResponseEntity.created(new URI("/api/evidencetypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /evidencetypes : Updates an existing evidencetype.
     *
     * @param evidencetypeDTO the evidencetypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evidencetypeDTO,
     * or with status 400 (Bad Request) if the evidencetypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the evidencetypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evidencetypes")
    @Timed
    public ResponseEntity<EvidencetypeDTO> updateEvidencetype(@Valid @RequestBody EvidencetypeDTO evidencetypeDTO) throws URISyntaxException {
        log.debug("REST request to update Evidencetype : {}", evidencetypeDTO);
        if (evidencetypeDTO.getId() == null) {
            return createEvidencetype(evidencetypeDTO);
        }
        EvidencetypeDTO result = evidencetypeService.save(evidencetypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evidencetypeDTO.getName()))
            .body(result);
    }

    /**
     * GET  /evidencetypes : get all the evidencetypes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of evidencetypes in body
     */
    @GetMapping("/evidencetypes")
    @Timed
    public ResponseEntity<List<EvidencetypeDTO>> getAllEvidencetypes(EvidencetypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Evidencetypes by criteria: {}", criteria);
        Page<EvidencetypeDTO> page = evidencetypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evidencetypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evidencetypes/:id : get the "id" evidencetype.
     *
     * @param id the id of the evidencetypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evidencetypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/evidencetypes/{id}")
    @Timed
    public ResponseEntity<EvidencetypeDTO> getEvidencetype(@PathVariable Long id) {
        log.debug("REST request to get Evidencetype : {}", id);
        EvidencetypeDTO evidencetypeDTO = evidencetypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evidencetypeDTO));
    }

    /**
     * DELETE  /evidencetypes/:id : delete the "id" evidencetype.
     *
     * @param id the id of the evidencetypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evidencetypes/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvidencetype(@PathVariable Long id) {
        log.debug("REST request to delete Evidencetype : {}", id);
        EvidencetypeDTO evidencetypeDTO = evidencetypeService.findOne(id);
        evidencetypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, evidencetypeDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/evidencetypes?query=:query : search for the evidencetype corresponding
     * to the query.
     *
     * @param query the query of the evidencetype search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/evidencetypes")
    @Timed
    public ResponseEntity<List<EvidencetypeDTO>> searchEvidencetypes(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Evidencetypes for query {}", query);
        Page<EvidencetypeDTO> page = evidencetypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/evidencetypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
