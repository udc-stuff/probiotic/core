package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.DoseformService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.DoseformDTO;
import es.udc.jose.service.dto.DoseformCriteria;
import es.udc.jose.service.DoseformQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Doseform.
 */
@RestController
@RequestMapping("/api")
public class DoseformResource {

    private final Logger log = LoggerFactory.getLogger(DoseformResource.class);

    private static final String ENTITY_NAME = "doseform";

    private final DoseformService doseformService;

    private final DoseformQueryService doseformQueryService;

    public DoseformResource(DoseformService doseformService, DoseformQueryService doseformQueryService) {
        this.doseformService = doseformService;
        this.doseformQueryService = doseformQueryService;
    }

    /**
     * POST  /doseforms : Create a new doseform.
     *
     * @param doseformDTO the doseformDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new doseformDTO, or with status 400 (Bad Request) if the doseform has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/doseforms")
    @Timed
    public ResponseEntity<DoseformDTO> createDoseform(@Valid @RequestBody DoseformDTO doseformDTO) throws URISyntaxException {
        log.debug("REST request to save Doseform : {}", doseformDTO);
        if (doseformDTO.getId() != null) {
            throw new BadRequestAlertException("A new doseform cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DoseformDTO result = doseformService.save(doseformDTO);
        return ResponseEntity.created(new URI("/api/doseforms/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /doseforms : Updates an existing doseform.
     *
     * @param doseformDTO the doseformDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated doseformDTO,
     * or with status 400 (Bad Request) if the doseformDTO is not valid,
     * or with status 500 (Internal Server Error) if the doseformDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/doseforms")
    @Timed
    public ResponseEntity<DoseformDTO> updateDoseform(@Valid @RequestBody DoseformDTO doseformDTO) throws URISyntaxException {
        log.debug("REST request to update Doseform : {}", doseformDTO);
        if (doseformDTO.getId() == null) {
            return createDoseform(doseformDTO);
        }
        DoseformDTO result = doseformService.save(doseformDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, doseformDTO.getName()))
            .body(result);
    }

    /**
     * GET  /doseforms : get all the doseforms.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of doseforms in body
     */
    @GetMapping("/doseforms")
    @Timed
    public ResponseEntity<List<DoseformDTO>> getAllDoseforms(DoseformCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Doseforms by criteria: {}", criteria);
        Page<DoseformDTO> page = doseformQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/doseforms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /doseforms/:id : get the "id" doseform.
     *
     * @param id the id of the doseformDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the doseformDTO, or with status 404 (Not Found)
     */
    @GetMapping("/doseforms/{id}")
    @Timed
    public ResponseEntity<DoseformDTO> getDoseform(@PathVariable Long id) {
        log.debug("REST request to get Doseform : {}", id);
        DoseformDTO doseformDTO = doseformService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(doseformDTO));
    }

    /**
     * DELETE  /doseforms/:id : delete the "id" doseform.
     *
     * @param id the id of the doseformDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/doseforms/{id}")
    @Timed
    public ResponseEntity<Void> deleteDoseform(@PathVariable Long id) {
        log.debug("REST request to delete Doseform : {}", id);
        DoseformDTO doseformDTO = doseformService.findOne(id);
        doseformService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, doseformDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/doseforms?query=:query : search for the doseform corresponding
     * to the query.
     *
     * @param query the query of the doseform search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/doseforms")
    @Timed
    public ResponseEntity<List<DoseformDTO>> searchDoseforms(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Doseforms for query {}", query);
        Page<DoseformDTO> page = doseformService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/doseforms");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
