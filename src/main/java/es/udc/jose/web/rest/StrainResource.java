package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.StrainService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.StrainDTO;
import es.udc.jose.service.dto.StrainCriteria;
import es.udc.jose.service.StrainQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Strain.
 */
@RestController
@RequestMapping("/api")
public class StrainResource {

    private final Logger log = LoggerFactory.getLogger(StrainResource.class);

    private static final String ENTITY_NAME = "strain";

    private final StrainService strainService;

    private final StrainQueryService strainQueryService;

    public StrainResource(StrainService strainService, StrainQueryService strainQueryService) {
        this.strainService = strainService;
        this.strainQueryService = strainQueryService;
    }

    /**
     * POST  /strains : Create a new strain.
     *
     * @param strainDTO the strainDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new strainDTO, or with status 400 (Bad Request) if the strain has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/strains")
    @Timed
    public ResponseEntity<StrainDTO> createStrain(@Valid @RequestBody StrainDTO strainDTO) throws URISyntaxException {
        log.debug("REST request to save Strain : {}", strainDTO);
        if (strainDTO.getId() != null) {
            throw new BadRequestAlertException("A new strain cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StrainDTO result = strainService.save(strainDTO);
        return ResponseEntity.created(new URI("/api/strains/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /strains : Updates an existing strain.
     *
     * @param strainDTO the strainDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated strainDTO,
     * or with status 400 (Bad Request) if the strainDTO is not valid,
     * or with status 500 (Internal Server Error) if the strainDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/strains")
    @Timed
    public ResponseEntity<StrainDTO> updateStrain(@Valid @RequestBody StrainDTO strainDTO) throws URISyntaxException {
        log.debug("REST request to update Strain : {}", strainDTO);
        if (strainDTO.getId() == null) {
            return createStrain(strainDTO);
        }
        StrainDTO result = strainService.save(strainDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, strainDTO.getName()))
            .body(result);
    }

    /**
     * GET  /strains : get all the strains.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of strains in body
     */
    @GetMapping("/strains")
    @Timed
    public ResponseEntity<List<StrainDTO>> getAllStrains(StrainCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Strains by criteria: {}", criteria);
        Page<StrainDTO> page = strainQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/strains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /strains/:id : get the "id" strain.
     *
     * @param id the id of the strainDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the strainDTO, or with status 404 (Not Found)
     */
    @GetMapping("/strains/{id}")
    @Timed
    public ResponseEntity<StrainDTO> getStrain(@PathVariable Long id) {
        log.debug("REST request to get Strain : {}", id);
        StrainDTO strainDTO = strainService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(strainDTO));
    }

    /**
     * DELETE  /strains/:id : delete the "id" strain.
     *
     * @param id the id of the strainDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/strains/{id}")
    @Timed
    public ResponseEntity<Void> deleteStrain(@PathVariable Long id) {
        log.debug("REST request to delete Strain : {}", id);
        StrainDTO strainDTO = strainService.findOne(id);
        strainService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, strainDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/strains?query=:query : search for the strain corresponding
     * to the query.
     *
     * @param query the query of the strain search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/strains")
    @Timed
    public ResponseEntity<List<StrainDTO>> searchStrains(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Strains for query {}", query);
        Page<StrainDTO> page = strainService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/strains");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
