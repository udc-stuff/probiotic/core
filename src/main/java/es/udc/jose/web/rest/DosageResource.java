package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.DosageService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.DosageDTO;
import es.udc.jose.service.dto.DosageCriteria;
import es.udc.jose.service.DosageQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Dosage.
 */
@RestController
@RequestMapping("/api")
public class DosageResource {

    private final Logger log = LoggerFactory.getLogger(DosageResource.class);

    private static final String ENTITY_NAME = "dosage";

    private final DosageService dosageService;

    private final DosageQueryService dosageQueryService;

    public DosageResource(DosageService dosageService, DosageQueryService dosageQueryService) {
        this.dosageService = dosageService;
        this.dosageQueryService = dosageQueryService;
    }

    /**
     * POST  /dosages : Create a new dosage.
     *
     * @param dosageDTO the dosageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dosageDTO, or with status 400 (Bad Request) if the dosage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dosages")
    @Timed
    public ResponseEntity<DosageDTO> createDosage(@Valid @RequestBody DosageDTO dosageDTO) throws URISyntaxException {
        log.debug("REST request to save Dosage : {}", dosageDTO);
        if (dosageDTO.getId() != null) {
            throw new BadRequestAlertException("A new dosage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DosageDTO result = dosageService.save(dosageDTO);
        return ResponseEntity.created(new URI("/api/dosages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dosages : Updates an existing dosage.
     *
     * @param dosageDTO the dosageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dosageDTO,
     * or with status 400 (Bad Request) if the dosageDTO is not valid,
     * or with status 500 (Internal Server Error) if the dosageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dosages")
    @Timed
    public ResponseEntity<DosageDTO> updateDosage(@Valid @RequestBody DosageDTO dosageDTO) throws URISyntaxException {
        log.debug("REST request to update Dosage : {}", dosageDTO);
        if (dosageDTO.getId() == null) {
            return createDosage(dosageDTO);
        }
        DosageDTO result = dosageService.save(dosageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dosageDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dosages : get all the dosages.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of dosages in body
     */
    @GetMapping("/dosages")
    @Timed
    public ResponseEntity<List<DosageDTO>> getAllDosages(DosageCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Dosages by criteria: {}", criteria);
        Page<DosageDTO> page = dosageQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dosages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /dosages/:id : get the "id" dosage.
     *
     * @param id the id of the dosageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dosageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dosages/{id}")
    @Timed
    public ResponseEntity<DosageDTO> getDosage(@PathVariable Long id) {
        log.debug("REST request to get Dosage : {}", id);
        DosageDTO dosageDTO = dosageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dosageDTO));
    }

    /**
     * DELETE  /dosages/:id : delete the "id" dosage.
     *
     * @param id the id of the dosageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dosages/{id}")
    @Timed
    public ResponseEntity<Void> deleteDosage(@PathVariable Long id) {
        log.debug("REST request to delete Dosage : {}", id);
        dosageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dosages?query=:query : search for the dosage corresponding
     * to the query.
     *
     * @param query the query of the dosage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/dosages")
    @Timed
    public ResponseEntity<List<DosageDTO>> searchDosages(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Dosages for query {}", query);
        Page<DosageDTO> page = dosageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/dosages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
