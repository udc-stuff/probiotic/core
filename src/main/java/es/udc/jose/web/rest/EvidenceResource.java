package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.EvidenceService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.EvidenceDTO;
import es.udc.jose.service.dto.EvidenceCriteria;
import es.udc.jose.service.EvidenceQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Evidence.
 */
@RestController
@RequestMapping("/api")
public class EvidenceResource {

    private final Logger log = LoggerFactory.getLogger(EvidenceResource.class);

    private static final String ENTITY_NAME = "evidence";

    private final EvidenceService evidenceService;

    private final EvidenceQueryService evidenceQueryService;

    public EvidenceResource(EvidenceService evidenceService, EvidenceQueryService evidenceQueryService) {
        this.evidenceService = evidenceService;
        this.evidenceQueryService = evidenceQueryService;
    }

    /**
     * POST  /evidences : Create a new evidence.
     *
     * @param evidenceDTO the evidenceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new evidenceDTO, or with status 400 (Bad Request) if the evidence has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/evidences")
    @Timed
    public ResponseEntity<EvidenceDTO> createEvidence(@Valid @RequestBody EvidenceDTO evidenceDTO) throws URISyntaxException {
        log.debug("REST request to save Evidence : {}", evidenceDTO);
        if (evidenceDTO.getId() != null) {
            throw new BadRequestAlertException("A new evidence cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EvidenceDTO result = evidenceService.save(evidenceDTO);
        return ResponseEntity.created(new URI("/api/evidences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /evidences : Updates an existing evidence.
     *
     * @param evidenceDTO the evidenceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated evidenceDTO,
     * or with status 400 (Bad Request) if the evidenceDTO is not valid,
     * or with status 500 (Internal Server Error) if the evidenceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/evidences")
    @Timed
    public ResponseEntity<EvidenceDTO> updateEvidence(@Valid @RequestBody EvidenceDTO evidenceDTO) throws URISyntaxException {
        log.debug("REST request to update Evidence : {}", evidenceDTO);
        if (evidenceDTO.getId() == null) {
            return createEvidence(evidenceDTO);
        }
        EvidenceDTO result = evidenceService.save(evidenceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, evidenceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /evidences : get all the evidences.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of evidences in body
     */
    @GetMapping("/evidences")
    @Timed
    public ResponseEntity<List<EvidenceDTO>> getAllEvidences(EvidenceCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Evidences by criteria: {}", criteria);
        Page<EvidenceDTO> page = evidenceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/evidences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /evidences/:id : get the "id" evidence.
     *
     * @param id the id of the evidenceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the evidenceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/evidences/{id}")
    @Timed
    public ResponseEntity<EvidenceDTO> getEvidence(@PathVariable Long id) {
        log.debug("REST request to get Evidence : {}", id);
        EvidenceDTO evidenceDTO = evidenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(evidenceDTO));
    }

    /**
     * DELETE  /evidences/:id : delete the "id" evidence.
     *
     * @param id the id of the evidenceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/evidences/{id}")
    @Timed
    public ResponseEntity<Void> deleteEvidence(@PathVariable Long id) {
        log.debug("REST request to delete Evidence : {}", id);
        evidenceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/evidences?query=:query : search for the evidence corresponding
     * to the query.
     *
     * @param query the query of the evidence search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/evidences")
    @Timed
    public ResponseEntity<List<EvidenceDTO>> searchEvidences(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Evidences for query {}", query);
        Page<EvidenceDTO> page = evidenceService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/evidences");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
