package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.ProbioticService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.ProbioticDTO;
import es.udc.jose.service.dto.ProbioticCriteria;
import es.udc.jose.service.ProbioticQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Probiotic.
 */
@RestController
@RequestMapping("/api")
public class ProbioticResource {

    private final Logger log = LoggerFactory.getLogger(ProbioticResource.class);

    private static final String ENTITY_NAME = "probiotic";

    private final ProbioticService probioticService;

    private final ProbioticQueryService probioticQueryService;

    public ProbioticResource(ProbioticService probioticService, ProbioticQueryService probioticQueryService) {
        this.probioticService = probioticService;
        this.probioticQueryService = probioticQueryService;
    }

    /**
     * POST  /probiotics : Create a new probiotic.
     *
     * @param probioticDTO the probioticDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new probioticDTO, or with status 400 (Bad Request) if the probiotic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/probiotics")
    @Timed
    public ResponseEntity<ProbioticDTO> createProbiotic(@Valid @RequestBody ProbioticDTO probioticDTO) throws URISyntaxException {
        log.debug("REST request to save Probiotic : {}", probioticDTO);
        if (probioticDTO.getId() != null) {
            throw new BadRequestAlertException("A new probiotic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProbioticDTO result = probioticService.save(probioticDTO);
        return ResponseEntity.created(new URI("/api/probiotics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /probiotics : Updates an existing probiotic.
     *
     * @param probioticDTO the probioticDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated probioticDTO,
     * or with status 400 (Bad Request) if the probioticDTO is not valid,
     * or with status 500 (Internal Server Error) if the probioticDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/probiotics")
    @Timed
    public ResponseEntity<ProbioticDTO> updateProbiotic(@Valid @RequestBody ProbioticDTO probioticDTO) throws URISyntaxException {
        log.debug("REST request to update Probiotic : {}", probioticDTO);
        if (probioticDTO.getId() == null) {
            return createProbiotic(probioticDTO);
        }
        ProbioticDTO result = probioticService.save(probioticDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, probioticDTO.getName()))
            .body(result);
    }

    /**
     * GET  /probiotics : get all the probiotics.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of probiotics in body
     */
    @GetMapping("/probiotics")
    @Timed
    public ResponseEntity<List<ProbioticDTO>> getAllProbiotics(ProbioticCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Probiotics by criteria: {}", criteria);
        Page<ProbioticDTO> page = probioticQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/probiotics");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /probiotics/:id : get the "id" probiotic.
     *
     * @param id the id of the probioticDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the probioticDTO, or with status 404 (Not Found)
     */
    @GetMapping("/probiotics/{id}")
    @Timed
    public ResponseEntity<ProbioticDTO> getProbiotic(@PathVariable Long id) {
        log.debug("REST request to get Probiotic : {}", id);
        ProbioticDTO probioticDTO = probioticService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(probioticDTO));
    }

    /**
     * DELETE  /probiotics/:id : delete the "id" probiotic.
     *
     * @param id the id of the probioticDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/probiotics/{id}")
    @Timed
    public ResponseEntity<Void> deleteProbiotic(@PathVariable Long id) {
        log.debug("REST request to delete Probiotic : {}", id);
        ProbioticDTO probioticDTO = probioticService.findOne(id);
        probioticService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, probioticDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/probiotics?query=:query : search for the probiotic corresponding
     * to the query.
     *
     * @param query the query of the probiotic search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/probiotics")
    @Timed
    public ResponseEntity<List<ProbioticDTO>> searchProbiotics(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Probiotics for query {}", query);
        Page<ProbioticDTO> page = probioticService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/probiotics");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
