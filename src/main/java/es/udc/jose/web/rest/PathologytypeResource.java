package es.udc.jose.web.rest;

import com.codahale.metrics.annotation.Timed;
import es.udc.jose.service.PathologytypeService;
import es.udc.jose.web.rest.errors.BadRequestAlertException;
import es.udc.jose.web.rest.util.HeaderUtil;
import es.udc.jose.web.rest.util.PaginationUtil;
import es.udc.jose.service.dto.PathologytypeDTO;
import es.udc.jose.service.dto.PathologytypeCriteria;
import es.udc.jose.service.PathologytypeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pathologytype.
 */
@RestController
@RequestMapping("/api")
public class PathologytypeResource {

    private final Logger log = LoggerFactory.getLogger(PathologytypeResource.class);

    private static final String ENTITY_NAME = "pathologytype";

    private final PathologytypeService pathologytypeService;

    private final PathologytypeQueryService pathologytypeQueryService;

    public PathologytypeResource(PathologytypeService pathologytypeService, PathologytypeQueryService pathologytypeQueryService) {
        this.pathologytypeService = pathologytypeService;
        this.pathologytypeQueryService = pathologytypeQueryService;
    }

    /**
     * POST  /pathologytypes : Create a new pathologytype.
     *
     * @param pathologytypeDTO the pathologytypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pathologytypeDTO, or with status 400 (Bad Request) if the pathologytype has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pathologytypes")
    @Timed
    public ResponseEntity<PathologytypeDTO> createPathologytype(@Valid @RequestBody PathologytypeDTO pathologytypeDTO) throws URISyntaxException {
        log.debug("REST request to save Pathologytype : {}", pathologytypeDTO);
        if (pathologytypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new pathologytype cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PathologytypeDTO result = pathologytypeService.save(pathologytypeDTO);
        return ResponseEntity.created(new URI("/api/pathologytypes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * PUT  /pathologytypes : Updates an existing pathologytype.
     *
     * @param pathologytypeDTO the pathologytypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pathologytypeDTO,
     * or with status 400 (Bad Request) if the pathologytypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the pathologytypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pathologytypes")
    @Timed
    public ResponseEntity<PathologytypeDTO> updatePathologytype(@Valid @RequestBody PathologytypeDTO pathologytypeDTO) throws URISyntaxException {
        log.debug("REST request to update Pathologytype : {}", pathologytypeDTO);
        if (pathologytypeDTO.getId() == null) {
            return createPathologytype(pathologytypeDTO);
        }
        PathologytypeDTO result = pathologytypeService.save(pathologytypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pathologytypeDTO.getName()))
            .body(result);
    }

    /**
     * GET  /pathologytypes : get all the pathologytypes.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of pathologytypes in body
     */
    @GetMapping("/pathologytypes")
    @Timed
    public ResponseEntity<List<PathologytypeDTO>> getAllPathologytypes(PathologytypeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Pathologytypes by criteria: {}", criteria);
        Page<PathologytypeDTO> page = pathologytypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pathologytypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pathologytypes/:id : get the "id" pathologytype.
     *
     * @param id the id of the pathologytypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pathologytypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/pathologytypes/{id}")
    @Timed
    public ResponseEntity<PathologytypeDTO> getPathologytype(@PathVariable Long id) {
        log.debug("REST request to get Pathologytype : {}", id);
        PathologytypeDTO pathologytypeDTO = pathologytypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pathologytypeDTO));
    }

    /**
     * DELETE  /pathologytypes/:id : delete the "id" pathologytype.
     *
     * @param id the id of the pathologytypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pathologytypes/{id}")
    @Timed
    public ResponseEntity<Void> deletePathologytype(@PathVariable Long id) {
        log.debug("REST request to delete Pathologytype : {}", id);
        PathologytypeDTO pathologytypeDTO = pathologytypeService.findOne(id);
        pathologytypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, pathologytypeDTO.getName())).build();
    }

    /**
     * SEARCH  /_search/pathologytypes?query=:query : search for the pathologytype corresponding
     * to the query.
     *
     * @param query the query of the pathologytype search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/pathologytypes")
    @Timed
    public ResponseEntity<List<PathologytypeDTO>> searchPathologytypes(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Pathologytypes for query {}", query);
        Page<PathologytypeDTO> page = pathologytypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/pathologytypes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
