package es.udc.jose.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Evidence.
 */
@Entity
@Table(name = "evidence")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "evidence")
public class Evidence implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @ManyToOne(optional = false)
    @NotNull
    private Pathology pathology;

    @ManyToOne(optional = false)
    @NotNull
    private Dosage dosage;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "evidence_evidencereferences",
               joinColumns = @JoinColumn(name="evidences_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="evidencereferences_id", referencedColumnName="id"))
    private Set<Evidencereferences> evidencereferences = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private Evidencetype evidencetype;

    @ManyToOne(optional = false)
    @NotNull
    private User modifiedby;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public Evidence timestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Pathology getPathology() {
        return pathology;
    }

    public Evidence pathology(Pathology pathology) {
        this.pathology = pathology;
        return this;
    }

    public void setPathology(Pathology pathology) {
        this.pathology = pathology;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public Evidence dosage(Dosage dosage) {
        this.dosage = dosage;
        return this;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public Set<Evidencereferences> getEvidencereferences() {
        return evidencereferences;
    }

    public Evidence evidencereferences(Set<Evidencereferences> evidencereferences) {
        this.evidencereferences = evidencereferences;
        return this;
    }

    public Evidence addEvidencereferences(Evidencereferences evidencereferences) {
        this.evidencereferences.add(evidencereferences);
        return this;
    }

    public Evidence removeEvidencereferences(Evidencereferences evidencereferences) {
        this.evidencereferences.remove(evidencereferences);
        return this;
    }

    public void setEvidencereferences(Set<Evidencereferences> evidencereferences) {
        this.evidencereferences = evidencereferences;
    }

    public Evidencetype getEvidencetype() {
        return evidencetype;
    }

    public Evidence evidencetype(Evidencetype evidencetype) {
        this.evidencetype = evidencetype;
        return this;
    }

    public void setEvidencetype(Evidencetype evidencetype) {
        this.evidencetype = evidencetype;
    }

    public User getModifiedby() {
        return modifiedby;
    }

    public Evidence modifiedby(User user) {
        this.modifiedby = user;
        return this;
    }

    public void setModifiedby(User user) {
        this.modifiedby = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Evidence evidence = (Evidence) o;
        if (evidence.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evidence.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Evidence{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }
}
