package es.udc.jose.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import es.udc.jose.domain.enumeration.Age;

/**
 * A Dosage.
 */
@Entity
@Table(name = "dosage")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "dosage")
public class Dosage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "age", nullable = false)
    private Age age;

    @NotNull
    @Column(name = "intake", nullable = false)
    private String intake;

    @NotNull
    @Column(name = "frequency", nullable = false)
    private String frequency;

    @Column(name = "indication")
    private String indication;

    @NotNull
    @Column(name = "cfu", nullable = false)
    private Long cfu;

    @ManyToOne(optional = false)
    @NotNull
    private Probiotic probiotic;

    @ManyToOne(optional = false)
    @NotNull
    private Doseform doseform;

    @ManyToOne(optional = false)
    @NotNull
    private User modifiedby;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public Dosage timestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Age getAge() {
        return age;
    }

    public Dosage age(Age age) {
        this.age = age;
        return this;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public String getIntake() {
        return intake;
    }

    public Dosage intake(String intake) {
        this.intake = intake;
        return this;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public String getFrequency() {
        return frequency;
    }

    public Dosage frequency(String frequency) {
        this.frequency = frequency;
        return this;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getIndication() {
        return indication;
    }

    public Dosage indication(String indication) {
        this.indication = indication;
        return this;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public Long getCfu() {
        return cfu;
    }

    public Dosage cfu(Long cfu) {
        this.cfu = cfu;
        return this;
    }

    public void setCfu(Long cfu) {
        this.cfu = cfu;
    }

    public Probiotic getProbiotic() {
        return probiotic;
    }

    public Dosage probiotic(Probiotic probiotic) {
        this.probiotic = probiotic;
        return this;
    }

    public void setProbiotic(Probiotic probiotic) {
        this.probiotic = probiotic;
    }

    public Doseform getDoseform() {
        return doseform;
    }

    public Dosage doseform(Doseform doseform) {
        this.doseform = doseform;
        return this;
    }

    public void setDoseform(Doseform doseform) {
        this.doseform = doseform;
    }

    public User getModifiedby() {
        return modifiedby;
    }

    public Dosage modifiedby(User user) {
        this.modifiedby = user;
        return this;
    }

    public void setModifiedby(User user) {
        this.modifiedby = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dosage dosage = (Dosage) o;
        if (dosage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dosage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Dosage{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", age='" + getAge() + "'" +
            ", intake='" + getIntake() + "'" +
            ", frequency='" + getFrequency() + "'" +
            ", indication='" + getIndication() + "'" +
            ", cfu=" + getCfu() +
            "}";
    }
}
