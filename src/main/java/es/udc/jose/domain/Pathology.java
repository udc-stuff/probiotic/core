package es.udc.jose.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Pathology.
 */
@Entity
@Table(name = "pathology")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "pathology")
public class Pathology implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "jhi_timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @NotNull
    @Column(name = "acronym", nullable = false)
    private String acronym;

    @ManyToOne(optional = false)
    @NotNull
    private User modifiedby;

    @ManyToOne(optional = false)
    @NotNull
    private Pathologytype pathologytype;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Pathology name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public Pathology timestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAcronym() {
        return acronym;
    }

    public Pathology acronym(String acronym) {
        this.acronym = acronym;
        return this;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public User getModifiedby() {
        return modifiedby;
    }

    public Pathology modifiedby(User user) {
        this.modifiedby = user;
        return this;
    }

    public void setModifiedby(User user) {
        this.modifiedby = user;
    }

    public Pathologytype getPathologytype() {
        return pathologytype;
    }

    public Pathology pathologytype(Pathologytype pathologytype) {
        this.pathologytype = pathologytype;
        return this;
    }

    public void setPathologytype(Pathologytype pathologytype) {
        this.pathologytype = pathologytype;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pathology pathology = (Pathology) o;
        if (pathology.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pathology.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pathology{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", acronym='" + getAcronym() + "'" +
            "}";
    }
}
