package es.udc.jose.domain.enumeration;

/**
 * The Age enumeration.
 */
public enum Age {
    Adulto,  Pediatrico
}
