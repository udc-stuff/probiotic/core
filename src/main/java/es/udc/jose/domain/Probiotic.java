package es.udc.jose.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Probiotic.
 */
@Entity
@Table(name = "probiotic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "probiotic")
public class Probiotic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "jhi_timestamp", nullable = false)
    private ZonedDateTime timestamp;

    @NotNull
    @Column(name = "have_allergens", nullable = false)
    private Boolean have_allergens;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "probiotic_allergens",
               joinColumns = @JoinColumn(name="probiotics_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="allergens_id", referencedColumnName="id"))
    private Set<Allergens> allergens = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "probiotic_strain",
               joinColumns = @JoinColumn(name="probiotics_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="strains_id", referencedColumnName="id"))
    private Set<Strain> strains = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private User modifiedby;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Probiotic name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public Probiotic timestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Boolean isHave_allergens() {
        return have_allergens;
    }

    public Probiotic have_allergens(Boolean have_allergens) {
        this.have_allergens = have_allergens;
        return this;
    }

    public void setHave_allergens(Boolean have_allergens) {
        this.have_allergens = have_allergens;
    }

    public Set<Allergens> getAllergens() {
        return allergens;
    }

    public Probiotic allergens(Set<Allergens> allergens) {
        this.allergens = allergens;
        return this;
    }

    public Probiotic addAllergens(Allergens allergens) {
        this.allergens.add(allergens);
        return this;
    }

    public Probiotic removeAllergens(Allergens allergens) {
        this.allergens.remove(allergens);
        return this;
    }

    public void setAllergens(Set<Allergens> allergens) {
        this.allergens = allergens;
    }

    public Set<Strain> getStrains() {
        return strains;
    }

    public Probiotic strains(Set<Strain> strains) {
        this.strains = strains;
        return this;
    }

    public Probiotic addStrain(Strain strain) {
        this.strains.add(strain);
        return this;
    }

    public Probiotic removeStrain(Strain strain) {
        this.strains.remove(strain);
        return this;
    }

    public void setStrains(Set<Strain> strains) {
        this.strains = strains;
    }

    public User getModifiedby() {
        return modifiedby;
    }

    public Probiotic modifiedby(User user) {
        this.modifiedby = user;
        return this;
    }

    public void setModifiedby(User user) {
        this.modifiedby = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Probiotic probiotic = (Probiotic) o;
        if (probiotic.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), probiotic.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Probiotic{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            ", have_allergens='" + isHave_allergens() + "'" +
            "}";
    }
}
