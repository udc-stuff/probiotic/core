package es.udc.jose.repository;

import es.udc.jose.domain.Strain;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Strain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StrainRepository extends JpaRepository<Strain, Long>, JpaSpecificationExecutor<Strain> {

    @Query("select strain from Strain strain where strain.modifiedby.login = ?#{principal.username}")
    List<Strain> findByModifiedbyIsCurrentUser();

}
