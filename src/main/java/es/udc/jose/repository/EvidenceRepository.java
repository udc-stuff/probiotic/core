package es.udc.jose.repository;

import es.udc.jose.domain.Evidence;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Evidence entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvidenceRepository extends JpaRepository<Evidence, Long>, JpaSpecificationExecutor<Evidence> {

    @Query("select evidence from Evidence evidence where evidence.modifiedby.login = ?#{principal.username}")
    List<Evidence> findByModifiedbyIsCurrentUser();
    @Query("select distinct evidence from Evidence evidence left join fetch evidence.evidencereferences")
    List<Evidence> findAllWithEagerRelationships();

    @Query("select evidence from Evidence evidence left join fetch evidence.evidencereferences where evidence.id =:id")
    Evidence findOneWithEagerRelationships(@Param("id") Long id);

}
