package es.udc.jose.repository;

import es.udc.jose.domain.Probiotic;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Probiotic entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProbioticRepository extends JpaRepository<Probiotic, Long>, JpaSpecificationExecutor<Probiotic> {

    @Query("select probiotic from Probiotic probiotic where probiotic.modifiedby.login = ?#{principal.username}")
    List<Probiotic> findByModifiedbyIsCurrentUser();
    @Query("select distinct probiotic from Probiotic probiotic left join fetch probiotic.allergens left join fetch probiotic.strains")
    List<Probiotic> findAllWithEagerRelationships();

    @Query("select probiotic from Probiotic probiotic left join fetch probiotic.allergens left join fetch probiotic.strains where probiotic.id =:id")
    Probiotic findOneWithEagerRelationships(@Param("id") Long id);

}
