package es.udc.jose.repository;

import es.udc.jose.domain.Doseform;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Doseform entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DoseformRepository extends JpaRepository<Doseform, Long>, JpaSpecificationExecutor<Doseform> {

}
