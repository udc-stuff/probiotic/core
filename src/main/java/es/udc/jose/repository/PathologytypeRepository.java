package es.udc.jose.repository;

import es.udc.jose.domain.Pathologytype;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pathologytype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PathologytypeRepository extends JpaRepository<Pathologytype, Long>, JpaSpecificationExecutor<Pathologytype> {

}
