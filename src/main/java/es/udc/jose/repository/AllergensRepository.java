package es.udc.jose.repository;

import es.udc.jose.domain.Allergens;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Allergens entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AllergensRepository extends JpaRepository<Allergens, Long>, JpaSpecificationExecutor<Allergens> {

}
