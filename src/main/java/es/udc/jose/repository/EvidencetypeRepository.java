package es.udc.jose.repository;

import es.udc.jose.domain.Evidencetype;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Evidencetype entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvidencetypeRepository extends JpaRepository<Evidencetype, Long>, JpaSpecificationExecutor<Evidencetype> {

}
