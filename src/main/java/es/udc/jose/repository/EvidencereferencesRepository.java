package es.udc.jose.repository;

import es.udc.jose.domain.Evidencereferences;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Evidencereferences entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EvidencereferencesRepository extends JpaRepository<Evidencereferences, Long>, JpaSpecificationExecutor<Evidencereferences> {

    @Query("select evidencereferences from Evidencereferences evidencereferences where evidencereferences.modifiedby.login = ?#{principal.username}")
    List<Evidencereferences> findByModifiedbyIsCurrentUser();

}
