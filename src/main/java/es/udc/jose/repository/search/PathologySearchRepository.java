package es.udc.jose.repository.search;

import es.udc.jose.domain.Pathology;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Pathology entity.
 */
public interface PathologySearchRepository extends ElasticsearchRepository<Pathology, Long> {
}
