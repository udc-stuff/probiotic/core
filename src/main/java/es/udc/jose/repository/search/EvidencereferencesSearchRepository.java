package es.udc.jose.repository.search;

import es.udc.jose.domain.Evidencereferences;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Evidencereferences entity.
 */
public interface EvidencereferencesSearchRepository extends ElasticsearchRepository<Evidencereferences, Long> {
}
