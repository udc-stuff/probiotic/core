package es.udc.jose.repository.search;

import es.udc.jose.domain.Pathologytype;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Pathologytype entity.
 */
public interface PathologytypeSearchRepository extends ElasticsearchRepository<Pathologytype, Long> {
}
