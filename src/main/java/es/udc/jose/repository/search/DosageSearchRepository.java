package es.udc.jose.repository.search;

import es.udc.jose.domain.Dosage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Dosage entity.
 */
public interface DosageSearchRepository extends ElasticsearchRepository<Dosage, Long> {
}
