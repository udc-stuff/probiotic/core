package es.udc.jose.repository.search;

import es.udc.jose.domain.Probiotic;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Probiotic entity.
 */
public interface ProbioticSearchRepository extends ElasticsearchRepository<Probiotic, Long> {
}
