package es.udc.jose.repository.search;

import es.udc.jose.domain.Evidence;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Evidence entity.
 */
public interface EvidenceSearchRepository extends ElasticsearchRepository<Evidence, Long> {
}
