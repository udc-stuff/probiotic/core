package es.udc.jose.repository.search;

import es.udc.jose.domain.Strain;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Strain entity.
 */
public interface StrainSearchRepository extends ElasticsearchRepository<Strain, Long> {
}
