package es.udc.jose.repository.search;

import es.udc.jose.domain.Evidencetype;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Evidencetype entity.
 */
public interface EvidencetypeSearchRepository extends ElasticsearchRepository<Evidencetype, Long> {
}
