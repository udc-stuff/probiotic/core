package es.udc.jose.repository.search;

import es.udc.jose.domain.Allergens;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Allergens entity.
 */
public interface AllergensSearchRepository extends ElasticsearchRepository<Allergens, Long> {
}
