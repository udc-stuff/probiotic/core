package es.udc.jose.repository.search;

import es.udc.jose.domain.Doseform;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Doseform entity.
 */
public interface DoseformSearchRepository extends ElasticsearchRepository<Doseform, Long> {
}
