package es.udc.jose.repository;

import es.udc.jose.domain.Dosage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Dosage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DosageRepository extends JpaRepository<Dosage, Long>, JpaSpecificationExecutor<Dosage> {

    @Query("select dosage from Dosage dosage where dosage.modifiedby.login = ?#{principal.username}")
    List<Dosage> findByModifiedbyIsCurrentUser();

}
