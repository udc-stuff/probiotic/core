package es.udc.jose.repository;

import es.udc.jose.domain.Pathology;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Pathology entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PathologyRepository extends JpaRepository<Pathology, Long>, JpaSpecificationExecutor<Pathology> {

    @Query("select pathology from Pathology pathology where pathology.modifiedby.login = ?#{principal.username}")
    List<Pathology> findByModifiedbyIsCurrentUser();

}
