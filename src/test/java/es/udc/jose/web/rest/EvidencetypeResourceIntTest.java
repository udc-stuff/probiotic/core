package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Evidencetype;
import es.udc.jose.repository.EvidencetypeRepository;
import es.udc.jose.service.EvidencetypeService;
import es.udc.jose.repository.search.EvidencetypeSearchRepository;
import es.udc.jose.service.dto.EvidencetypeDTO;
import es.udc.jose.service.mapper.EvidencetypeMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.EvidencetypeCriteria;
import es.udc.jose.service.EvidencetypeQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EvidencetypeResource REST controller.
 *
 * @see EvidencetypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class EvidencetypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private EvidencetypeRepository evidencetypeRepository;

    @Autowired
    private EvidencetypeMapper evidencetypeMapper;

    @Autowired
    private EvidencetypeService evidencetypeService;

    @Autowired
    private EvidencetypeSearchRepository evidencetypeSearchRepository;

    @Autowired
    private EvidencetypeQueryService evidencetypeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEvidencetypeMockMvc;

    private Evidencetype evidencetype;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EvidencetypeResource evidencetypeResource = new EvidencetypeResource(evidencetypeService, evidencetypeQueryService);
        this.restEvidencetypeMockMvc = MockMvcBuilders.standaloneSetup(evidencetypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evidencetype createEntity(EntityManager em) {
        Evidencetype evidencetype = new Evidencetype()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP);
        return evidencetype;
    }

    @Before
    public void initTest() {
        evidencetypeSearchRepository.deleteAll();
        evidencetype = createEntity(em);
    }

    @Test
    @Transactional
    public void createEvidencetype() throws Exception {
        int databaseSizeBeforeCreate = evidencetypeRepository.findAll().size();

        // Create the Evidencetype
        EvidencetypeDTO evidencetypeDTO = evidencetypeMapper.toDto(evidencetype);
        restEvidencetypeMockMvc.perform(post("/api/evidencetypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencetypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidencetype in the database
        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeCreate + 1);
        Evidencetype testEvidencetype = evidencetypeList.get(evidencetypeList.size() - 1);
        assertThat(testEvidencetype.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testEvidencetype.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);

        // Validate the Evidencetype in Elasticsearch
        Evidencetype evidencetypeEs = evidencetypeSearchRepository.findOne(testEvidencetype.getId());
        assertThat(testEvidencetype.getTimestamp()).isEqualTo(testEvidencetype.getTimestamp());
        assertThat(evidencetypeEs).isEqualToIgnoringGivenFields(testEvidencetype, "timestamp");
    }

    @Test
    @Transactional
    public void createEvidencetypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = evidencetypeRepository.findAll().size();

        // Create the Evidencetype with an existing ID
        evidencetype.setId(1L);
        EvidencetypeDTO evidencetypeDTO = evidencetypeMapper.toDto(evidencetype);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvidencetypeMockMvc.perform(post("/api/evidencetypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencetypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Evidencetype in the database
        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = evidencetypeRepository.findAll().size();
        // set the field null
        evidencetype.setName(null);

        // Create the Evidencetype, which fails.
        EvidencetypeDTO evidencetypeDTO = evidencetypeMapper.toDto(evidencetype);

        restEvidencetypeMockMvc.perform(post("/api/evidencetypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencetypeDTO)))
            .andExpect(status().isBadRequest());

        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEvidencetypes() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList
        restEvidencetypeMockMvc.perform(get("/api/evidencetypes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencetype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getEvidencetype() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get the evidencetype
        restEvidencetypeMockMvc.perform(get("/api/evidencetypes/{id}", evidencetype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(evidencetype.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where name equals to DEFAULT_NAME
        defaultEvidencetypeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the evidencetypeList where name equals to UPDATED_NAME
        defaultEvidencetypeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultEvidencetypeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the evidencetypeList where name equals to UPDATED_NAME
        defaultEvidencetypeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where name is not null
        defaultEvidencetypeShouldBeFound("name.specified=true");

        // Get all the evidencetypeList where name is null
        defaultEvidencetypeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where timestamp equals to DEFAULT_TIMESTAMP
        defaultEvidencetypeShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the evidencetypeList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidencetypeShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultEvidencetypeShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the evidencetypeList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidencetypeShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where timestamp is not null
        defaultEvidencetypeShouldBeFound("timestamp.specified=true");

        // Get all the evidencetypeList where timestamp is null
        defaultEvidencetypeShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultEvidencetypeShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidencetypeList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultEvidencetypeShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencetypesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);

        // Get all the evidencetypeList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultEvidencetypeShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidencetypeList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultEvidencetypeShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEvidencetypeShouldBeFound(String filter) throws Exception {
        restEvidencetypeMockMvc.perform(get("/api/evidencetypes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencetype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEvidencetypeShouldNotBeFound(String filter) throws Exception {
        restEvidencetypeMockMvc.perform(get("/api/evidencetypes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingEvidencetype() throws Exception {
        // Get the evidencetype
        restEvidencetypeMockMvc.perform(get("/api/evidencetypes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEvidencetype() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);
        evidencetypeSearchRepository.save(evidencetype);
        int databaseSizeBeforeUpdate = evidencetypeRepository.findAll().size();

        // Update the evidencetype
        Evidencetype updatedEvidencetype = evidencetypeRepository.findOne(evidencetype.getId());
        // Disconnect from session so that the updates on updatedEvidencetype are not directly saved in db
        em.detach(updatedEvidencetype);
        updatedEvidencetype
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP);
        EvidencetypeDTO evidencetypeDTO = evidencetypeMapper.toDto(updatedEvidencetype);

        restEvidencetypeMockMvc.perform(put("/api/evidencetypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencetypeDTO)))
            .andExpect(status().isOk());

        // Validate the Evidencetype in the database
        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeUpdate);
        Evidencetype testEvidencetype = evidencetypeList.get(evidencetypeList.size() - 1);
        assertThat(testEvidencetype.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testEvidencetype.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Evidencetype in Elasticsearch
        Evidencetype evidencetypeEs = evidencetypeSearchRepository.findOne(testEvidencetype.getId());
        assertThat(testEvidencetype.getTimestamp()).isEqualTo(testEvidencetype.getTimestamp());
        assertThat(evidencetypeEs).isEqualToIgnoringGivenFields(testEvidencetype, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingEvidencetype() throws Exception {
        int databaseSizeBeforeUpdate = evidencetypeRepository.findAll().size();

        // Create the Evidencetype
        EvidencetypeDTO evidencetypeDTO = evidencetypeMapper.toDto(evidencetype);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEvidencetypeMockMvc.perform(put("/api/evidencetypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencetypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidencetype in the database
        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEvidencetype() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);
        evidencetypeSearchRepository.save(evidencetype);
        int databaseSizeBeforeDelete = evidencetypeRepository.findAll().size();

        // Get the evidencetype
        restEvidencetypeMockMvc.perform(delete("/api/evidencetypes/{id}", evidencetype.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean evidencetypeExistsInEs = evidencetypeSearchRepository.exists(evidencetype.getId());
        assertThat(evidencetypeExistsInEs).isFalse();

        // Validate the database is empty
        List<Evidencetype> evidencetypeList = evidencetypeRepository.findAll();
        assertThat(evidencetypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEvidencetype() throws Exception {
        // Initialize the database
        evidencetypeRepository.saveAndFlush(evidencetype);
        evidencetypeSearchRepository.save(evidencetype);

        // Search the evidencetype
        restEvidencetypeMockMvc.perform(get("/api/_search/evidencetypes?query=id:" + evidencetype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencetype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Evidencetype.class);
        Evidencetype evidencetype1 = new Evidencetype();
        evidencetype1.setId(1L);
        Evidencetype evidencetype2 = new Evidencetype();
        evidencetype2.setId(evidencetype1.getId());
        assertThat(evidencetype1).isEqualTo(evidencetype2);
        evidencetype2.setId(2L);
        assertThat(evidencetype1).isNotEqualTo(evidencetype2);
        evidencetype1.setId(null);
        assertThat(evidencetype1).isNotEqualTo(evidencetype2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvidencetypeDTO.class);
        EvidencetypeDTO evidencetypeDTO1 = new EvidencetypeDTO();
        evidencetypeDTO1.setId(1L);
        EvidencetypeDTO evidencetypeDTO2 = new EvidencetypeDTO();
        assertThat(evidencetypeDTO1).isNotEqualTo(evidencetypeDTO2);
        evidencetypeDTO2.setId(evidencetypeDTO1.getId());
        assertThat(evidencetypeDTO1).isEqualTo(evidencetypeDTO2);
        evidencetypeDTO2.setId(2L);
        assertThat(evidencetypeDTO1).isNotEqualTo(evidencetypeDTO2);
        evidencetypeDTO1.setId(null);
        assertThat(evidencetypeDTO1).isNotEqualTo(evidencetypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(evidencetypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(evidencetypeMapper.fromId(null)).isNull();
    }
}
