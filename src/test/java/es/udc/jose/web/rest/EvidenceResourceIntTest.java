package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Evidence;
import es.udc.jose.domain.Pathology;
import es.udc.jose.domain.Dosage;
import es.udc.jose.domain.Evidencereferences;
import es.udc.jose.domain.Evidencetype;
import es.udc.jose.domain.User;
import es.udc.jose.repository.EvidenceRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.EvidenceService;
import es.udc.jose.repository.search.EvidenceSearchRepository;
import es.udc.jose.service.dto.EvidenceDTO;
import es.udc.jose.service.mapper.EvidenceMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.EvidenceCriteria;
import es.udc.jose.service.EvidenceQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EvidenceResource REST controller.
 *
 * @see EvidenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class EvidenceResourceIntTest {

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private EvidenceRepository evidenceRepository;

    @Autowired
    private EvidenceMapper evidenceMapper;

    @Autowired
    private EvidenceService evidenceService;

    @Autowired
    private EvidenceSearchRepository evidenceSearchRepository;

    @Autowired
    private EvidenceQueryService evidenceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEvidenceMockMvc;

    private Evidence evidence;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EvidenceResource evidenceResource = new EvidenceResource(evidenceService, evidenceQueryService);
        this.restEvidenceMockMvc = MockMvcBuilders.standaloneSetup(evidenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evidence createEntity(EntityManager em) {
        Evidence evidence = new Evidence()
            .timestamp(DEFAULT_TIMESTAMP);
        // Add required entity
        Pathology pathology = PathologyResourceIntTest.createEntity(em);
        em.persist(pathology);
        em.flush();
        evidence.setPathology(pathology);
        // Add required entity
        Dosage dosage = DosageResourceIntTest.createEntity(em);
        em.persist(dosage);
        em.flush();
        evidence.setDosage(dosage);
        // Add required entity
        Evidencereferences evidencereferences = EvidencereferencesResourceIntTest.createEntity(em);
        em.persist(evidencereferences);
        em.flush();
        evidence.getEvidencereferences().add(evidencereferences);
        // Add required entity
        Evidencetype evidencetype = EvidencetypeResourceIntTest.createEntity(em);
        em.persist(evidencetype);
        em.flush();
        evidence.setEvidencetype(evidencetype);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        evidence.setModifiedby(modifiedby);
        return evidence;
    }

    @Before
    public void initTest() {
        evidenceSearchRepository.deleteAll();
        evidence = createEntity(em);
    }

    @Test
    @Transactional
    public void createEvidence() throws Exception {
        int databaseSizeBeforeCreate = evidenceRepository.findAll().size();
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // Create the Evidence
        EvidenceDTO evidenceDTO = evidenceMapper.toDto(evidence);
        restEvidenceMockMvc.perform(post("/api/evidences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidenceDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidence in the database
        List<Evidence> evidenceList = evidenceRepository.findAll();
        assertThat(evidenceList).hasSize(databaseSizeBeforeCreate + 1);
        Evidence testEvidence = evidenceList.get(evidenceList.size() - 1);
        assertThat(testEvidence.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);

        // Validate the Evidence in Elasticsearch
        Evidence evidenceEs = evidenceSearchRepository.findOne(testEvidence.getId());
        assertThat(testEvidence.getTimestamp()).isEqualTo(testEvidence.getTimestamp());
        assertThat(evidenceEs).isEqualToIgnoringGivenFields(testEvidence, "timestamp");
    }

    @Test
    @Transactional
    public void createEvidenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = evidenceRepository.findAll().size();

        // Create the Evidence with an existing ID
        evidence.setId(1L);
        EvidenceDTO evidenceDTO = evidenceMapper.toDto(evidence);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvidenceMockMvc.perform(post("/api/evidences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Evidence in the database
        List<Evidence> evidenceList = evidenceRepository.findAll();
        assertThat(evidenceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEvidences() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList
        restEvidenceMockMvc.perform(get("/api/evidences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidence.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }


    @Test
    @Transactional
    public void getEvidence() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get the evidence
        restEvidenceMockMvc.perform(get("/api/evidences/{id}", evidence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(evidence.getId().intValue()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllEvidencesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList where timestamp equals to DEFAULT_TIMESTAMP
        defaultEvidenceShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the evidenceList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidenceShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultEvidenceShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the evidenceList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidenceShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList where timestamp is not null
        defaultEvidenceShouldBeFound("timestamp.specified=true");

        // Get all the evidenceList where timestamp is null
        defaultEvidenceShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvidencesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultEvidenceShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidenceList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultEvidenceShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);

        // Get all the evidenceList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultEvidenceShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidenceList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultEvidenceShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllEvidencesByPathologyIsEqualToSomething() throws Exception {
        // Initialize the database
        Pathology pathology = PathologyResourceIntTest.createEntity(em);
        em.persist(pathology);
        em.flush();
        evidence.setPathology(pathology);
        evidenceRepository.saveAndFlush(evidence);
        Long pathologyId = pathology.getId();

        // Get all the evidenceList where pathology equals to pathologyId
        defaultEvidenceShouldBeFound("pathologyId.equals=" + pathologyId);

        // Get all the evidenceList where pathology equals to pathologyId + 1
        defaultEvidenceShouldNotBeFound("pathologyId.equals=" + (pathologyId + 1));
    }


    @Test
    @Transactional
    public void getAllEvidencesByDosageIsEqualToSomething() throws Exception {
        // Initialize the database
        Dosage dosage = DosageResourceIntTest.createEntity(em);
        em.persist(dosage);
        em.flush();
        evidence.setDosage(dosage);
        evidenceRepository.saveAndFlush(evidence);
        Long dosageId = dosage.getId();

        // Get all the evidenceList where dosage equals to dosageId
        defaultEvidenceShouldBeFound("dosageId.equals=" + dosageId);

        // Get all the evidenceList where dosage equals to dosageId + 1
        defaultEvidenceShouldNotBeFound("dosageId.equals=" + (dosageId + 1));
    }


    @Test
    @Transactional
    public void getAllEvidencesByEvidencereferencesIsEqualToSomething() throws Exception {
        // Initialize the database
        Evidencereferences evidencereferences = EvidencereferencesResourceIntTest.createEntity(em);
        em.persist(evidencereferences);
        em.flush();
        evidence.addEvidencereferences(evidencereferences);
        evidenceRepository.saveAndFlush(evidence);
        Long evidencereferencesId = evidencereferences.getId();

        // Get all the evidenceList where evidencereferences equals to evidencereferencesId
        defaultEvidenceShouldBeFound("evidencereferencesId.equals=" + evidencereferencesId);

        // Get all the evidenceList where evidencereferences equals to evidencereferencesId + 1
        defaultEvidenceShouldNotBeFound("evidencereferencesId.equals=" + (evidencereferencesId + 1));
    }


    @Test
    @Transactional
    public void getAllEvidencesByEvidencetypeIsEqualToSomething() throws Exception {
        // Initialize the database
        Evidencetype evidencetype = EvidencetypeResourceIntTest.createEntity(em);
        em.persist(evidencetype);
        em.flush();
        evidence.setEvidencetype(evidencetype);
        evidenceRepository.saveAndFlush(evidence);
        Long evidencetypeId = evidencetype.getId();

        // Get all the evidenceList where evidencetype equals to evidencetypeId
        defaultEvidenceShouldBeFound("evidencetypeId.equals=" + evidencetypeId);

        // Get all the evidenceList where evidencetype equals to evidencetypeId + 1
        defaultEvidenceShouldNotBeFound("evidencetypeId.equals=" + (evidencetypeId + 1));
    }


    @Test
    @Transactional
    public void getAllEvidencesByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        evidence.setModifiedby(modifiedby);
        evidenceRepository.saveAndFlush(evidence);
        Long modifiedbyId = modifiedby.getId();

        // Get all the evidenceList where modifiedby equals to modifiedbyId
        defaultEvidenceShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the evidenceList where modifiedby equals to modifiedbyId + 1
        defaultEvidenceShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEvidenceShouldBeFound(String filter) throws Exception {
        restEvidenceMockMvc.perform(get("/api/evidences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidence.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEvidenceShouldNotBeFound(String filter) throws Exception {
        restEvidenceMockMvc.perform(get("/api/evidences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingEvidence() throws Exception {
        // Get the evidence
        restEvidenceMockMvc.perform(get("/api/evidences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEvidence() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);
        evidenceSearchRepository.save(evidence);
        int databaseSizeBeforeUpdate = evidenceRepository.findAll().size();

        // Update the evidence
        Evidence updatedEvidence = evidenceRepository.findOne(evidence.getId());
        // Disconnect from session so that the updates on updatedEvidence are not directly saved in db
        em.detach(updatedEvidence);
        updatedEvidence
            .timestamp(UPDATED_TIMESTAMP);
        EvidenceDTO evidenceDTO = evidenceMapper.toDto(updatedEvidence);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user_evidence", SecurityUtilsUnitTest.USER_ROLE);

        restEvidenceMockMvc.perform(put("/api/evidences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidenceDTO)))
            .andExpect(status().isOk());

        // Validate the Evidence in the database
        List<Evidence> evidenceList = evidenceRepository.findAll();
        assertThat(evidenceList).hasSize(databaseSizeBeforeUpdate);
        Evidence testEvidence = evidenceList.get(evidenceList.size() - 1);
        assertThat(testEvidence.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Evidence in Elasticsearch
        Evidence evidenceEs = evidenceSearchRepository.findOne(testEvidence.getId());
        assertThat(testEvidence.getTimestamp()).isEqualTo(testEvidence.getTimestamp());
        assertThat(evidenceEs).isEqualToIgnoringGivenFields(testEvidence, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingEvidence() throws Exception {
        int databaseSizeBeforeUpdate = evidenceRepository.findAll().size();

        // Create the Evidence
        EvidenceDTO evidenceDTO = evidenceMapper.toDto(evidence);
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEvidenceMockMvc.perform(put("/api/evidences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidenceDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidence in the database
        List<Evidence> evidenceList = evidenceRepository.findAll();
        assertThat(evidenceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEvidence() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);
        evidenceSearchRepository.save(evidence);
        int databaseSizeBeforeDelete = evidenceRepository.findAll().size();

        // Get the evidence
        restEvidenceMockMvc.perform(delete("/api/evidences/{id}", evidence.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean evidenceExistsInEs = evidenceSearchRepository.exists(evidence.getId());
        assertThat(evidenceExistsInEs).isFalse();

        // Validate the database is empty
        List<Evidence> evidenceList = evidenceRepository.findAll();
        assertThat(evidenceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEvidence() throws Exception {
        // Initialize the database
        evidenceRepository.saveAndFlush(evidence);
        evidenceSearchRepository.save(evidence);

        // Search the evidence
        restEvidenceMockMvc.perform(get("/api/_search/evidences?query=id:" + evidence.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidence.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Evidence.class);
        Evidence evidence1 = new Evidence();
        evidence1.setId(1L);
        Evidence evidence2 = new Evidence();
        evidence2.setId(evidence1.getId());
        assertThat(evidence1).isEqualTo(evidence2);
        evidence2.setId(2L);
        assertThat(evidence1).isNotEqualTo(evidence2);
        evidence1.setId(null);
        assertThat(evidence1).isNotEqualTo(evidence2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvidenceDTO.class);
        EvidenceDTO evidenceDTO1 = new EvidenceDTO();
        evidenceDTO1.setId(1L);
        EvidenceDTO evidenceDTO2 = new EvidenceDTO();
        assertThat(evidenceDTO1).isNotEqualTo(evidenceDTO2);
        evidenceDTO2.setId(evidenceDTO1.getId());
        assertThat(evidenceDTO1).isEqualTo(evidenceDTO2);
        evidenceDTO2.setId(2L);
        assertThat(evidenceDTO1).isNotEqualTo(evidenceDTO2);
        evidenceDTO1.setId(null);
        assertThat(evidenceDTO1).isNotEqualTo(evidenceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(evidenceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(evidenceMapper.fromId(null)).isNull();
    }
}
