package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Strain;
import es.udc.jose.domain.User;
import es.udc.jose.repository.StrainRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.StrainService;
import es.udc.jose.repository.search.StrainSearchRepository;
import es.udc.jose.service.dto.StrainDTO;
import es.udc.jose.service.mapper.StrainMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.StrainCriteria;
import es.udc.jose.service.StrainQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StrainResource REST controller.
 *
 * @see StrainResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class StrainResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private StrainRepository strainRepository;

    @Autowired
    private StrainMapper strainMapper;

    @Autowired
    private StrainService strainService;

    @Autowired
    private StrainSearchRepository strainSearchRepository;

    @Autowired
    private StrainQueryService strainQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStrainMockMvc;

    private Strain strain;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StrainResource strainResource = new StrainResource(strainService, strainQueryService);
        this.restStrainMockMvc = MockMvcBuilders.standaloneSetup(strainResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Strain createEntity(EntityManager em) {
        Strain strain = new Strain()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        strain.setModifiedby(modifiedby);
        return strain;
    }

    @Before
    public void initTest() {
        strainSearchRepository.deleteAll();
        strain = createEntity(em);
    }

    @Test
    @Transactional
    public void createStrain() throws Exception {
        int databaseSizeBeforeCreate = strainRepository.findAll().size();
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // Create the Strain
        StrainDTO strainDTO = strainMapper.toDto(strain);
        restStrainMockMvc.perform(post("/api/strains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(strainDTO)))
            .andExpect(status().isCreated());

        // Validate the Strain in the database
        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeCreate + 1);
        Strain testStrain = strainList.get(strainList.size() - 1);
        assertThat(testStrain.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStrain.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);

        // Validate the Strain in Elasticsearch
        Strain strainEs = strainSearchRepository.findOne(testStrain.getId());
        assertThat(testStrain.getTimestamp()).isEqualTo(testStrain.getTimestamp());
        assertThat(strainEs).isEqualToIgnoringGivenFields(testStrain, "timestamp");
    }

    @Test
    @Transactional
    public void createStrainWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = strainRepository.findAll().size();

        // Create the Strain with an existing ID
        strain.setId(1L);
        StrainDTO strainDTO = strainMapper.toDto(strain);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStrainMockMvc.perform(post("/api/strains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(strainDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Strain in the database
        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = strainRepository.findAll().size();
        // set the field null
        strain.setName(null);

        // Create the Strain, which fails.
        StrainDTO strainDTO = strainMapper.toDto(strain);

        restStrainMockMvc.perform(post("/api/strains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(strainDTO)))
            .andExpect(status().isBadRequest());

        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStrains() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList
        restStrainMockMvc.perform(get("/api/strains?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(strain.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getStrain() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get the strain
        restStrainMockMvc.perform(get("/api/strains/{id}", strain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(strain.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllStrainsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where name equals to DEFAULT_NAME
        defaultStrainShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the strainList where name equals to UPDATED_NAME
        defaultStrainShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllStrainsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where name in DEFAULT_NAME or UPDATED_NAME
        defaultStrainShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the strainList where name equals to UPDATED_NAME
        defaultStrainShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllStrainsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where name is not null
        defaultStrainShouldBeFound("name.specified=true");

        // Get all the strainList where name is null
        defaultStrainShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllStrainsByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where timestamp equals to DEFAULT_TIMESTAMP
        defaultStrainShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the strainList where timestamp equals to UPDATED_TIMESTAMP
        defaultStrainShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllStrainsByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultStrainShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the strainList where timestamp equals to UPDATED_TIMESTAMP
        defaultStrainShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllStrainsByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where timestamp is not null
        defaultStrainShouldBeFound("timestamp.specified=true");

        // Get all the strainList where timestamp is null
        defaultStrainShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllStrainsByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultStrainShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the strainList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultStrainShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllStrainsByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);

        // Get all the strainList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultStrainShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the strainList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultStrainShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllStrainsByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        strain.setModifiedby(modifiedby);
        strainRepository.saveAndFlush(strain);
        Long modifiedbyId = modifiedby.getId();

        // Get all the strainList where modifiedby equals to modifiedbyId
        defaultStrainShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the strainList where modifiedby equals to modifiedbyId + 1
        defaultStrainShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultStrainShouldBeFound(String filter) throws Exception {
        restStrainMockMvc.perform(get("/api/strains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(strain.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultStrainShouldNotBeFound(String filter) throws Exception {
        restStrainMockMvc.perform(get("/api/strains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingStrain() throws Exception {
        // Get the strain
        restStrainMockMvc.perform(get("/api/strains/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStrain() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);
        strainSearchRepository.save(strain);
        int databaseSizeBeforeUpdate = strainRepository.findAll().size();

        // Update the strain
        Strain updatedStrain = strainRepository.findOne(strain.getId());
        // Disconnect from session so that the updates on updatedStrain are not directly saved in db
        em.detach(updatedStrain);
        updatedStrain
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP);
        StrainDTO strainDTO = strainMapper.toDto(updatedStrain);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        restStrainMockMvc.perform(put("/api/strains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(strainDTO)))
            .andExpect(status().isOk());

        // Validate the Strain in the database
        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeUpdate);
        Strain testStrain = strainList.get(strainList.size() - 1);
        assertThat(testStrain.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStrain.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Strain in Elasticsearch
        Strain strainEs = strainSearchRepository.findOne(testStrain.getId());
        assertThat(testStrain.getTimestamp()).isEqualTo(testStrain.getTimestamp());
        assertThat(strainEs).isEqualToIgnoringGivenFields(testStrain, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingStrain() throws Exception {
        int databaseSizeBeforeUpdate = strainRepository.findAll().size();

        // Create the Strain
        StrainDTO strainDTO = strainMapper.toDto(strain);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStrainMockMvc.perform(put("/api/strains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(strainDTO)))
            .andExpect(status().isCreated());

        // Validate the Strain in the database
        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStrain() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);
        strainSearchRepository.save(strain);
        int databaseSizeBeforeDelete = strainRepository.findAll().size();

        // Get the strain
        restStrainMockMvc.perform(delete("/api/strains/{id}", strain.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean strainExistsInEs = strainSearchRepository.exists(strain.getId());
        assertThat(strainExistsInEs).isFalse();

        // Validate the database is empty
        List<Strain> strainList = strainRepository.findAll();
        assertThat(strainList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchStrain() throws Exception {
        // Initialize the database
        strainRepository.saveAndFlush(strain);
        strainSearchRepository.save(strain);

        // Search the strain
        restStrainMockMvc.perform(get("/api/_search/strains?query=id:" + strain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(strain.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Strain.class);
        Strain strain1 = new Strain();
        strain1.setId(1L);
        Strain strain2 = new Strain();
        strain2.setId(strain1.getId());
        assertThat(strain1).isEqualTo(strain2);
        strain2.setId(2L);
        assertThat(strain1).isNotEqualTo(strain2);
        strain1.setId(null);
        assertThat(strain1).isNotEqualTo(strain2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StrainDTO.class);
        StrainDTO strainDTO1 = new StrainDTO();
        strainDTO1.setId(1L);
        StrainDTO strainDTO2 = new StrainDTO();
        assertThat(strainDTO1).isNotEqualTo(strainDTO2);
        strainDTO2.setId(strainDTO1.getId());
        assertThat(strainDTO1).isEqualTo(strainDTO2);
        strainDTO2.setId(2L);
        assertThat(strainDTO1).isNotEqualTo(strainDTO2);
        strainDTO1.setId(null);
        assertThat(strainDTO1).isNotEqualTo(strainDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(strainMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(strainMapper.fromId(null)).isNull();
    }
}
