package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Doseform;
import es.udc.jose.repository.DoseformRepository;
import es.udc.jose.service.DoseformService;
import es.udc.jose.repository.search.DoseformSearchRepository;
import es.udc.jose.service.dto.DoseformDTO;
import es.udc.jose.service.mapper.DoseformMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.DoseformCriteria;
import es.udc.jose.service.DoseformQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DoseformResource REST controller.
 *
 * @see DoseformResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class DoseformResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);;
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private DoseformRepository doseformRepository;

    @Autowired
    private DoseformMapper doseformMapper;

    @Autowired
    private DoseformService doseformService;

    @Autowired
    private DoseformSearchRepository doseformSearchRepository;

    @Autowired
    private DoseformQueryService doseformQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDoseformMockMvc;

    private Doseform doseform;

    private final Logger log = LoggerFactory.getLogger(DoseformResourceIntTest.class);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DoseformResource doseformResource = new DoseformResource(doseformService, doseformQueryService);
        this.restDoseformMockMvc = MockMvcBuilders.standaloneSetup(doseformResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Doseform createEntity(EntityManager em) {
        Doseform doseform = new Doseform()
            .name(DEFAULT_NAME)
            .type(DEFAULT_TYPE)
            .timestamp(DEFAULT_TIMESTAMP);
        return doseform;
    }

    @Before
    public void initTest() {
        doseformSearchRepository.deleteAll();
        doseform = createEntity(em);
    }

    @Test
    @Transactional
    public void createDoseform() throws Exception {
        int databaseSizeBeforeCreate = doseformRepository.findAll().size();

        // Create the Doseform
        DoseformDTO doseformDTO = doseformMapper.toDto(doseform);
        restDoseformMockMvc.perform(post("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isCreated());

        // Validate the Doseform in the database
        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeCreate + 1);
        Doseform testDoseform = doseformList.get(doseformList.size() - 1);
        assertThat(testDoseform.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDoseform.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testDoseform.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);


        // Validate the Doseform in Elasticsearch
        Doseform doseformEs = doseformSearchRepository.findOne(testDoseform.getId());
        assertThat(testDoseform.getTimestamp()).isEqualTo(testDoseform.getTimestamp());
        assertThat(doseformEs).isEqualToIgnoringGivenFields(testDoseform, "timestamp");
    }

    @Test
    @Transactional
    public void createDoseformWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = doseformRepository.findAll().size();

        // Create the Doseform with an existing ID
        doseform.setId(1L);
        DoseformDTO doseformDTO = doseformMapper.toDto(doseform);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDoseformMockMvc.perform(post("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Doseform in the database
        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = doseformRepository.findAll().size();
        // set the field null
        doseform.setName(null);

        // Create the Doseform, which fails.
        DoseformDTO doseformDTO = doseformMapper.toDto(doseform);

        restDoseformMockMvc.perform(post("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isBadRequest());

        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = doseformRepository.findAll().size();
        // set the field null
        doseform.setType(null);

        // Create the Doseform, which fails.
        DoseformDTO doseformDTO = doseformMapper.toDto(doseform);

        restDoseformMockMvc.perform(post("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isBadRequest());

        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void getAllDoseforms() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList
        restDoseformMockMvc.perform(get("/api/doseforms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doseform.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getDoseform() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get the doseform
        restDoseformMockMvc.perform(get("/api/doseforms/{id}", doseform.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(doseform.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllDoseformsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where name equals to DEFAULT_NAME
        defaultDoseformShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the doseformList where name equals to UPDATED_NAME
        defaultDoseformShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDoseformsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where name in DEFAULT_NAME or UPDATED_NAME
        defaultDoseformShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the doseformList where name equals to UPDATED_NAME
        defaultDoseformShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDoseformsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where name is not null
        defaultDoseformShouldBeFound("name.specified=true");

        // Get all the doseformList where name is null
        defaultDoseformShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoseformsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where type equals to DEFAULT_TYPE
        defaultDoseformShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the doseformList where type equals to UPDATED_TYPE
        defaultDoseformShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllDoseformsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultDoseformShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the doseformList where type equals to UPDATED_TYPE
        defaultDoseformShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllDoseformsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where type is not null
        defaultDoseformShouldBeFound("type.specified=true");

        // Get all the doseformList where type is null
        defaultDoseformShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoseformsByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where timestamp equals to DEFAULT_TIMESTAMP
        defaultDoseformShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the doseformList where timestamp equals to UPDATED_TIMESTAMP
        defaultDoseformShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDoseformsByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultDoseformShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the doseformList where timestamp equals to UPDATED_TIMESTAMP
        defaultDoseformShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDoseformsByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where timestamp is not null
        defaultDoseformShouldBeFound("timestamp.specified=true");

        // Get all the doseformList where timestamp is null
        defaultDoseformShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllDoseformsByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultDoseformShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the doseformList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultDoseformShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDoseformsByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);

        // Get all the doseformList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultDoseformShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the doseformList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultDoseformShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDoseformShouldBeFound(String filter) throws Exception {
        restDoseformMockMvc.perform(get("/api/doseforms?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doseform.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDoseformShouldNotBeFound(String filter) throws Exception {
        restDoseformMockMvc.perform(get("/api/doseforms?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingDoseform() throws Exception {
        // Get the doseform
        restDoseformMockMvc.perform(get("/api/doseforms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDoseform() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);
        doseformSearchRepository.save(doseform);
        int databaseSizeBeforeUpdate = doseformRepository.findAll().size();

        // Update the doseform
        Doseform updatedDoseform = doseformRepository.findOne(doseform.getId());
        // Disconnect from session so that the updates on updatedDoseform are not directly saved in db
        em.detach(updatedDoseform);
        updatedDoseform
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .timestamp(UPDATED_TIMESTAMP);
        DoseformDTO doseformDTO = doseformMapper.toDto(updatedDoseform);

        restDoseformMockMvc.perform(put("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isOk());

        // Validate the Doseform in the database
        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeUpdate);
        Doseform testDoseform = doseformList.get(doseformList.size() - 1);
        assertThat(testDoseform.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDoseform.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testDoseform.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Doseform in Elasticsearch
        Doseform doseformEs = doseformSearchRepository.findOne(testDoseform.getId());
        assertThat(testDoseform.getTimestamp()).isEqualTo(testDoseform.getTimestamp());
        assertThat(doseformEs).isEqualToIgnoringGivenFields(testDoseform, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingDoseform() throws Exception {
        int databaseSizeBeforeUpdate = doseformRepository.findAll().size();

        // Create the Doseform
        DoseformDTO doseformDTO = doseformMapper.toDto(doseform);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDoseformMockMvc.perform(put("/api/doseforms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(doseformDTO)))
            .andExpect(status().isCreated());

        // Validate the Doseform in the database
        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDoseform() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);
        doseformSearchRepository.save(doseform);
        int databaseSizeBeforeDelete = doseformRepository.findAll().size();

        // Get the doseform
        restDoseformMockMvc.perform(delete("/api/doseforms/{id}", doseform.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean doseformExistsInEs = doseformSearchRepository.exists(doseform.getId());
        assertThat(doseformExistsInEs).isFalse();

        // Validate the database is empty
        List<Doseform> doseformList = doseformRepository.findAll();
        assertThat(doseformList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDoseform() throws Exception {
        // Initialize the database
        doseformRepository.saveAndFlush(doseform);
        doseformSearchRepository.save(doseform);

        // Search the doseform
        restDoseformMockMvc.perform(get("/api/_search/doseforms?query=id:" + doseform.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(doseform.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Doseform.class);
        Doseform doseform1 = new Doseform();
        doseform1.setId(1L);
        Doseform doseform2 = new Doseform();
        doseform2.setId(doseform1.getId());
        assertThat(doseform1).isEqualTo(doseform2);
        doseform2.setId(2L);
        assertThat(doseform1).isNotEqualTo(doseform2);
        doseform1.setId(null);
        assertThat(doseform1).isNotEqualTo(doseform2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DoseformDTO.class);
        DoseformDTO doseformDTO1 = new DoseformDTO();
        doseformDTO1.setId(1L);
        DoseformDTO doseformDTO2 = new DoseformDTO();
        assertThat(doseformDTO1).isNotEqualTo(doseformDTO2);
        doseformDTO2.setId(doseformDTO1.getId());
        assertThat(doseformDTO1).isEqualTo(doseformDTO2);
        doseformDTO2.setId(2L);
        assertThat(doseformDTO1).isNotEqualTo(doseformDTO2);
        doseformDTO1.setId(null);
        assertThat(doseformDTO1).isNotEqualTo(doseformDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(doseformMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(doseformMapper.fromId(null)).isNull();
    }
}
