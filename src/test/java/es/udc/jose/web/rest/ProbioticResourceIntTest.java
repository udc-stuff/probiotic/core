package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Probiotic;
import es.udc.jose.domain.Allergens;
import es.udc.jose.domain.Strain;
import es.udc.jose.domain.User;
import es.udc.jose.repository.ProbioticRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.ProbioticService;
import es.udc.jose.repository.search.ProbioticSearchRepository;
import es.udc.jose.service.dto.ProbioticDTO;
import es.udc.jose.service.mapper.ProbioticMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.ProbioticCriteria;
import es.udc.jose.service.ProbioticQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProbioticResource REST controller.
 *
 * @see ProbioticResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class ProbioticResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    private static final Boolean DEFAULT_HAVE_ALLERGENS = false;
    private static final Boolean UPDATED_HAVE_ALLERGENS = true;

    @Autowired
    private ProbioticRepository probioticRepository;

    @Autowired
    private ProbioticMapper probioticMapper;

    @Autowired
    private ProbioticService probioticService;

    @Autowired
    private ProbioticSearchRepository probioticSearchRepository;

    @Autowired
    private ProbioticQueryService probioticQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProbioticMockMvc;

    private Probiotic probiotic;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProbioticResource probioticResource = new ProbioticResource(probioticService, probioticQueryService);
        this.restProbioticMockMvc = MockMvcBuilders.standaloneSetup(probioticResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Probiotic createEntity(EntityManager em) {
        Probiotic probiotic = new Probiotic()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP)
            .have_allergens(DEFAULT_HAVE_ALLERGENS);
        // Add required entity
        Allergens allergens = AllergensResourceIntTest.createEntity(em);
        em.persist(allergens);
        em.flush();
        probiotic.getAllergens().add(allergens);
        // Add required entity
        Strain strain = StrainResourceIntTest.createEntity(em);
        em.persist(strain);
        em.flush();
        probiotic.getStrains().add(strain);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        probiotic.setModifiedby(modifiedby);
        return probiotic;
    }

    @Before
    public void initTest() {
        probioticSearchRepository.deleteAll();
        probiotic = createEntity(em);
    }

    @Test
    @Transactional
    public void createProbiotic() throws Exception {
        int databaseSizeBeforeCreate = probioticRepository.findAll().size();
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        // Create the Probiotic
        ProbioticDTO probioticDTO = probioticMapper.toDto(probiotic);
        restProbioticMockMvc.perform(post("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isCreated());

        // Validate the Probiotic in the database
        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeCreate + 1);
        Probiotic testProbiotic = probioticList.get(probioticList.size() - 1);
        assertThat(testProbiotic.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProbiotic.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testProbiotic.isHave_allergens()).isEqualTo(DEFAULT_HAVE_ALLERGENS);

        // Validate the Probiotic in Elasticsearch
        Probiotic probioticEs = probioticSearchRepository.findOne(testProbiotic.getId());
        assertThat(testProbiotic.getTimestamp()).isEqualTo(testProbiotic.getTimestamp());
        assertThat(probioticEs).isEqualToIgnoringGivenFields(testProbiotic, "timestamp");
    }

    @Test
    @Transactional
    public void createProbioticWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = probioticRepository.findAll().size();

        // Create the Probiotic with an existing ID
        probiotic.setId(1L);
        ProbioticDTO probioticDTO = probioticMapper.toDto(probiotic);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProbioticMockMvc.perform(post("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Probiotic in the database
        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = probioticRepository.findAll().size();
        // set the field null
        probiotic.setName(null);

        // Create the Probiotic, which fails.
        ProbioticDTO probioticDTO = probioticMapper.toDto(probiotic);

        restProbioticMockMvc.perform(post("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isBadRequest());

        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void checkHave_allergensIsRequired() throws Exception {
        int databaseSizeBeforeTest = probioticRepository.findAll().size();
        // set the field null
        probiotic.setHave_allergens(null);

        // Create the Probiotic, which fails.
        ProbioticDTO probioticDTO = probioticMapper.toDto(probiotic);

        restProbioticMockMvc.perform(post("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isBadRequest());

        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProbiotics() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList
        restProbioticMockMvc.perform(get("/api/probiotics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(probiotic.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].have_allergens").value(hasItem(DEFAULT_HAVE_ALLERGENS.booleanValue())));
    }

    @Test
    @Transactional
    public void getProbiotic() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get the probiotic
        restProbioticMockMvc.perform(get("/api/probiotics/{id}", probiotic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(probiotic.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)))
            .andExpect(jsonPath("$.have_allergens").value(DEFAULT_HAVE_ALLERGENS.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProbioticsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where name equals to DEFAULT_NAME
        defaultProbioticShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the probioticList where name equals to UPDATED_NAME
        defaultProbioticShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProbioticsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProbioticShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the probioticList where name equals to UPDATED_NAME
        defaultProbioticShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProbioticsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where name is not null
        defaultProbioticShouldBeFound("name.specified=true");

        // Get all the probioticList where name is null
        defaultProbioticShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbioticsByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where timestamp equals to DEFAULT_TIMESTAMP
        defaultProbioticShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the probioticList where timestamp equals to UPDATED_TIMESTAMP
        defaultProbioticShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllProbioticsByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultProbioticShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the probioticList where timestamp equals to UPDATED_TIMESTAMP
        defaultProbioticShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllProbioticsByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where timestamp is not null
        defaultProbioticShouldBeFound("timestamp.specified=true");

        // Get all the probioticList where timestamp is null
        defaultProbioticShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbioticsByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultProbioticShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the probioticList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultProbioticShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllProbioticsByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultProbioticShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the probioticList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultProbioticShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllProbioticsByHave_allergensIsEqualToSomething() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where have_allergens equals to DEFAULT_HAVE_ALLERGENS
        defaultProbioticShouldBeFound("have_allergens.equals=" + DEFAULT_HAVE_ALLERGENS);

        // Get all the probioticList where have_allergens equals to UPDATED_HAVE_ALLERGENS
        defaultProbioticShouldNotBeFound("have_allergens.equals=" + UPDATED_HAVE_ALLERGENS);
    }

    @Test
    @Transactional
    public void getAllProbioticsByHave_allergensIsInShouldWork() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where have_allergens in DEFAULT_HAVE_ALLERGENS or UPDATED_HAVE_ALLERGENS
        defaultProbioticShouldBeFound("have_allergens.in=" + DEFAULT_HAVE_ALLERGENS + "," + UPDATED_HAVE_ALLERGENS);

        // Get all the probioticList where have_allergens equals to UPDATED_HAVE_ALLERGENS
        defaultProbioticShouldNotBeFound("have_allergens.in=" + UPDATED_HAVE_ALLERGENS);
    }

    @Test
    @Transactional
    public void getAllProbioticsByHave_allergensIsNullOrNotNull() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);

        // Get all the probioticList where have_allergens is not null
        defaultProbioticShouldBeFound("have_allergens.specified=true");

        // Get all the probioticList where have_allergens is null
        defaultProbioticShouldNotBeFound("have_allergens.specified=false");
    }

    @Test
    @Transactional
    public void getAllProbioticsByAllergensIsEqualToSomething() throws Exception {
        // Initialize the database
        Allergens allergens = AllergensResourceIntTest.createEntity(em);
        em.persist(allergens);
        em.flush();
        probiotic.addAllergens(allergens);
        probioticRepository.saveAndFlush(probiotic);
        Long allergensId = allergens.getId();

        // Get all the probioticList where allergens equals to allergensId
        defaultProbioticShouldBeFound("allergensId.equals=" + allergensId);

        // Get all the probioticList where allergens equals to allergensId + 1
        defaultProbioticShouldNotBeFound("allergensId.equals=" + (allergensId + 1));
    }


    @Test
    @Transactional
    public void getAllProbioticsByStrainIsEqualToSomething() throws Exception {
        // Initialize the database
        Strain strain = StrainResourceIntTest.createEntity(em);
        em.persist(strain);
        em.flush();
        probiotic.addStrain(strain);
        probioticRepository.saveAndFlush(probiotic);
        Long strainId = strain.getId();

        // Get all the probioticList where strain equals to strainId
        defaultProbioticShouldBeFound("strainId.equals=" + strainId);

        // Get all the probioticList where strain equals to strainId + 1
        defaultProbioticShouldNotBeFound("strainId.equals=" + (strainId + 1));
    }


    @Test
    @Transactional
    public void getAllProbioticsByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        probiotic.setModifiedby(modifiedby);
        probioticRepository.saveAndFlush(probiotic);
        Long modifiedbyId = modifiedby.getId();

        // Get all the probioticList where modifiedby equals to modifiedbyId
        defaultProbioticShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the probioticList where modifiedby equals to modifiedbyId + 1
        defaultProbioticShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultProbioticShouldBeFound(String filter) throws Exception {
        restProbioticMockMvc.perform(get("/api/probiotics?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(probiotic.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].have_allergens").value(hasItem(DEFAULT_HAVE_ALLERGENS.booleanValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultProbioticShouldNotBeFound(String filter) throws Exception {
        restProbioticMockMvc.perform(get("/api/probiotics?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingProbiotic() throws Exception {
        // Get the probiotic
        restProbioticMockMvc.perform(get("/api/probiotics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProbiotic() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);
        probioticSearchRepository.save(probiotic);
        int databaseSizeBeforeUpdate = probioticRepository.findAll().size();

        // Update the probiotic
        Probiotic updatedProbiotic = probioticRepository.findOne(probiotic.getId());
        // Disconnect from session so that the updates on updatedProbiotic are not directly saved in db
        em.detach(updatedProbiotic);
        updatedProbiotic
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP)
            .have_allergens(UPDATED_HAVE_ALLERGENS);
        ProbioticDTO probioticDTO = probioticMapper.toDto(updatedProbiotic);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        restProbioticMockMvc.perform(put("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isOk());

        // Validate the Probiotic in the database
        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeUpdate);
        Probiotic testProbiotic = probioticList.get(probioticList.size() - 1);
        assertThat(testProbiotic.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProbiotic.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);
        assertThat(testProbiotic.isHave_allergens()).isEqualTo(UPDATED_HAVE_ALLERGENS);

        // Validate the Probiotic in Elasticsearch
        Probiotic probioticEs = probioticSearchRepository.findOne(testProbiotic.getId());
        assertThat(testProbiotic.getTimestamp()).isEqualTo(testProbiotic.getTimestamp());
        assertThat(probioticEs).isEqualToIgnoringGivenFields(testProbiotic, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingProbiotic() throws Exception {
        int databaseSizeBeforeUpdate = probioticRepository.findAll().size();

        // Create the Probiotic
        ProbioticDTO probioticDTO = probioticMapper.toDto(probiotic);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProbioticMockMvc.perform(put("/api/probiotics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(probioticDTO)))
            .andExpect(status().isCreated());

        // Validate the Probiotic in the database
        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProbiotic() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);
        probioticSearchRepository.save(probiotic);
        int databaseSizeBeforeDelete = probioticRepository.findAll().size();

        // Get the probiotic
        restProbioticMockMvc.perform(delete("/api/probiotics/{id}", probiotic.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean probioticExistsInEs = probioticSearchRepository.exists(probiotic.getId());
        assertThat(probioticExistsInEs).isFalse();

        // Validate the database is empty
        List<Probiotic> probioticList = probioticRepository.findAll();
        assertThat(probioticList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchProbiotic() throws Exception {
        // Initialize the database
        probioticRepository.saveAndFlush(probiotic);
        probioticSearchRepository.save(probiotic);

        // Search the probiotic
        restProbioticMockMvc.perform(get("/api/_search/probiotics?query=id:" + probiotic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(probiotic.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].have_allergens").value(hasItem(DEFAULT_HAVE_ALLERGENS.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Probiotic.class);
        Probiotic probiotic1 = new Probiotic();
        probiotic1.setId(1L);
        Probiotic probiotic2 = new Probiotic();
        probiotic2.setId(probiotic1.getId());
        assertThat(probiotic1).isEqualTo(probiotic2);
        probiotic2.setId(2L);
        assertThat(probiotic1).isNotEqualTo(probiotic2);
        probiotic1.setId(null);
        assertThat(probiotic1).isNotEqualTo(probiotic2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProbioticDTO.class);
        ProbioticDTO probioticDTO1 = new ProbioticDTO();
        probioticDTO1.setId(1L);
        ProbioticDTO probioticDTO2 = new ProbioticDTO();
        assertThat(probioticDTO1).isNotEqualTo(probioticDTO2);
        probioticDTO2.setId(probioticDTO1.getId());
        assertThat(probioticDTO1).isEqualTo(probioticDTO2);
        probioticDTO2.setId(2L);
        assertThat(probioticDTO1).isNotEqualTo(probioticDTO2);
        probioticDTO1.setId(null);
        assertThat(probioticDTO1).isNotEqualTo(probioticDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(probioticMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(probioticMapper.fromId(null)).isNull();
    }
}
