package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Pathology;
import es.udc.jose.domain.User;
import es.udc.jose.domain.Pathologytype;
import es.udc.jose.repository.PathologyRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.PathologyService;
import es.udc.jose.repository.search.PathologySearchRepository;
import es.udc.jose.service.dto.PathologyDTO;
import es.udc.jose.service.mapper.PathologyMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.PathologyCriteria;
import es.udc.jose.service.PathologyQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PathologyResource REST controller.
 *
 * @see PathologyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class PathologyResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    private static final String DEFAULT_ACRONYM = "AAAAAAAAAA";
    private static final String UPDATED_ACRONYM = "BBBBBBBBBB";

    @Autowired
    private PathologyRepository pathologyRepository;

    @Autowired
    private PathologyMapper pathologyMapper;

    @Autowired
    private PathologyService pathologyService;

    @Autowired
    private PathologySearchRepository pathologySearchRepository;

    @Autowired
    private PathologyQueryService pathologyQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPathologyMockMvc;

    private Pathology pathology;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PathologyResource pathologyResource = new PathologyResource(pathologyService, pathologyQueryService);
        this.restPathologyMockMvc = MockMvcBuilders.standaloneSetup(pathologyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pathology createEntity(EntityManager em) {
        Pathology pathology = new Pathology()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP)
            .acronym(DEFAULT_ACRONYM);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        pathology.setModifiedby(modifiedby);
        // Add required entity
        Pathologytype pathologytype = PathologytypeResourceIntTest.createEntity(em);
        em.persist(pathologytype);
        em.flush();
        pathology.setPathologytype(pathologytype);
        return pathology;
    }

    @Before
    public void initTest() {
        pathologySearchRepository.deleteAll();
        pathology = createEntity(em);
    }

    @Test
    @Transactional
    public void createPathology() throws Exception {
        int databaseSizeBeforeCreate = pathologyRepository.findAll().size();

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);


        // Create the Pathology
        PathologyDTO pathologyDTO = pathologyMapper.toDto(pathology);
        restPathologyMockMvc.perform(post("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isCreated());

        // Validate the Pathology in the database
        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeCreate + 1);
        Pathology testPathology = pathologyList.get(pathologyList.size() - 1);
        assertThat(testPathology.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPathology.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testPathology.getAcronym()).isEqualTo(DEFAULT_ACRONYM);

        // Validate the Pathology in Elasticsearch
        Pathology pathologyEs = pathologySearchRepository.findOne(testPathology.getId());
        assertThat(testPathology.getTimestamp()).isEqualTo(testPathology.getTimestamp());
        assertThat(pathologyEs).isEqualToIgnoringGivenFields(testPathology, "timestamp");
    }

    @Test
    @Transactional
    public void createPathologyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pathologyRepository.findAll().size();

        // Create the Pathology with an existing ID
        pathology.setId(1L);
        PathologyDTO pathologyDTO = pathologyMapper.toDto(pathology);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPathologyMockMvc.perform(post("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pathology in the database
        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pathologyRepository.findAll().size();
        // set the field null
        pathology.setName(null);

        // Create the Pathology, which fails.
        PathologyDTO pathologyDTO = pathologyMapper.toDto(pathology);

        restPathologyMockMvc.perform(post("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isBadRequest());

        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void checkAcronymIsRequired() throws Exception {
        int databaseSizeBeforeTest = pathologyRepository.findAll().size();
        // set the field null
        pathology.setAcronym(null);

        // Create the Pathology, which fails.
        PathologyDTO pathologyDTO = pathologyMapper.toDto(pathology);

        restPathologyMockMvc.perform(post("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isBadRequest());

        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPathologies() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList
        restPathologyMockMvc.perform(get("/api/pathologies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathology.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].acronym").value(hasItem(DEFAULT_ACRONYM.toString())));
    }

    @Test
    @Transactional
    public void getPathology() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get the pathology
        restPathologyMockMvc.perform(get("/api/pathologies/{id}", pathology.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pathology.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)))
            .andExpect(jsonPath("$.acronym").value(DEFAULT_ACRONYM.toString()));
    }

    @Test
    @Transactional
    public void getAllPathologiesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where name equals to DEFAULT_NAME
        defaultPathologyShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the pathologyList where name equals to UPDATED_NAME
        defaultPathologyShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPathologiesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPathologyShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the pathologyList where name equals to UPDATED_NAME
        defaultPathologyShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPathologiesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where name is not null
        defaultPathologyShouldBeFound("name.specified=true");

        // Get all the pathologyList where name is null
        defaultPathologyShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllPathologiesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where timestamp equals to DEFAULT_TIMESTAMP
        defaultPathologyShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the pathologyList where timestamp equals to UPDATED_TIMESTAMP
        defaultPathologyShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologiesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultPathologyShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the pathologyList where timestamp equals to UPDATED_TIMESTAMP
        defaultPathologyShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologiesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where timestamp is not null
        defaultPathologyShouldBeFound("timestamp.specified=true");

        // Get all the pathologyList where timestamp is null
        defaultPathologyShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllPathologiesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultPathologyShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the pathologyList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultPathologyShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologiesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultPathologyShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the pathologyList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultPathologyShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllPathologiesByAcronymIsEqualToSomething() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where acronym equals to DEFAULT_ACRONYM
        defaultPathologyShouldBeFound("acronym.equals=" + DEFAULT_ACRONYM);

        // Get all the pathologyList where acronym equals to UPDATED_ACRONYM
        defaultPathologyShouldNotBeFound("acronym.equals=" + UPDATED_ACRONYM);
    }

    @Test
    @Transactional
    public void getAllPathologiesByAcronymIsInShouldWork() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where acronym in DEFAULT_ACRONYM or UPDATED_ACRONYM
        defaultPathologyShouldBeFound("acronym.in=" + DEFAULT_ACRONYM + "," + UPDATED_ACRONYM);

        // Get all the pathologyList where acronym equals to UPDATED_ACRONYM
        defaultPathologyShouldNotBeFound("acronym.in=" + UPDATED_ACRONYM);
    }

    @Test
    @Transactional
    public void getAllPathologiesByAcronymIsNullOrNotNull() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);

        // Get all the pathologyList where acronym is not null
        defaultPathologyShouldBeFound("acronym.specified=true");

        // Get all the pathologyList where acronym is null
        defaultPathologyShouldNotBeFound("acronym.specified=false");
    }

    @Test
    @Transactional
    public void getAllPathologiesByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        pathology.setModifiedby(modifiedby);
        pathologyRepository.saveAndFlush(pathology);
        Long modifiedbyId = modifiedby.getId();

        // Get all the pathologyList where modifiedby equals to modifiedbyId
        defaultPathologyShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the pathologyList where modifiedby equals to modifiedbyId + 1
        defaultPathologyShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }


    @Test
    @Transactional
    public void getAllPathologiesByPathologytypeIsEqualToSomething() throws Exception {
        // Initialize the database
        Pathologytype pathologytype = PathologytypeResourceIntTest.createEntity(em);
        em.persist(pathologytype);
        em.flush();
        pathology.setPathologytype(pathologytype);
        pathologyRepository.saveAndFlush(pathology);
        Long pathologytypeId = pathologytype.getId();

        // Get all the pathologyList where pathologytype equals to pathologytypeId
        defaultPathologyShouldBeFound("pathologytypeId.equals=" + pathologytypeId);

        // Get all the pathologyList where pathologytype equals to pathologytypeId + 1
        defaultPathologyShouldNotBeFound("pathologytypeId.equals=" + (pathologytypeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPathologyShouldBeFound(String filter) throws Exception {
        restPathologyMockMvc.perform(get("/api/pathologies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathology.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].acronym").value(hasItem(DEFAULT_ACRONYM.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPathologyShouldNotBeFound(String filter) throws Exception {
        restPathologyMockMvc.perform(get("/api/pathologies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingPathology() throws Exception {
        // Get the pathology
        restPathologyMockMvc.perform(get("/api/pathologies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePathology() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);
        pathologySearchRepository.save(pathology);
        int databaseSizeBeforeUpdate = pathologyRepository.findAll().size();

        // Update the pathology
        Pathology updatedPathology = pathologyRepository.findOne(pathology.getId());
        // Disconnect from session so that the updates on updatedPathology are not directly saved in db
        em.detach(updatedPathology);
        updatedPathology
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP)
            .acronym(UPDATED_ACRONYM);
        PathologyDTO pathologyDTO = pathologyMapper.toDto(updatedPathology);
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        restPathologyMockMvc.perform(put("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isOk());

        // Validate the Pathology in the database
        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeUpdate);
        Pathology testPathology = pathologyList.get(pathologyList.size() - 1);
        assertThat(testPathology.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPathology.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);
        assertThat(testPathology.getAcronym()).isEqualTo(UPDATED_ACRONYM);

        // Validate the Pathology in Elasticsearch
        Pathology pathologyEs = pathologySearchRepository.findOne(testPathology.getId());
        assertThat(testPathology.getTimestamp()).isEqualTo(testPathology.getTimestamp());
        assertThat(pathologyEs).isEqualToIgnoringGivenFields(testPathology, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingPathology() throws Exception {
        int databaseSizeBeforeUpdate = pathologyRepository.findAll().size();

        // Create the Pathology
        PathologyDTO pathologyDTO = pathologyMapper.toDto(pathology);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPathologyMockMvc.perform(put("/api/pathologies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologyDTO)))
            .andExpect(status().isCreated());

        // Validate the Pathology in the database
        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePathology() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);
        pathologySearchRepository.save(pathology);
        int databaseSizeBeforeDelete = pathologyRepository.findAll().size();

        // Get the pathology
        restPathologyMockMvc.perform(delete("/api/pathologies/{id}", pathology.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pathologyExistsInEs = pathologySearchRepository.exists(pathology.getId());
        assertThat(pathologyExistsInEs).isFalse();

        // Validate the database is empty
        List<Pathology> pathologyList = pathologyRepository.findAll();
        assertThat(pathologyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPathology() throws Exception {
        // Initialize the database
        pathologyRepository.saveAndFlush(pathology);
        pathologySearchRepository.save(pathology);

        // Search the pathology
        restPathologyMockMvc.perform(get("/api/_search/pathologies?query=id:" + pathology.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathology.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].acronym").value(hasItem(DEFAULT_ACRONYM.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pathology.class);
        Pathology pathology1 = new Pathology();
        pathology1.setId(1L);
        Pathology pathology2 = new Pathology();
        pathology2.setId(pathology1.getId());
        assertThat(pathology1).isEqualTo(pathology2);
        pathology2.setId(2L);
        assertThat(pathology1).isNotEqualTo(pathology2);
        pathology1.setId(null);
        assertThat(pathology1).isNotEqualTo(pathology2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PathologyDTO.class);
        PathologyDTO pathologyDTO1 = new PathologyDTO();
        pathologyDTO1.setId(1L);
        PathologyDTO pathologyDTO2 = new PathologyDTO();
        assertThat(pathologyDTO1).isNotEqualTo(pathologyDTO2);
        pathologyDTO2.setId(pathologyDTO1.getId());
        assertThat(pathologyDTO1).isEqualTo(pathologyDTO2);
        pathologyDTO2.setId(2L);
        assertThat(pathologyDTO1).isNotEqualTo(pathologyDTO2);
        pathologyDTO1.setId(null);
        assertThat(pathologyDTO1).isNotEqualTo(pathologyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pathologyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pathologyMapper.fromId(null)).isNull();
    }
}
