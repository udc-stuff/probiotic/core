package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Dosage;
import es.udc.jose.domain.Probiotic;
import es.udc.jose.domain.Doseform;
import es.udc.jose.domain.User;
import es.udc.jose.repository.DosageRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.DosageService;
import es.udc.jose.repository.search.DosageSearchRepository;
import es.udc.jose.service.dto.DosageDTO;
import es.udc.jose.service.mapper.DosageMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.DosageCriteria;
import es.udc.jose.service.DosageQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import es.udc.jose.domain.enumeration.Age;
/**
 * Test class for the DosageResource REST controller.
 *
 * @see DosageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class DosageResourceIntTest {

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);;
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    private static final Age DEFAULT_AGE = Age.Adulto;
    private static final Age UPDATED_AGE = Age.Pediatrico;

    private static final String DEFAULT_INTAKE = "AAAAAAAAAA";
    private static final String UPDATED_INTAKE = "BBBBBBBBBB";

    private static final String DEFAULT_FREQUENCY = "AAAAAAAAAA";
    private static final String UPDATED_FREQUENCY = "BBBBBBBBBB";

    private static final String DEFAULT_INDICATION = "AAAAAAAAAA";
    private static final String UPDATED_INDICATION = "BBBBBBBBBB";

    private static final Long DEFAULT_CFU = 1L;
    private static final Long UPDATED_CFU = 2L;
    private static final Long NOT_CFU = 10000000000000L;

    @Autowired
    private DosageRepository dosageRepository;

    @Autowired
    private DosageMapper dosageMapper;

    @Autowired
    private DosageService dosageService;

    @Autowired
    private DosageSearchRepository dosageSearchRepository;

    @Autowired
    private DosageQueryService dosageQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDosageMockMvc;

    private Dosage dosage;

    private User user;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DosageResource dosageResource = new DosageResource(dosageService, dosageQueryService);
        this.restDosageMockMvc = MockMvcBuilders.standaloneSetup(dosageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dosage createEntity(EntityManager em) {
        Dosage dosage = new Dosage()
            .timestamp(DEFAULT_TIMESTAMP)
            .age(DEFAULT_AGE)
            .intake(DEFAULT_INTAKE)
            .frequency(DEFAULT_FREQUENCY)
            .indication(DEFAULT_INDICATION)
            .cfu(DEFAULT_CFU);

        // Add required entity
        Probiotic probiotic = ProbioticResourceIntTest.createEntity(em);
        em.persist(probiotic);
        em.flush();
        dosage.setProbiotic(probiotic);
        // Add required entity
        Doseform doseform = DoseformResourceIntTest.createEntity(em);
        em.persist(doseform);
        em.flush();
        dosage.setDoseform(doseform);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        dosage.setModifiedby(modifiedby);
        return dosage;
    }

    @Before
    public void initTest() {
        dosageSearchRepository.deleteAll();
        dosage = createEntity(em);
    }

    @Test
    @Transactional
    public void createDosage() throws Exception {
        int databaseSizeBeforeCreate = dosageRepository.findAll().size();
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        // Create the Dosage
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);
        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isCreated());

        // Validate the Dosage in the database
        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeCreate + 1);
        Dosage testDosage = dosageList.get(dosageList.size() - 1);
        assertThat(testDosage.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testDosage.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testDosage.getIntake()).isEqualTo(DEFAULT_INTAKE);
        assertThat(testDosage.getFrequency()).isEqualTo(DEFAULT_FREQUENCY);
        assertThat(testDosage.getIndication()).isEqualTo(DEFAULT_INDICATION);
        assertThat(testDosage.getCfu()).isEqualTo(DEFAULT_CFU);

        // Validate the Dosage in Elasticsearch
        Dosage dosageEs = dosageSearchRepository.findOne(testDosage.getId());
        assertThat(testDosage.getTimestamp()).isEqualTo(testDosage.getTimestamp());
        assertThat(dosageEs).isEqualToIgnoringGivenFields(testDosage, "timestamp");
    }

    @Test
    @Transactional
    public void createDosageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dosageRepository.findAll().size();

        // Create the Dosage with an existing ID
        dosage.setId(1L);
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Dosage in the database
        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAgeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dosageRepository.findAll().size();
        // set the field null
        dosage.setAge(null);

        // Create the Dosage, which fails.
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isBadRequest());

        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIntakeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dosageRepository.findAll().size();
        // set the field null
        dosage.setIntake(null);

        // Create the Dosage, which fails.
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isBadRequest());

        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFrequencyIsRequired() throws Exception {
        int databaseSizeBeforeTest = dosageRepository.findAll().size();
        // set the field null
        dosage.setFrequency(null);

        // Create the Dosage, which fails.
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isBadRequest());

        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCfuIsRequired() throws Exception {
        int databaseSizeBeforeTest = dosageRepository.findAll().size();
        // set the field null
        dosage.setCfu(null);

        // Create the Dosage, which fails.
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        restDosageMockMvc.perform(post("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isBadRequest());

        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDosages() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList
        restDosageMockMvc.perform(get("/api/dosages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosage.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.toString())))
            .andExpect(jsonPath("$.[*].intake").value(hasItem(DEFAULT_INTAKE.toString())))
            .andExpect(jsonPath("$.[*].frequency").value(hasItem(DEFAULT_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].indication").value(hasItem(DEFAULT_INDICATION.toString())))
            .andExpect(jsonPath("$.[*].cfu").value(hasItem(DEFAULT_CFU.intValue())));
    }

    @Test
    @Transactional
    public void getDosage() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get the dosage
        restDosageMockMvc.perform(get("/api/dosages/{id}", dosage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dosage.getId().intValue()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE.toString()))
            .andExpect(jsonPath("$.intake").value(DEFAULT_INTAKE.toString()))
            .andExpect(jsonPath("$.frequency").value(DEFAULT_FREQUENCY.toString()))
            .andExpect(jsonPath("$.indication").value(DEFAULT_INDICATION.toString()))
            .andExpect(jsonPath("$.cfu").value(DEFAULT_CFU.intValue()));
    }

    @Test
    @Transactional
    public void getAllDosagesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where timestamp equals to DEFAULT_TIMESTAMP
        defaultDosageShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the dosageList where timestamp equals to UPDATED_TIMESTAMP
        defaultDosageShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDosagesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultDosageShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the dosageList where timestamp equals to UPDATED_TIMESTAMP
        defaultDosageShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDosagesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where timestamp is not null
        defaultDosageShouldBeFound("timestamp.specified=true");

        // Get all the dosageList where timestamp is null
        defaultDosageShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultDosageShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the dosageList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultDosageShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllDosagesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultDosageShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the dosageList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultDosageShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllDosagesByAgeIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where age equals to DEFAULT_AGE
        defaultDosageShouldBeFound("age.equals=" + DEFAULT_AGE);
    }

    @Test
    @Transactional
    public void getAllDosagesByAgeIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where age in DEFAULT_AGE or UPDATED_AGE
        defaultDosageShouldBeFound("age.in=" + DEFAULT_AGE + "," + UPDATED_AGE);
    }

    @Test
    @Transactional
    public void getAllDosagesByAgeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where age is not null
        defaultDosageShouldBeFound("age.specified=true");

        // Get all the dosageList where age is null
        defaultDosageShouldNotBeFound("age.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByIntakeIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where intake equals to DEFAULT_INTAKE
        defaultDosageShouldBeFound("intake.equals=" + DEFAULT_INTAKE);

        // Get all the dosageList where intake equals to UPDATED_INTAKE
        defaultDosageShouldNotBeFound("intake.equals=" + UPDATED_INTAKE);
    }

    @Test
    @Transactional
    public void getAllDosagesByIntakeIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where intake in DEFAULT_INTAKE or UPDATED_INTAKE
        defaultDosageShouldBeFound("intake.in=" + DEFAULT_INTAKE + "," + UPDATED_INTAKE);

        // Get all the dosageList where intake equals to UPDATED_INTAKE
        defaultDosageShouldNotBeFound("intake.in=" + UPDATED_INTAKE);
    }

    @Test
    @Transactional
    public void getAllDosagesByIntakeIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where intake is not null
        defaultDosageShouldBeFound("intake.specified=true");

        // Get all the dosageList where intake is null
        defaultDosageShouldNotBeFound("intake.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByFrequencyIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where frequency equals to DEFAULT_FREQUENCY
        defaultDosageShouldBeFound("frequency.equals=" + DEFAULT_FREQUENCY);

        // Get all the dosageList where frequency equals to UPDATED_FREQUENCY
        defaultDosageShouldNotBeFound("frequency.equals=" + UPDATED_FREQUENCY);
    }

    @Test
    @Transactional
    public void getAllDosagesByFrequencyIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where frequency in DEFAULT_FREQUENCY or UPDATED_FREQUENCY
        defaultDosageShouldBeFound("frequency.in=" + DEFAULT_FREQUENCY + "," + UPDATED_FREQUENCY);

        // Get all the dosageList where frequency equals to UPDATED_FREQUENCY
        defaultDosageShouldNotBeFound("frequency.in=" + UPDATED_FREQUENCY);
    }

    @Test
    @Transactional
    public void getAllDosagesByFrequencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where frequency is not null
        defaultDosageShouldBeFound("frequency.specified=true");

        // Get all the dosageList where frequency is null
        defaultDosageShouldNotBeFound("frequency.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByIndicationIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where indication equals to DEFAULT_INDICATION
        defaultDosageShouldBeFound("indication.equals=" + DEFAULT_INDICATION);

        // Get all the dosageList where indication equals to UPDATED_INDICATION
        defaultDosageShouldNotBeFound("indication.equals=" + UPDATED_INDICATION);
    }

    @Test
    @Transactional
    public void getAllDosagesByIndicationIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where indication in DEFAULT_INDICATION or UPDATED_INDICATION
        defaultDosageShouldBeFound("indication.in=" + DEFAULT_INDICATION + "," + UPDATED_INDICATION);

        // Get all the dosageList where indication equals to UPDATED_INDICATION
        defaultDosageShouldNotBeFound("indication.in=" + UPDATED_INDICATION);
    }

    @Test
    @Transactional
    public void getAllDosagesByIndicationIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where indication is not null
        defaultDosageShouldBeFound("indication.specified=true");

        // Get all the dosageList where indication is null
        defaultDosageShouldNotBeFound("indication.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByCfuIsEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where cfu equals to DEFAULT_CFU
        defaultDosageShouldBeFound("cfu.equals=" + DEFAULT_CFU);

        // Get all the dosageList where cfu equals to UPDATED_CFU
        defaultDosageShouldNotBeFound("cfu.equals=" + UPDATED_CFU);
    }

    @Test
    @Transactional
    public void getAllDosagesByCfuIsInShouldWork() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where cfu in DEFAULT_CFU or UPDATED_CFU
        defaultDosageShouldBeFound("cfu.in=" + DEFAULT_CFU + "," + UPDATED_CFU);

        // Get all the dosageList where cfu equals to UPDATED_CFU
        defaultDosageShouldNotBeFound("cfu.in=" + UPDATED_CFU);
    }

    @Test
    @Transactional
    public void getAllDosagesByCfuIsNullOrNotNull() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where cfu is not null
        defaultDosageShouldBeFound("cfu.specified=true");

        // Get all the dosageList where cfu is null
        defaultDosageShouldNotBeFound("cfu.specified=false");
    }

    @Test
    @Transactional
    public void getAllDosagesByCfuIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where cfu greater than or equals to DEFAULT_CFU
        defaultDosageShouldBeFound("cfu.greaterOrEqualThan=" + DEFAULT_CFU);

        // Get all the dosageList where cfu greater than or equals to UPDATED_CFU
        defaultDosageShouldNotBeFound("cfu.greaterOrEqualThan=" + NOT_CFU);
    }

    @Test
    @Transactional
    public void getAllDosagesByCfuIsLessThanSomething() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);

        // Get all the dosageList where cfu less than or equals to DEFAULT_CFU
        defaultDosageShouldNotBeFound("cfu.lessThan=" + DEFAULT_CFU);

        // Get all the dosageList where cfu less than or equals to UPDATED_CFU
        defaultDosageShouldBeFound("cfu.lessThan=" + UPDATED_CFU);
    }


    @Test
    @Transactional
    public void getAllDosagesByProbioticIsEqualToSomething() throws Exception {
        // Initialize the database
        Probiotic probiotic = ProbioticResourceIntTest.createEntity(em);
        em.persist(probiotic);
        em.flush();
        dosage.setProbiotic(probiotic);
        dosageRepository.saveAndFlush(dosage);
        Long probioticId = probiotic.getId();

        // Get all the dosageList where probiotic equals to probioticId
        defaultDosageShouldBeFound("probioticId.equals=" + probioticId);

        // Get all the dosageList where probiotic equals to probioticId + 1
        defaultDosageShouldNotBeFound("probioticId.equals=" + (probioticId + 1));
    }


    @Test
    @Transactional
    public void getAllDosagesByDoseformIsEqualToSomething() throws Exception {
        // Initialize the database
        Doseform doseform = DoseformResourceIntTest.createEntity(em);
        em.persist(doseform);
        em.flush();
        dosage.setDoseform(doseform);
        dosageRepository.saveAndFlush(dosage);
        Long doseformId = doseform.getId();

        // Get all the dosageList where doseform equals to doseformId
        defaultDosageShouldBeFound("doseformId.equals=" + doseformId);

        // Get all the dosageList where doseform equals to doseformId + 1
        defaultDosageShouldNotBeFound("doseformId.equals=" + (doseformId + 1));
    }


    @Test
    @Transactional
    public void getAllDosagesByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        dosage.setModifiedby(modifiedby);
        dosageRepository.saveAndFlush(dosage);
        Long modifiedbyId = modifiedby.getId();

        // Get all the dosageList where modifiedby equals to modifiedbyId
        defaultDosageShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the dosageList where modifiedby equals to modifiedbyId + 1
        defaultDosageShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDosageShouldBeFound(String filter) throws Exception {
        restDosageMockMvc.perform(get("/api/dosages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosage.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.toString())))
            .andExpect(jsonPath("$.[*].intake").value(hasItem(DEFAULT_INTAKE.toString())))
            .andExpect(jsonPath("$.[*].frequency").value(hasItem(DEFAULT_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].indication").value(hasItem(DEFAULT_INDICATION.toString())))
            .andExpect(jsonPath("$.[*].cfu").value(hasItem(DEFAULT_CFU.intValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDosageShouldNotBeFound(String filter) throws Exception {
        restDosageMockMvc.perform(get("/api/dosages?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingDosage() throws Exception {
        // Get the dosage
        restDosageMockMvc.perform(get("/api/dosages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDosage() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);
        dosageSearchRepository.save(dosage);
        int databaseSizeBeforeUpdate = dosageRepository.findAll().size();

        // Update the dosage
        Dosage updatedDosage = dosageRepository.findOne(dosage.getId());
        // Disconnect from session so that the updates on updatedDosage are not directly saved in db
        em.detach(updatedDosage);
        updatedDosage
            .timestamp(UPDATED_TIMESTAMP)
            .age(UPDATED_AGE)
            .intake(UPDATED_INTAKE)
            .frequency(UPDATED_FREQUENCY)
            .indication(UPDATED_INDICATION)
            .cfu(UPDATED_CFU);
        DosageDTO dosageDTO = dosageMapper.toDto(updatedDosage);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        restDosageMockMvc.perform(put("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isOk());

        // Validate the Dosage in the database
        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeUpdate);
        Dosage testDosage = dosageList.get(dosageList.size() - 1);
        assertThat(testDosage.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);
        assertThat(testDosage.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testDosage.getIntake()).isEqualTo(UPDATED_INTAKE);
        assertThat(testDosage.getFrequency()).isEqualTo(UPDATED_FREQUENCY);
        assertThat(testDosage.getIndication()).isEqualTo(UPDATED_INDICATION);
        assertThat(testDosage.getCfu()).isEqualTo(UPDATED_CFU);

        // Validate the Dosage in Elasticsearch
        Dosage dosageEs = dosageSearchRepository.findOne(testDosage.getId());
        assertThat(testDosage.getTimestamp()).isEqualTo(testDosage.getTimestamp());
        assertThat(dosageEs).isEqualToIgnoringGivenFields(testDosage, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingDosage() throws Exception {
        int databaseSizeBeforeUpdate = dosageRepository.findAll().size();

        // Create the Dosage
        DosageDTO dosageDTO = dosageMapper.toDto(dosage);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDosageMockMvc.perform(put("/api/dosages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dosageDTO)))
            .andExpect(status().isCreated());

        // Validate the Dosage in the database
        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDosage() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);
        dosageSearchRepository.save(dosage);
        int databaseSizeBeforeDelete = dosageRepository.findAll().size();



        // Get the dosage
        restDosageMockMvc.perform(delete("/api/dosages/{id}", dosage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dosageExistsInEs = dosageSearchRepository.exists(dosage.getId());
        assertThat(dosageExistsInEs).isFalse();

        // Validate the database is empty
        List<Dosage> dosageList = dosageRepository.findAll();
        assertThat(dosageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDosage() throws Exception {
        // Initialize the database
        dosageRepository.saveAndFlush(dosage);
        dosageSearchRepository.save(dosage);

        // Search the dosage
        restDosageMockMvc.perform(get("/api/_search/dosages?query=id:" + dosage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dosage.getId().intValue())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.toString())))
            .andExpect(jsonPath("$.[*].intake").value(hasItem(DEFAULT_INTAKE.toString())))
            .andExpect(jsonPath("$.[*].frequency").value(hasItem(DEFAULT_FREQUENCY.toString())))
            .andExpect(jsonPath("$.[*].indication").value(hasItem(DEFAULT_INDICATION.toString())))
            .andExpect(jsonPath("$.[*].cfu").value(hasItem(DEFAULT_CFU.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dosage.class);
        Dosage dosage1 = new Dosage();
        dosage1.setId(1L);
        Dosage dosage2 = new Dosage();
        dosage2.setId(dosage1.getId());
        assertThat(dosage1).isEqualTo(dosage2);
        dosage2.setId(2L);
        assertThat(dosage1).isNotEqualTo(dosage2);
        dosage1.setId(null);
        assertThat(dosage1).isNotEqualTo(dosage2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DosageDTO.class);
        DosageDTO dosageDTO1 = new DosageDTO();
        dosageDTO1.setId(1L);
        DosageDTO dosageDTO2 = new DosageDTO();
        assertThat(dosageDTO1).isNotEqualTo(dosageDTO2);
        dosageDTO2.setId(dosageDTO1.getId());
        assertThat(dosageDTO1).isEqualTo(dosageDTO2);
        dosageDTO2.setId(2L);
        assertThat(dosageDTO1).isNotEqualTo(dosageDTO2);
        dosageDTO1.setId(null);
        assertThat(dosageDTO1).isNotEqualTo(dosageDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dosageMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dosageMapper.fromId(null)).isNull();
    }
}
