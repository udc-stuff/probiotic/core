package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Allergens;
import es.udc.jose.repository.AllergensRepository;
import es.udc.jose.service.AllergensService;
import es.udc.jose.repository.search.AllergensSearchRepository;
import es.udc.jose.service.dto.AllergensDTO;
import es.udc.jose.service.mapper.AllergensMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.AllergensCriteria;
import es.udc.jose.service.AllergensQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AllergensResource REST controller.
 *
 * @see AllergensResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class AllergensResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private AllergensRepository allergensRepository;

    @Autowired
    private AllergensMapper allergensMapper;

    @Autowired
    private AllergensService allergensService;

    @Autowired
    private AllergensSearchRepository allergensSearchRepository;

    @Autowired
    private AllergensQueryService allergensQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAllergensMockMvc;

    private Allergens allergens;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AllergensResource allergensResource = new AllergensResource(allergensService, allergensQueryService);
        this.restAllergensMockMvc = MockMvcBuilders.standaloneSetup(allergensResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Allergens createEntity(EntityManager em) {
        Allergens allergens = new Allergens()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP);
        return allergens;
    }

    @Before
    public void initTest() {
        allergensSearchRepository.deleteAll();
        allergens = createEntity(em);
    }

    @Test
    @Transactional
    public void createAllergens() throws Exception {
        int databaseSizeBeforeCreate = allergensRepository.findAll().size();

        // Create the Allergens
        AllergensDTO allergensDTO = allergensMapper.toDto(allergens);
        restAllergensMockMvc.perform(post("/api/allergens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allergensDTO)))
            .andExpect(status().isCreated());

        // Validate the Allergens in the database
        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeCreate + 1);
        Allergens testAllergens = allergensList.get(allergensList.size() - 1);
        assertThat(testAllergens.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAllergens.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);

        // Validate the Allergens in Elasticsearch
        Allergens allergensEs = allergensSearchRepository.findOne(testAllergens.getId());
        assertThat(testAllergens.getTimestamp()).isEqualTo(testAllergens.getTimestamp());
        assertThat(allergensEs).isEqualToIgnoringGivenFields(testAllergens, "timestamp");
    }

    @Test
    @Transactional
    public void createAllergensWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = allergensRepository.findAll().size();

        // Create the Allergens with an existing ID
        allergens.setId(1L);
        AllergensDTO allergensDTO = allergensMapper.toDto(allergens);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAllergensMockMvc.perform(post("/api/allergens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allergensDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Allergens in the database
        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = allergensRepository.findAll().size();
        // set the field null
        allergens.setName(null);

        // Create the Allergens, which fails.
        AllergensDTO allergensDTO = allergensMapper.toDto(allergens);

        restAllergensMockMvc.perform(post("/api/allergens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allergensDTO)))
            .andExpect(status().isBadRequest());

        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAllergens() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList
        restAllergensMockMvc.perform(get("/api/allergens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(allergens.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getAllergens() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get the allergens
        restAllergensMockMvc.perform(get("/api/allergens/{id}", allergens.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(allergens.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllAllergensByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where name equals to DEFAULT_NAME
        defaultAllergensShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the allergensList where name equals to UPDATED_NAME
        defaultAllergensShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAllergensByNameIsInShouldWork() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAllergensShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the allergensList where name equals to UPDATED_NAME
        defaultAllergensShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllAllergensByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where name is not null
        defaultAllergensShouldBeFound("name.specified=true");

        // Get all the allergensList where name is null
        defaultAllergensShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllAllergensByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where timestamp equals to DEFAULT_TIMESTAMP
        defaultAllergensShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the allergensList where timestamp equals to UPDATED_TIMESTAMP
        defaultAllergensShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllAllergensByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultAllergensShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the allergensList where timestamp equals to UPDATED_TIMESTAMP
        defaultAllergensShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllAllergensByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where timestamp is not null
        defaultAllergensShouldBeFound("timestamp.specified=true");

        // Get all the allergensList where timestamp is null
        defaultAllergensShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllAllergensByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultAllergensShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the allergensList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultAllergensShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllAllergensByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);

        // Get all the allergensList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultAllergensShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the allergensList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultAllergensShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAllergensShouldBeFound(String filter) throws Exception {
        restAllergensMockMvc.perform(get("/api/allergens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(allergens.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAllergensShouldNotBeFound(String filter) throws Exception {
        restAllergensMockMvc.perform(get("/api/allergens?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingAllergens() throws Exception {
        // Get the allergens
        restAllergensMockMvc.perform(get("/api/allergens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAllergens() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);
        allergensSearchRepository.save(allergens);
        int databaseSizeBeforeUpdate = allergensRepository.findAll().size();

        // Update the allergens
        Allergens updatedAllergens = allergensRepository.findOne(allergens.getId());
        // Disconnect from session so that the updates on updatedAllergens are not directly saved in db
        em.detach(updatedAllergens);
        updatedAllergens
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP);
        AllergensDTO allergensDTO = allergensMapper.toDto(updatedAllergens);

        restAllergensMockMvc.perform(put("/api/allergens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allergensDTO)))
            .andExpect(status().isOk());

        // Validate the Allergens in the database
        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeUpdate);
        Allergens testAllergens = allergensList.get(allergensList.size() - 1);
        assertThat(testAllergens.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAllergens.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Allergens in Elasticsearch
        Allergens allergensEs = allergensSearchRepository.findOne(testAllergens.getId());
        assertThat(testAllergens.getTimestamp()).isEqualTo(testAllergens.getTimestamp());
        assertThat(allergensEs).isEqualToIgnoringGivenFields(testAllergens, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingAllergens() throws Exception {
        int databaseSizeBeforeUpdate = allergensRepository.findAll().size();

        // Create the Allergens
        AllergensDTO allergensDTO = allergensMapper.toDto(allergens);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAllergensMockMvc.perform(put("/api/allergens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(allergensDTO)))
            .andExpect(status().isCreated());

        // Validate the Allergens in the database
        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAllergens() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);
        allergensSearchRepository.save(allergens);
        int databaseSizeBeforeDelete = allergensRepository.findAll().size();

        // Get the allergens
        restAllergensMockMvc.perform(delete("/api/allergens/{id}", allergens.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean allergensExistsInEs = allergensSearchRepository.exists(allergens.getId());
        assertThat(allergensExistsInEs).isFalse();

        // Validate the database is empty
        List<Allergens> allergensList = allergensRepository.findAll();
        assertThat(allergensList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAllergens() throws Exception {
        // Initialize the database
        allergensRepository.saveAndFlush(allergens);
        allergensSearchRepository.save(allergens);

        // Search the allergens
        restAllergensMockMvc.perform(get("/api/_search/allergens?query=id:" + allergens.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(allergens.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Allergens.class);
        Allergens allergens1 = new Allergens();
        allergens1.setId(1L);
        Allergens allergens2 = new Allergens();
        allergens2.setId(allergens1.getId());
        assertThat(allergens1).isEqualTo(allergens2);
        allergens2.setId(2L);
        assertThat(allergens1).isNotEqualTo(allergens2);
        allergens1.setId(null);
        assertThat(allergens1).isNotEqualTo(allergens2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AllergensDTO.class);
        AllergensDTO allergensDTO1 = new AllergensDTO();
        allergensDTO1.setId(1L);
        AllergensDTO allergensDTO2 = new AllergensDTO();
        assertThat(allergensDTO1).isNotEqualTo(allergensDTO2);
        allergensDTO2.setId(allergensDTO1.getId());
        assertThat(allergensDTO1).isEqualTo(allergensDTO2);
        allergensDTO2.setId(2L);
        assertThat(allergensDTO1).isNotEqualTo(allergensDTO2);
        allergensDTO1.setId(null);
        assertThat(allergensDTO1).isNotEqualTo(allergensDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(allergensMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(allergensMapper.fromId(null)).isNull();
    }
}
