package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Pathologytype;
import es.udc.jose.repository.PathologytypeRepository;
import es.udc.jose.service.PathologytypeService;
import es.udc.jose.repository.search.PathologytypeSearchRepository;
import es.udc.jose.service.dto.PathologytypeDTO;
import es.udc.jose.service.mapper.PathologytypeMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.PathologytypeCriteria;
import es.udc.jose.service.PathologytypeQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PathologytypeResource REST controller.
 *
 * @see PathologytypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class PathologytypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private PathologytypeRepository pathologytypeRepository;

    @Autowired
    private PathologytypeMapper pathologytypeMapper;

    @Autowired
    private PathologytypeService pathologytypeService;

    @Autowired
    private PathologytypeSearchRepository pathologytypeSearchRepository;

    @Autowired
    private PathologytypeQueryService pathologytypeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPathologytypeMockMvc;

    private Pathologytype pathologytype;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PathologytypeResource pathologytypeResource = new PathologytypeResource(pathologytypeService, pathologytypeQueryService);
        this.restPathologytypeMockMvc = MockMvcBuilders.standaloneSetup(pathologytypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pathologytype createEntity(EntityManager em) {
        Pathologytype pathologytype = new Pathologytype()
            .name(DEFAULT_NAME)
            .timestamp(DEFAULT_TIMESTAMP);
        return pathologytype;
    }

    @Before
    public void initTest() {
        pathologytypeSearchRepository.deleteAll();
        pathologytype = createEntity(em);
    }

    @Test
    @Transactional
    public void createPathologytype() throws Exception {
        int databaseSizeBeforeCreate = pathologytypeRepository.findAll().size();

        // Create the Pathologytype
        PathologytypeDTO pathologytypeDTO = pathologytypeMapper.toDto(pathologytype);
        restPathologytypeMockMvc.perform(post("/api/pathologytypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologytypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Pathologytype in the database
        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeCreate + 1);
        Pathologytype testPathologytype = pathologytypeList.get(pathologytypeList.size() - 1);
        assertThat(testPathologytype.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPathologytype.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);

        // Validate the Pathologytype in Elasticsearch
        Pathologytype pathologytypeEs = pathologytypeSearchRepository.findOne(testPathologytype.getId());
        assertThat(testPathologytype.getTimestamp()).isEqualTo(testPathologytype.getTimestamp());
        assertThat(pathologytypeEs).isEqualToIgnoringGivenFields(testPathologytype, "timestamp");
    }

    @Test
    @Transactional
    public void createPathologytypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pathologytypeRepository.findAll().size();

        // Create the Pathologytype with an existing ID
        pathologytype.setId(1L);
        PathologytypeDTO pathologytypeDTO = pathologytypeMapper.toDto(pathologytype);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPathologytypeMockMvc.perform(post("/api/pathologytypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologytypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pathologytype in the database
        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pathologytypeRepository.findAll().size();
        // set the field null
        pathologytype.setName(null);

        // Create the Pathologytype, which fails.
        PathologytypeDTO pathologytypeDTO = pathologytypeMapper.toDto(pathologytype);

        restPathologytypeMockMvc.perform(post("/api/pathologytypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologytypeDTO)))
            .andExpect(status().isBadRequest());

        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeTest);
    }


    @Test
    @Transactional
    public void getAllPathologytypes() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList
        restPathologytypeMockMvc.perform(get("/api/pathologytypes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathologytype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getPathologytype() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get the pathologytype
        restPathologytypeMockMvc.perform(get("/api/pathologytypes/{id}", pathologytype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pathologytype.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllPathologytypesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where name equals to DEFAULT_NAME
        defaultPathologytypeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the pathologytypeList where name equals to UPDATED_NAME
        defaultPathologytypeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPathologytypesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPathologytypeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the pathologytypeList where name equals to UPDATED_NAME
        defaultPathologytypeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPathologytypesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where name is not null
        defaultPathologytypeShouldBeFound("name.specified=true");

        // Get all the pathologytypeList where name is null
        defaultPathologytypeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllPathologytypesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where timestamp equals to DEFAULT_TIMESTAMP
        defaultPathologytypeShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the pathologytypeList where timestamp equals to UPDATED_TIMESTAMP
        defaultPathologytypeShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologytypesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultPathologytypeShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the pathologytypeList where timestamp equals to UPDATED_TIMESTAMP
        defaultPathologytypeShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologytypesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where timestamp is not null
        defaultPathologytypeShouldBeFound("timestamp.specified=true");

        // Get all the pathologytypeList where timestamp is null
        defaultPathologytypeShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllPathologytypesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultPathologytypeShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the pathologytypeList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultPathologytypeShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllPathologytypesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);

        // Get all the pathologytypeList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultPathologytypeShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the pathologytypeList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultPathologytypeShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPathologytypeShouldBeFound(String filter) throws Exception {
        restPathologytypeMockMvc.perform(get("/api/pathologytypes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathologytype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPathologytypeShouldNotBeFound(String filter) throws Exception {
        restPathologytypeMockMvc.perform(get("/api/pathologytypes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingPathologytype() throws Exception {
        // Get the pathologytype
        restPathologytypeMockMvc.perform(get("/api/pathologytypes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePathologytype() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);
        pathologytypeSearchRepository.save(pathologytype);
        int databaseSizeBeforeUpdate = pathologytypeRepository.findAll().size();

        // Update the pathologytype
        Pathologytype updatedPathologytype = pathologytypeRepository.findOne(pathologytype.getId());
        // Disconnect from session so that the updates on updatedPathologytype are not directly saved in db
        em.detach(updatedPathologytype);
        updatedPathologytype
            .name(UPDATED_NAME)
            .timestamp(UPDATED_TIMESTAMP);
        PathologytypeDTO pathologytypeDTO = pathologytypeMapper.toDto(updatedPathologytype);

        restPathologytypeMockMvc.perform(put("/api/pathologytypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologytypeDTO)))
            .andExpect(status().isOk());

        // Validate the Pathologytype in the database
        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeUpdate);
        Pathologytype testPathologytype = pathologytypeList.get(pathologytypeList.size() - 1);
        assertThat(testPathologytype.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPathologytype.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Pathologytype in Elasticsearch
        Pathologytype pathologytypeEs = pathologytypeSearchRepository.findOne(testPathologytype.getId());
        assertThat(testPathologytype.getTimestamp()).isEqualTo(testPathologytype.getTimestamp());
        assertThat(pathologytypeEs).isEqualToIgnoringGivenFields(testPathologytype, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingPathologytype() throws Exception {
        int databaseSizeBeforeUpdate = pathologytypeRepository.findAll().size();

        // Create the Pathologytype
        PathologytypeDTO pathologytypeDTO = pathologytypeMapper.toDto(pathologytype);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPathologytypeMockMvc.perform(put("/api/pathologytypes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathologytypeDTO)))
            .andExpect(status().isCreated());

        // Validate the Pathologytype in the database
        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePathologytype() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);
        pathologytypeSearchRepository.save(pathologytype);
        int databaseSizeBeforeDelete = pathologytypeRepository.findAll().size();

        // Get the pathologytype
        restPathologytypeMockMvc.perform(delete("/api/pathologytypes/{id}", pathologytype.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean pathologytypeExistsInEs = pathologytypeSearchRepository.exists(pathologytype.getId());
        assertThat(pathologytypeExistsInEs).isFalse();

        // Validate the database is empty
        List<Pathologytype> pathologytypeList = pathologytypeRepository.findAll();
        assertThat(pathologytypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPathologytype() throws Exception {
        // Initialize the database
        pathologytypeRepository.saveAndFlush(pathologytype);
        pathologytypeSearchRepository.save(pathologytype);

        // Search the pathologytype
        restPathologytypeMockMvc.perform(get("/api/_search/pathologytypes?query=id:" + pathologytype.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pathologytype.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pathologytype.class);
        Pathologytype pathologytype1 = new Pathologytype();
        pathologytype1.setId(1L);
        Pathologytype pathologytype2 = new Pathologytype();
        pathologytype2.setId(pathologytype1.getId());
        assertThat(pathologytype1).isEqualTo(pathologytype2);
        pathologytype2.setId(2L);
        assertThat(pathologytype1).isNotEqualTo(pathologytype2);
        pathologytype1.setId(null);
        assertThat(pathologytype1).isNotEqualTo(pathologytype2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PathologytypeDTO.class);
        PathologytypeDTO pathologytypeDTO1 = new PathologytypeDTO();
        pathologytypeDTO1.setId(1L);
        PathologytypeDTO pathologytypeDTO2 = new PathologytypeDTO();
        assertThat(pathologytypeDTO1).isNotEqualTo(pathologytypeDTO2);
        pathologytypeDTO2.setId(pathologytypeDTO1.getId());
        assertThat(pathologytypeDTO1).isEqualTo(pathologytypeDTO2);
        pathologytypeDTO2.setId(2L);
        assertThat(pathologytypeDTO1).isNotEqualTo(pathologytypeDTO2);
        pathologytypeDTO1.setId(null);
        assertThat(pathologytypeDTO1).isNotEqualTo(pathologytypeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pathologytypeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pathologytypeMapper.fromId(null)).isNull();
    }
}
