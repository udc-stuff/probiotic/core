package es.udc.jose.web.rest;

import es.udc.jose.ProbioticApp;

import es.udc.jose.domain.Evidencereferences;
import es.udc.jose.domain.User;
import es.udc.jose.repository.EvidencereferencesRepository;
import es.udc.jose.security.SecurityUtilsUnitTest;
import es.udc.jose.service.EvidencereferencesService;
import es.udc.jose.repository.search.EvidencereferencesSearchRepository;
import es.udc.jose.service.dto.EvidencereferencesDTO;
import es.udc.jose.service.mapper.EvidencereferencesMapper;
import es.udc.jose.web.rest.errors.ExceptionTranslator;
import es.udc.jose.service.dto.EvidencereferencesCriteria;
import es.udc.jose.service.EvidencereferencesQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static es.udc.jose.web.rest.TestUtil.sameInstant;
import static es.udc.jose.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EvidencereferencesResource REST controller.
 *
 * @see EvidencereferencesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProbioticApp.class)
public class EvidencereferencesResourceIntTest {

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TIMESTAMP = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TIMESTAMP = ZonedDateTime.now();

    @Autowired
    private EvidencereferencesRepository evidencereferencesRepository;

    @Autowired
    private EvidencereferencesMapper evidencereferencesMapper;

    @Autowired
    private EvidencereferencesService evidencereferencesService;

    @Autowired
    private EvidencereferencesSearchRepository evidencereferencesSearchRepository;

    @Autowired
    private EvidencereferencesQueryService evidencereferencesQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEvidencereferencesMockMvc;

    private Evidencereferences evidencereferences;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EvidencereferencesResource evidencereferencesResource = new EvidencereferencesResource(evidencereferencesService, evidencereferencesQueryService);
        this.restEvidencereferencesMockMvc = MockMvcBuilders.standaloneSetup(evidencereferencesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Evidencereferences createEntity(EntityManager em) {
        Evidencereferences evidencereferences = new Evidencereferences()
            .text(DEFAULT_TEXT)
            .timestamp(DEFAULT_TIMESTAMP);
        // Add required entity
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        evidencereferences.setModifiedby(modifiedby);
        return evidencereferences;
    }

    @Before
    public void initTest() {
        evidencereferencesSearchRepository.deleteAll();
        evidencereferences = createEntity(em);
    }

    @Test
    @Transactional
    public void createEvidencereferences() throws Exception {
        int databaseSizeBeforeCreate = evidencereferencesRepository.findAll().size();
        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user_references", SecurityUtilsUnitTest.USER_ROLE);

        // Create the Evidencereferences
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesMapper.toDto(evidencereferences);
        restEvidencereferencesMockMvc.perform(post("/api/evidencereferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencereferencesDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidencereferences in the database
        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeCreate + 1);
        Evidencereferences testEvidencereferences = evidencereferencesList.get(evidencereferencesList.size() - 1);
        assertThat(testEvidencereferences.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testEvidencereferences.getTimestamp()).isAfterOrEqualTo(DEFAULT_TIMESTAMP);
        // Validate the Evidencereferences in Elasticsearch
        Evidencereferences evidencereferencesEs = evidencereferencesSearchRepository.findOne(testEvidencereferences.getId());
        assertThat(testEvidencereferences.getTimestamp()).isEqualTo(testEvidencereferences.getTimestamp());
        assertThat(evidencereferencesEs).isEqualToIgnoringGivenFields(testEvidencereferences, "timestamp");
    }

    @Test
    @Transactional
    public void createEvidencereferencesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = evidencereferencesRepository.findAll().size();

        // Create the Evidencereferences with an existing ID
        evidencereferences.setId(1L);
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesMapper.toDto(evidencereferences);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEvidencereferencesMockMvc.perform(post("/api/evidencereferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencereferencesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Evidencereferences in the database
        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTextIsRequired() throws Exception {
        int databaseSizeBeforeTest = evidencereferencesRepository.findAll().size();
        // set the field null
        evidencereferences.setText(null);

        // Create the Evidencereferences, which fails.
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesMapper.toDto(evidencereferences);

        restEvidencereferencesMockMvc.perform(post("/api/evidencereferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencereferencesDTO)))
            .andExpect(status().isBadRequest());

        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEvidencereferences() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList
        restEvidencereferencesMockMvc.perform(get("/api/evidencereferences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencereferences.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void getEvidencereferences() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get the evidencereferences
        restEvidencereferencesMockMvc.perform(get("/api/evidencereferences/{id}", evidencereferences.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(evidencereferences.getId().intValue()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.timestamp").value(sameInstant(DEFAULT_TIMESTAMP)));
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTextIsEqualToSomething() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where text equals to DEFAULT_TEXT
        defaultEvidencereferencesShouldBeFound("text.equals=" + DEFAULT_TEXT);

        // Get all the evidencereferencesList where text equals to UPDATED_TEXT
        defaultEvidencereferencesShouldNotBeFound("text.equals=" + UPDATED_TEXT);
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTextIsInShouldWork() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where text in DEFAULT_TEXT or UPDATED_TEXT
        defaultEvidencereferencesShouldBeFound("text.in=" + DEFAULT_TEXT + "," + UPDATED_TEXT);

        // Get all the evidencereferencesList where text equals to UPDATED_TEXT
        defaultEvidencereferencesShouldNotBeFound("text.in=" + UPDATED_TEXT);
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTextIsNullOrNotNull() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where text is not null
        defaultEvidencereferencesShouldBeFound("text.specified=true");

        // Get all the evidencereferencesList where text is null
        defaultEvidencereferencesShouldNotBeFound("text.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTimestampIsEqualToSomething() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where timestamp equals to DEFAULT_TIMESTAMP
        defaultEvidencereferencesShouldBeFound("timestamp.equals=" + DEFAULT_TIMESTAMP);

        // Get all the evidencereferencesList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidencereferencesShouldNotBeFound("timestamp.equals=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTimestampIsInShouldWork() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where timestamp in DEFAULT_TIMESTAMP or UPDATED_TIMESTAMP
        defaultEvidencereferencesShouldBeFound("timestamp.in=" + DEFAULT_TIMESTAMP + "," + UPDATED_TIMESTAMP);

        // Get all the evidencereferencesList where timestamp equals to UPDATED_TIMESTAMP
        defaultEvidencereferencesShouldNotBeFound("timestamp.in=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTimestampIsNullOrNotNull() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where timestamp is not null
        defaultEvidencereferencesShouldBeFound("timestamp.specified=true");

        // Get all the evidencereferencesList where timestamp is null
        defaultEvidencereferencesShouldNotBeFound("timestamp.specified=false");
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTimestampIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where timestamp greater than or equals to DEFAULT_TIMESTAMP
        defaultEvidencereferencesShouldBeFound("timestamp.greaterOrEqualThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidencereferencesList where timestamp greater than or equals to UPDATED_TIMESTAMP
        defaultEvidencereferencesShouldNotBeFound("timestamp.greaterOrEqualThan=" + UPDATED_TIMESTAMP);
    }

    @Test
    @Transactional
    public void getAllEvidencereferencesByTimestampIsLessThanSomething() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);

        // Get all the evidencereferencesList where timestamp less than or equals to DEFAULT_TIMESTAMP
        defaultEvidencereferencesShouldNotBeFound("timestamp.lessThan=" + DEFAULT_TIMESTAMP);

        // Get all the evidencereferencesList where timestamp less than or equals to UPDATED_TIMESTAMP
        defaultEvidencereferencesShouldBeFound("timestamp.lessThan=" + UPDATED_TIMESTAMP);
    }


    @Test
    @Transactional
    public void getAllEvidencereferencesByModifiedbyIsEqualToSomething() throws Exception {
        // Initialize the database
        User modifiedby = UserResourceIntTest.createEntity(em);
        em.persist(modifiedby);
        em.flush();
        evidencereferences.setModifiedby(modifiedby);
        evidencereferencesRepository.saveAndFlush(evidencereferences);
        Long modifiedbyId = modifiedby.getId();

        // Get all the evidencereferencesList where modifiedby equals to modifiedbyId
        defaultEvidencereferencesShouldBeFound("modifiedbyId.equals=" + modifiedbyId);

        // Get all the evidencereferencesList where modifiedby equals to modifiedbyId + 1
        defaultEvidencereferencesShouldNotBeFound("modifiedbyId.equals=" + (modifiedbyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultEvidencereferencesShouldBeFound(String filter) throws Exception {
        restEvidencereferencesMockMvc.perform(get("/api/evidencereferences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencereferences.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultEvidencereferencesShouldNotBeFound(String filter) throws Exception {
        restEvidencereferencesMockMvc.perform(get("/api/evidencereferences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingEvidencereferences() throws Exception {
        // Get the evidencereferences
        restEvidencereferencesMockMvc.perform(get("/api/evidencereferences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEvidencereferences() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);
        evidencereferencesSearchRepository.save(evidencereferences);
        int databaseSizeBeforeUpdate = evidencereferencesRepository.findAll().size();

        // Update the evidencereferences
        Evidencereferences updatedEvidencereferences = evidencereferencesRepository.findOne(evidencereferences.getId());
        // Disconnect from session so that the updates on updatedEvidencereferences are not directly saved in db
        em.detach(updatedEvidencereferences);
        updatedEvidencereferences
            .text(UPDATED_TEXT)
            .timestamp(UPDATED_TIMESTAMP);
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesMapper.toDto(updatedEvidencereferences);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);

        restEvidencereferencesMockMvc.perform(put("/api/evidencereferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencereferencesDTO)))
            .andExpect(status().isOk());

        // Validate the Evidencereferences in the database
        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeUpdate);
        Evidencereferences testEvidencereferences = evidencereferencesList.get(evidencereferencesList.size() - 1);
        assertThat(testEvidencereferences.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testEvidencereferences.getTimestamp()).isAfterOrEqualTo(UPDATED_TIMESTAMP);

        // Validate the Evidencereferences in Elasticsearch
        Evidencereferences evidencereferencesEs = evidencereferencesSearchRepository.findOne(testEvidencereferences.getId());
        assertThat(testEvidencereferences.getTimestamp()).isEqualTo(testEvidencereferences.getTimestamp());
        assertThat(evidencereferencesEs).isEqualToIgnoringGivenFields(testEvidencereferences, "timestamp");
    }

    @Test
    @Transactional
    public void updateNonExistingEvidencereferences() throws Exception {
        int databaseSizeBeforeUpdate = evidencereferencesRepository.findAll().size();

        // Create the Evidencereferences
        EvidencereferencesDTO evidencereferencesDTO = evidencereferencesMapper.toDto(evidencereferences);

        //Authenticate
        SecurityUtilsUnitTest.authenticate(4L, "user", SecurityUtilsUnitTest.USER_ROLE);
        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEvidencereferencesMockMvc.perform(put("/api/evidencereferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(evidencereferencesDTO)))
            .andExpect(status().isCreated());

        // Validate the Evidencereferences in the database
        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEvidencereferences() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);
        evidencereferencesSearchRepository.save(evidencereferences);
        int databaseSizeBeforeDelete = evidencereferencesRepository.findAll().size();

        // Get the evidencereferences
        restEvidencereferencesMockMvc.perform(delete("/api/evidencereferences/{id}", evidencereferences.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean evidencereferencesExistsInEs = evidencereferencesSearchRepository.exists(evidencereferences.getId());
        assertThat(evidencereferencesExistsInEs).isFalse();

        // Validate the database is empty
        List<Evidencereferences> evidencereferencesList = evidencereferencesRepository.findAll();
        assertThat(evidencereferencesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEvidencereferences() throws Exception {
        // Initialize the database
        evidencereferencesRepository.saveAndFlush(evidencereferences);
        evidencereferencesSearchRepository.save(evidencereferences);

        // Search the evidencereferences
        restEvidencereferencesMockMvc.perform(get("/api/_search/evidencereferences?query=id:" + evidencereferences.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(evidencereferences.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(sameInstant(DEFAULT_TIMESTAMP))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Evidencereferences.class);
        Evidencereferences evidencereferences1 = new Evidencereferences();
        evidencereferences1.setId(1L);
        Evidencereferences evidencereferences2 = new Evidencereferences();
        evidencereferences2.setId(evidencereferences1.getId());
        assertThat(evidencereferences1).isEqualTo(evidencereferences2);
        evidencereferences2.setId(2L);
        assertThat(evidencereferences1).isNotEqualTo(evidencereferences2);
        evidencereferences1.setId(null);
        assertThat(evidencereferences1).isNotEqualTo(evidencereferences2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EvidencereferencesDTO.class);
        EvidencereferencesDTO evidencereferencesDTO1 = new EvidencereferencesDTO();
        evidencereferencesDTO1.setId(1L);
        EvidencereferencesDTO evidencereferencesDTO2 = new EvidencereferencesDTO();
        assertThat(evidencereferencesDTO1).isNotEqualTo(evidencereferencesDTO2);
        evidencereferencesDTO2.setId(evidencereferencesDTO1.getId());
        assertThat(evidencereferencesDTO1).isEqualTo(evidencereferencesDTO2);
        evidencereferencesDTO2.setId(2L);
        assertThat(evidencereferencesDTO1).isNotEqualTo(evidencereferencesDTO2);
        evidencereferencesDTO1.setId(null);
        assertThat(evidencereferencesDTO1).isNotEqualTo(evidencereferencesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(evidencereferencesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(evidencereferencesMapper.fromId(null)).isNull();
    }
}
