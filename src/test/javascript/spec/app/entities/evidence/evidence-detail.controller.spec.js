'use strict';

describe('Controller Tests', function() {

    describe('Evidence Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockEvidence, MockPathology, MockDosage, MockEvidencereferences, MockEvidencetype, MockUser;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockEvidence = jasmine.createSpy('MockEvidence');
            MockPathology = jasmine.createSpy('MockPathology');
            MockDosage = jasmine.createSpy('MockDosage');
            MockEvidencereferences = jasmine.createSpy('MockEvidencereferences');
            MockEvidencetype = jasmine.createSpy('MockEvidencetype');
            MockUser = jasmine.createSpy('MockUser');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Evidence': MockEvidence,
                'Pathology': MockPathology,
                'Dosage': MockDosage,
                'Evidencereferences': MockEvidencereferences,
                'Evidencetype': MockEvidencetype,
                'User': MockUser
            };
            createController = function() {
                $injector.get('$controller')("EvidenceDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'probioticApp:evidenceUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
