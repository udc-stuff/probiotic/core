'use strict';

describe('Controller Tests', function() {

    describe('Dosage Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockDosage, MockProbiotic, MockDoseform, MockUser;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockDosage = jasmine.createSpy('MockDosage');
            MockProbiotic = jasmine.createSpy('MockProbiotic');
            MockDoseform = jasmine.createSpy('MockDoseform');
            MockUser = jasmine.createSpy('MockUser');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Dosage': MockDosage,
                'Probiotic': MockProbiotic,
                'Doseform': MockDoseform,
                'User': MockUser
            };
            createController = function() {
                $injector.get('$controller')("DosageDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'probioticApp:dosageUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
