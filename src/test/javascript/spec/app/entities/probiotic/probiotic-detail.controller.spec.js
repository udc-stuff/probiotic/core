'use strict';

describe('Controller Tests', function() {

    describe('Probiotic Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockProbiotic, MockAllergens, MockStrain, MockUser;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockProbiotic = jasmine.createSpy('MockProbiotic');
            MockAllergens = jasmine.createSpy('MockAllergens');
            MockStrain = jasmine.createSpy('MockStrain');
            MockUser = jasmine.createSpy('MockUser');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Probiotic': MockProbiotic,
                'Allergens': MockAllergens,
                'Strain': MockStrain,
                'User': MockUser
            };
            createController = function() {
                $injector.get('$controller')("ProbioticDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'probioticApp:probioticUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
